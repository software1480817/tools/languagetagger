unit commandline;

interface

uses
  replaceTags;

procedure print(const aMsg: string; const aWaitAfter:Boolean = False);
procedure waitForInput();
function setExpressionsINI(const aReplaceTags: TReplaceTags): Boolean;
procedure setSources(const aReplaceTags: TReplaceTags);
procedure setLangDir(const aReplaceTags: TReplaceTags);
function setLogFile(): Boolean;
function CommandLineExecution(): Integer;
procedure printHelp();

implementation

uses
  System.SysUtils, Vcl.Forms;



var
  fOutput:      Boolean;
  fLogFile:   string;

procedure print(const aMsg: string; const aWaitAfter:Boolean = False);
begin
  if fOutput then
  begin
    Writeln(aMsg);
    if aWaitAfter then
      Readln;
  end;
end;

procedure waitForInput();
begin
  if fOutput then
    ReadLn;
end;
function setExpressionsINI(const aReplaceTags: TReplaceTags): Boolean;
var
  i: Integer;
  vExp: string;
begin
  if FindCmdLineSwitch('e') then
  begin
    for i := 0 to ParamCount do
      if ('-e' = ParamStr(i)) then Break;
    if not FileExists(ParamStr(i+1)) or not (ExtractFileExt(ParamStr(i+1)) = '.ini') then
    begin
      print('Pfad zur Expressions.ini erwartet');
      waitForInput;
      Result := False;
      Exit;
    end;
    vExp := ExpandFileName(ParamStr(i+1));
    aReplaceTags.Settings.Expressions := vExp;
    print('nutze Expressions-Datei: ' + sLineBreak + vExp);
  end;
  Result := True;
end;

procedure setSources(const aReplaceTags: TReplaceTags);
var
  i: Integer;
begin
  if not FindCmdLineSwitch('sources') then
    Exit;
  for i := 0 to ParamCount do
    if ParamStr(i) = '-sources' then Break;
  if not DirectoryExists(ParamStr(i+1)) then
  begin
    print('Quellverzeichnis existiert nicht', True);
    Exit;
  end;
  aReplaceTags.Settings.Sources := ParamStr(i+1);
end;

procedure setLangDir(const aReplaceTags: TReplaceTags);
var
  i: Integer;
begin
  for i := 0 to ParamCount do
    if ParamStr(i) = '-langdir' then
    begin
      aReplaceTags.Settings.LanguagePath := ParamStr(i+1);
      print('erste Sprachdatei: ' + aReplaceTags.Settings.LanguageFiles[0]);
      print('im Verzeichnis: ' + aReplaceTags.Settings.LanguagePath);
      Exit;
    end;
end;

function setLogFile(): Boolean;
var
  i: Integer;
begin
  Result := False;
  if not FindCmdLineSwitch('logfile') then
  begin
    print('keine Logdatei angegeben!'
        + sLineBreak + 'Angabe mit -logfile');
    Exit;
  end;
  for i := 0 to ParamCount do
    if ParamStr(i) = '-logfile' then Break;
  if i > ParamCount then Exit;
  fLogFile := ExpandFileName(ParamStr(i+1));
  print('Logfile: ' + fLogFile);
  Result := True;
end;

procedure printHelp();
begin
  fOutput := True;
  print('Benutzung wie folgt: ');
  print(ExtractFileName(Application.ExeName) + ' option' + sLineBreak
    + 'm�gliche Optionen:' + sLineBreak
    + '   -h        Hilfe' + sLineBreak
    + '   -v        Ausgabe aktivieren' + sLineBreak
    + '   -e        Pfad zur Expressions.ini' + sLineBreak
    + '   -logfile  Angabe des LogFile' + sLineBreak
    + '   -sources  Quellverzeichnis angeben' + sLineBreak
    + '   -langdir  Verzeichnis der Sprachdateien');
end;

const
  RET_ERROR = -1;

function CommandLineExecution(): Integer;
var
  vReplaceTags: TReplaceTags;
  i: Integer;
  vCountOccurrences: Integer;
  vFilesIncomplete: TArray<string>;
begin

  ExitCode := 0;
  result := 0;

  fLogFile := EmptyStr;
  fOutput := False;
  if FindCmdLineSwitch('h') or FindCmdLineSwitch('?') then
  begin
    printHelp();
    ExitCode := 0;
    result := RET_ERROR;
    Exit;
  end;
  if FindCmdLineSwitch('v') then
    fOutput := True;
  if not setLogFile then
  begin
    Result := RET_ERROR;
    //Exit;
  end;
  vReplaceTags := TReplaceTags.Create;
  try
    print('Ausgabe aktiviert..');
    if not setExpressionsINI(vReplaceTags) then
    begin
      Result := RET_ERROR;
      Exit;
    end;
    if not vReplaceTags.loadExpressionsFile() then
    begin
      print('Fehler beim Laden der Expressions.ini', True);
      result := RET_ERROR;
      Exit;
    end;
    setLangDir(vReplaceTags);
    setSources(vReplaceTags);
    vReplaceTags.LoadFilesSourcesFromPath();
    vCountOccurrences := vReplaceTags.searchExpressionsInSRC;
    if vCountOccurrences <> 0 then
    begin
      print('so viele m�ssen noch ersetzt werden: ' + IntToStr(vCountOccurrences));
      print('in folgenden Dateien:');
      vFilesIncomplete := vReplaceTags.GetListOfFilesToProcess;
      for i := 0 to High(vFilesIncomplete) do
        print(vFilesIncomplete[i]);
      if not (fLogFile = EmptyStr) then
        vReplaceTags.LogFilesToProcess(fLogFile);
    end;
    result := vCountOccurrences;
  finally
    vReplaceTags.Free;
  end;
end;

end.
