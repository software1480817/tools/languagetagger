object FormUnusedTags: TFormUnusedTags
  Left = 0
  Top = 0
  Caption = 'Unused Tags'
  ClientHeight = 274
  ClientWidth = 1027
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 758
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  OnShow = FormShow
  DesignSize = (
    1027
    274)
  PixelsPerInch = 96
  TextHeight = 13
  object lLanguageFile: TLabel
    Left = 8
    Top = 5
    Width = 57
    Height = 13
    Caption = 'Sprachdatei'
  end
  object lSources: TLabel
    Left = 135
    Top = 5
    Width = 36
    Height = 13
    Caption = 'Quellen'
  end
  object lDiffLangSRC: TLabel
    Left = 262
    Top = 5
    Width = 98
    Height = 13
    Hint = 'Doppelklick oder "m" |Tags, die in den Quellen gefunden wurden, '
    Caption = 'diff Sprache-Quellen'
  end
  object lCountTagsLangFile: TLabel
    Left = 8
    Top = 123
    Width = 93
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lCountTagsLangFile'
  end
  object lCountTagsSources: TLabel
    Left = 135
    Top = 123
    Width = 58
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Anzahl Tags'
  end
  object lCountTagsDiffLangSRC: TLabel
    Left = 262
    Top = 123
    Width = 58
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Anzahl Tags'
  end
  object lTagsNotFoundInSRCs: TLabel
    Left = 389
    Top = 5
    Width = 104
    Height = 13
    Caption = 'nicht gefundene Tags'
  end
  object lCountTagsNotFoundInSRCs: TLabel
    Left = 389
    Top = 123
    Width = 58
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Anzahl Tags'
  end
  object lTagSearchedInSRCs: TLabel
    Left = 516
    Top = 5
    Width = 100
    Height = 13
    Caption = 'lTagSearchedInSRCs'
  end
  object lstTagsLangFile: TListBox
    Left = 8
    Top = 24
    Width = 121
    Height = 95
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object lstTagsSources: TListBox
    Left = 135
    Top = 24
    Width = 121
    Height = 95
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 1
  end
  object lstTagsDiffLangSRC: TListBox
    Left = 262
    Top = 24
    Width = 121
    Height = 95
    Hint = 
      'Tags, die in den Quellen gefunden wurden, '#13#10'aber nicht in der Sp' +
      'rachdatei enthalten sind.'#13#10'Tags in dieser Spalte werden nicht au' +
      's der '#13#10'Sprachdatei entfernt.'#13#10'- Doppelklick oder Enter um Tag i' +
      'n Quellen '#13#10'  zu suchen und anzuzeigen'#13#10'- "m" um Tag nach rechts' +
      ' '#13#10'   zu verschieben'
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 2
    OnDblClick = bSearchSelectedInSRCClick
    OnKeyPress = lstTagsDiffLangSRCKeyPress
  end
  object bLookForUsedTags: TButton
    Left = 135
    Top = 180
    Width = 121
    Height = 40
    Anchors = [akLeft, akBottom]
    Caption = 'finde Tags in Quelldateien'
    TabOrder = 8
    WordWrap = True
    OnClick = bLookForUsedTagsClick
  end
  object bSearchTagsInSRCs: TButton
    Left = 262
    Top = 141
    Width = 121
    Height = 33
    Anchors = [akLeft, akBottom]
    Caption = 'suche Vorkommen  aller Tags in Quellen'
    Enabled = False
    TabOrder = 6
    WordWrap = True
    OnClick = bSearchTagsInSRCsClick
  end
  object lstTagsNotFoundInSRCs: TListBox
    Left = 389
    Top = 24
    Width = 121
    Height = 95
    Hint = 
      'diese Tags werden aus der Sprachdatei entfernt'#13#10'- "m" um Tag nac' +
      'h links zu verschieben'
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 3
    OnKeyPress = lstTagsNotFoundInSRCsKeyPress
  end
  object lstFoundTagsText: TListBox
    Left = 516
    Top = 24
    Width = 503
    Height = 95
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 4
    OnClick = lstFoundTagsTextClick
  end
  object bSearchSelectedInSRC: TButton
    Left = 262
    Top = 180
    Width = 121
    Height = 40
    Anchors = [akLeft, akBottom]
    Caption = 'suche ausgew'#228'hlten Tag in Quellen'
    ElevationRequired = True
    Enabled = False
    TabOrder = 9
    WordWrap = True
    OnClick = bSearchSelectedInSRCClick
  end
  object eFileSearchedTagInSRCs: TEdit
    Left = 516
    Top = 123
    Width = 503
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 5
    Text = 'Datei in der der Ausdruck gefunden wurde'
  end
  object bMoveTagRight: TButton
    Left = 262
    Top = 226
    Width = 121
    Height = 25
    Hint = 'Die Liste markieren und "m" dr'#252'cken'#13#10'bewirkt das selbe'
    Anchors = [akLeft, akBottom]
    Caption = 'verschiebe Tag -->'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 12
    OnClick = bMoveTagRightClick
  end
  object bMoveTagLeft: TButton
    Left = 389
    Top = 226
    Width = 121
    Height = 25
    Hint = 'Die Liste markieren und "m" dr'#252'cken'
    Anchors = [akLeft, akBottom]
    Caption = '<-- verschiebe Tag'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 13
    OnClick = bMoveTagLeftClick
  end
  object bDeleteTagsFromLangFile: TButton
    Left = 848
    Top = 226
    Width = 171
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'l'#246'sche Tags aus Sprachdatei'
    Enabled = False
    TabOrder = 14
    WordWrap = True
    OnClick = bDeleteTagsFromLangFileClick
  end
  object chkOverWriteFiles: TCheckBox
    Left = 801
    Top = 203
    Width = 355
    Height = 17
    Anchors = [akRight, akBottom]
    Caption = 'Sprachdatei im Zielordner '#252'berschreiben'
    TabOrder = 11
  end
  object eTargetDirectory: TEdit
    Left = 516
    Top = 176
    Width = 503
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 7
    Text = 'eTargetDirectory'
  end
  object bMoveAllBack: TButton
    Left = 389
    Top = 180
    Width = 121
    Height = 40
    Anchors = [akLeft, akBottom]
    Caption = '<-- verschiebe alle zur'#252'ck'
    Enabled = False
    TabOrder = 10
    WordWrap = True
    OnClick = bMoveAllBackClick
  end
  object statBottom: TStatusBar
    Left = 0
    Top = 255
    Width = 1027
    Height = 19
    Panels = <>
    ExplicitLeft = 520
    ExplicitTop = 192
    ExplicitWidth = 0
  end
  object chkShowToolTips: TCheckBox
    Left = 8
    Top = 232
    Width = 97
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'zeige Hints'
    TabOrder = 15
    OnClick = chkShowToolTipsClick
  end
end
