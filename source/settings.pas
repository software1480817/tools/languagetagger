unit settings;

interface

uses
  common.SafeMemIni,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls,
  replaceTags;

type
  TFormSettings = class(TForm)
    lTopic1: TLabel;
    chkOverWriteFiles: TCheckBox;
    chkOverwriteOrigins: TCheckBox;
    eExprFile: TLabeledEdit;
    bCancel: TButton;
    bLeave: TButton;
    bLoadActualFile: TButton;
    eDirDefLanguageFile: TLabeledEdit;
    eDirDefSRCs: TLabeledEdit;
    eDirTargetFiles: TLabeledEdit;
    chkSearchPAS: TCheckBox;
    chkSearchDFM: TCheckBox;
    bOK: TButton;
    chkPreferLastTag: TCheckBox;
    procedure bLeaveClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure bLoadActualFileClick(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure eExprFileKeyPress(Sender: TObject; var Key: Char);

  private
    { Private-Deklarationen }
    fReplaceTags: TReplaceTags;

    procedure saveExprFile;
    procedure showActualSettings;
    procedure readSettingsFromInput;
    function loadSettingsFromFile(): Boolean;


  public
    { Public-Deklarationen }
    class function LoadSettings(const aReplaceTags: TReplaceTags):
      Boolean;

    constructor Create(const aOwner: TComponent; aReplaceTags: TReplaceTags); reintroduce;
  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.dfm}

{ TFormSettings }

constructor TFormSettings.Create(const aOwner: TComponent; aReplaceTags: TReplaceTags);
begin
  inherited Create(aOwner);
  fReplaceTags := aReplaceTags;
  LoadSettingsFromFile;
end;


procedure TFormSettings.eExprFileKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key = #13) then
    Exit;
  fReplaceTags.Settings.IsLoaded := False;
  readSettingsFromInput;
  loadSettingsFromFile;
end;

procedure TFormSettings.FormShow(Sender: TObject);
begin
  showActualSettings;
end;

procedure TFormSettings.bCancelClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFormSettings.bLeaveClick(Sender: TObject);
begin
  readSettingsFromInput;
  saveExprFile;
end;

procedure TFormSettings.bLoadActualFileClick(Sender: TObject);
begin
  fReplaceTags.loadExpressionsFile;
  showActualSettings;
end;

procedure TFormSettings.bOKClick(Sender: TObject);
begin
  readSettingsFromInput;
  Self.Close;
end;

function TFormSettings.loadSettingsFromFile(): Boolean;
begin
  if not fReplaceTags.Settings.IsLoaded then
  begin
    fReplaceTags.loadExpressionsFile;
  end;
  if not fReplaceTags.Settings.IsLoaded then
    Result := False
  else
    Result := True;
end;

class function TFormSettings.LoadSettings(const aReplaceTags: TReplaceTags):
  Boolean;
var
  vSet: TFormSettings;
begin
  vSet := TFormSettings.Create(nil, aReplaceTags);
  Result := vSet.fReplaceTags.Settings.IsLoaded;
  vSet.Free;
end;

procedure TFormSettings.saveExprFile;
var
  vINI: TSafeIni;
begin
  with fReplaceTags.Settings do
  begin
    Sources := eDirDefSRCs.Text;
    LanguagePath := eDirDefLanguageFile.Text;
    OverWrite := chkOverWriteFiles.Checked;
    OverWriteOrigins := chkOverwriteOrigins.Checked;
    DirectoryTarget := eDirTargetFiles.Text;

    vINI := TSafeIni.Create(Expressions);
    vINI.WriteString('settings', 'SourcesDefDir', Sources);
    vINI.WriteString('settings', 'LanguageDefDir', LanguagePath);
    vINI.WriteBool('settings', 'OverWriteFiles', OverWrite);
    vINI.WriteBool('settings', 'OverWriteOrigins', OverWriteOrigins);
    vINI.WriteBool('settings', 'SearchPASFiles', PASSearch);
    vINI.WriteBool('settings', 'SearchDFMFiles', DFMSearch);
    vINI.WriteBool('settings', 'PreferLastTagAsSuggestion', PreferLastTag);
    vINI.WriteString('settings', 'TargetDirectory', DirectoryTarget);
    vINI.UpdateFile;
    vINI.Free;
  end;
end;

procedure TFormSettings.showActualSettings;
begin
  with fReplaceTags.Settings do
  begin
    eExprFile.Text := Expressions;
    eDirDefSRCs.Text := Sources;
    eDirDefLanguageFile.Text := LanguagePath;
    chkOverWriteFiles.Checked := OverWrite;
    chkOverwriteOrigins.Checked := OverWriteOrigins;
    chkSearchPAS.Checked := PASSearch;
    chkSearchDFM.Checked := DFMSearch;
    eDirTargetFiles.Text := DirectoryTarget;
    chkPreferLastTag.Checked := PreferLastTag;
  end;
end;

procedure TFormSettings.readSettingsFromInput;
begin
  with fReplaceTags.Settings do
  begin
    Expressions := eExprFile.Text;
    Sources := eDirDefSRCs.Text;
    LanguagePath := eDirDefLanguageFile.Text;
    OverWrite := chkOverWriteFiles.Checked;
    OverWriteOrigins := chkOverwriteOrigins.Checked;
    PASSearch := chkSearchPAS.Checked;
    DFMSearch := chkSearchDFM.Checked;
    DirectoryTarget := eDirTargetFiles.Text;
    PreferLastTag := chkPreferLastTag.Checked;
  end;
end;


end.
