unit usedTags;

interface
uses
  common.SafeMemIni,
  System.RegularExpressions,
  System.Classes,
  System.Generics.Collections;

type
  TUsedTags = class
    strict private
      fFileNameSRC: string;
      fTagList: TList<Integer>;
      fLanguageFile: string;
    public

      procedure LoadExpressionsFile(const aFN: string);
      procedure LookForTagsInFile(const aPattern: string);
      constructor Create(const aFileName:string);
      destructor Destroy; override;

      property LanguageFile: string read fLanguageFile write fLanguageFile;
  end;

implementation

{ TUsedTags }

constructor TUsedTags.Create(const aFileName: string);
begin
  inherited Create;
  fFileNameSRC := aFileName;
  fTagList := TList<Integer>.Create;
end;
destructor TUsedTags.Destroy;
begin
  fTagList.Free;
  inherited;
end;

procedure TUsedTags.LoadExpressionsFile(const aFN: string);
var
  vINI: TSafeIni;
begin
  vINI := TSafeIni.Create(aFN);
  try

  finally
    vINI.Free;
  end;
end;
procedure TUsedTags.LookForTagsInFile(const aPattern: string);
var
  vContent: TStringList;
  vMatch : TMatch;
begin
  vContent := TStringList.Create;
  try
    vContent.LoadFromFile(fFileNameSRC);
    vMatch := TRegEx.Match(vContent.Text, aPattern, [roIgnoreCase, roMultiLine]);
    while vMatch.Success do
    begin



      vMatch.NextMatch;
    end;

  finally
    vContent.Free;
  end;
end;

end.
