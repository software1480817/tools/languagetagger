unit dfmTagging;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Spring.Collections,
  System.Generics.Collections,
  System.Types,
  Vcl.Grids,
  Vcl.Buttons,
  Vcl.Samples.Spin,
  Vcl.ExtCtrls;

type
  EDFMTagger = class(Exception) end;
  TDFMTagger = class(TForm)
    lstFilesSRCs: TListBox;
    lCountFilesSRCs: TLabel;
    sgResult: TStringGrid;
    bLoadFilesSRCs: TButton;
    bAllYesOrNo: TButton;
    bSetToMinusOne: TButton;
    lCountListObjects: TLabel;
    eFilterSourceFiles: TEdit;
    bFilterSources: TBitBtn;
    bResetFilterSources: TBitBtn;
    lFilterSources: TLabel;
    seMinLengthObject: TSpinEdit;
    bFilterObjectsByCaptionLength: TBitBtn;
    lMinLengthCaption: TLabel;
    lCountReplacements: TLabel;
    bFilterOKFiles: TButton;
    pListsBack: TPanel;
    pLeftList: TPanel;
    eDirectorySRCs: TEdit;
    l1: TLabel;
    spl1: TSplitter;
    procedure FormShow(Sender: TObject);
    procedure bLoadFilesSRCsClick(Sender: TObject);
    procedure bAllYesOrNoClick(Sender: TObject);
    procedure bFilterObjectsByCaptionLengthClick(Sender: TObject);
    procedure bFilterOKFilesClick(Sender: TObject);
    procedure bFilterSourcesClick(Sender: TObject);
    procedure bResetFilterSourcesClick(Sender: TObject);
    procedure bSetToMinusOneClick(Sender: TObject);
    procedure eFilterSourceFilesKeyDown(Sender: TObject; var Key: Word;
        Shift: TShiftState);
    procedure lstFilesSRCsKeyDown(Sender: TObject; var Key: Word;
        Shift: TShiftState);
    procedure sgResultDblClick(Sender: TObject);
    procedure sgResultMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sgResultKeyPress(Sender: TObject; var Key: Char);

  private
  type
    { Private-Deklarationen }
    TListObjects = record
      Name: string;
      InlineType: Boolean;
      Typ: string;
      Caption: string;
      Tag : Integer;
      ShowCaption: Boolean;
      FileName: string;
      ReplaceTag: Boolean;
      procedure Reset;
    end;
  const
    CP_TYPES = 0;

  var
    fDirSRCs: string;
    fDirTarget: string;
    fFilesDFM: TStringDynArray;
    fListObjects: IList<TListObjects>;
    fINITypes: TArray<string>;

    function createPattern(const aType: Integer): string;
    procedure getDFMFilelist;
    function cutLastNameFromInherited(const aName: string): string;
    procedure setInheritedName(const aLine: string; var aInheritedName: string);
    procedure lookForObjectsWith0Tags(const aFileList: TArray<string>);
    procedure addArrayToListObjects(const aElements: array of TListObjects);
    procedure removeObjectsWithValueAssignments;

    { methods working with visible objects }
    procedure toggleReplaceTagValue(const aNumberListElement: Integer);
    function getRowForStringGrid(const aNumberListElement: Integer): TArray<string>;
    procedure showListInStrGrd;
  public
    { Public-Deklarationen }
    procedure SetDirTarget(const aDir: string);
    procedure LoadINIFile(const aFileName: string);

    constructor Create(const aOwner: TComponent; aDirSRCs: string); reintroduce;
    destructor Destroy; override;
    function getSelectedFiles: TArray<string>;
    procedure updateCountReplacements;
  end;

var
  DFMTagger: TDFMTagger;

implementation

uses
  System.IOUtils,
  System.RegularExpressions,
  common.SafeMemIni,
  Winapi.ShellAPI;

{$R *.dfm}

{ TDFMTagger }

procedure TDFMTagger.removeObjectsWithValueAssignments;
var
  i: Integer;
  vFilePAS: string;
  vContent: TStringList;
  vPattern: string;
begin
  if fListObjects.Count = 0 then
    Exit;
  vContent := TStringList.Create;
  i := 0;
  while i < fListObjects.Count do
  begin
    vFilePAS := ExtractFilePath(fListObjects.Items[i].FileName);
    vFilePAS := vFilePAS + TPath.GetFileNameWithoutExtension(fListObjects.Items[i].FileName) + '.pas';
    vContent.LoadFromFile(vFilePAS);
    vPattern := fListObjects.Items[i].Name + '.Caption *:=';
    if TRegEx.IsMatch(vContent.Text, vPattern,[roIgnoreCase,roMultiLine]) then
      fListObjects.Delete(i)
    else
      Inc(i);
  end;
  vContent.Free;
  showListInStrGrd;
end;

function TDFMTagger.createPattern(const aType: Integer): string;
var
  i: Integer;
begin
  if aType = CP_TYPES then
  begin
    Result := '(';
    for i := 0 to High(fINITypes) do
    begin
      Result := Result + fINITypes[i];
      if i = High(fINITypes) then
        Result := Result + '$)'
      else
        Result := Result + '$|';
    end;
  end;
end;


procedure TDFMTagger.getDFMFilelist;
var
  i: Integer;
  vStartIndex: Integer;
begin
  if (fDirSRCs = EmptyStr) or not DirectoryExists(fDirSRCs) then
    raise EDFMTagger.Create('Quellenverzeichnis nicht angegeben');
  fFilesDFM := TDirectory.GetFiles(fDirSRCs, '*.dfm', TSearchOption.soAllDirectories);
  if Length(fFilesDFM) = 0 then
    raise EDFMTagger.Create('keine dfm-Dateien im Quellordner vorhanden');
  lstFilesSRCs.Clear;
  vStartIndex := Length(fDirSRCs);
  for i := 0 to High(fFilesDFM) do
    lstFilesSRCs.Items.Add(fFilesDFM[i].Substring(vStartIndex));
  lCountFilesSRCs.Caption := 'Anzahl: ' + IntToStr(lstFilesSRCs.Count);
end;

function TDFMTagger.cutLastNameFromInherited(const aName: string): string;
var
  i: Integer;
  vSplit: TArray<string>;
begin
  Result := '';
  if aName = '' then Exit;
  vSplit := aName.Split(['.'], TStringSplitOptions.ExcludeEmpty);
  if Length(vSplit) = 1 then
    Exit;
  for i := 0 to ( High(vSplit)-1 ) do
    Result := Result + vSplit[i] + '.';
end;

procedure TDFMTagger.setInheritedName(const aLine: string; var aInheritedName: string);
var
  vMatch: TMatch;
begin
  if TRegEx.IsMatch(aLine, 'inline \w+ *: *\w+', [roIgnoreCase]) then
  begin
    vMatch := TRegEx.Match(aLine, '\w+');
    vMatch := vMatch.NextMatch;
    aInheritedName := aInheritedName + vMatch.Value + '.';
  end;
end;

procedure TDFMTagger.lookForObjectsWith0Tags(const aFileList: TArray<string>);
var
  vLineID, vFileID: Integer;
  vContent: TStringList;
  vMatch: TMatch;
  vSTemp: string;
  vListElements: array of TListObjects;
  vInheritedName: string;
begin
  fListObjects.Clear;
  vInheritedName := EmptyStr;
  SetLength(vListElements,0);
  vContent := TStringList.Create;
  for vFileID := 0 to High(aFileList) do
  begin
    vContent.LoadFromFile(aFileList[vFileID]);
    for vLineID := 0 to vContent.Count-1 do
    begin
      if TRegEx.IsMatch(vContent[vLineID], '^ *end *$') then
        vInheritedName := cutLastNameFromInherited(vInheritedName);
      if TRegEx.IsMatch(vContent[vLineID], '(object|inherited|inline) \w+ *: *\w+', [roIgnoreCase]) then
      begin
        SetLength(vListElements, Length(vListElements)+1);
        vListElements[High(vListElements)].Reset;

        vListElements[High(vListElements)].FileName := aFileList[vFileID];
        vMatch := TRegEx.Match(vContent[vLineID], '\w+', [roIgnoreCase, roMultiLine]);
        vListElements[High(vListElements)].InlineType := SameText( vMatch.Value, 'inline');
        vMatch := vMatch.NextMatch;
        vListElements[High(vListElements)].Name := vInheritedName + vMatch.Value;
        setInheritedName(vContent[vLineID], vInheritedName);
        vMatch := vMatch.NextMatch;
        vListElements[High(vListElements)].Typ := vMatch.Value;
//        vListElements[High(vListElements)].ReplaceTag := False;
        Continue;
      end;
      vMatch := TRegEx.Match(vContent[vLineID], '\bTag = ', [roIgnoreCase]);
      if vMatch.Success then
      begin
        vSTemp := vContent[vLineID].Substring(vMatch.Index+vMatch.Length-1);
        vListElements[High(vListElements)].Tag := StrToInt(vSTemp);
        Continue;
      end;
      vMatch := TRegEx.Match(vContent[vLineID], '\bCaption = ', [roIgnoreCase]);
      if vMatch.Success then
      begin
        vSTemp := vContent.ValueFromIndex[vLineID].Trim.Substring(1);
        vSTemp := vSTemp.Substring(0, Length(vSTemp)-1);
        vListElements[High(vListElements)].Caption := vSTemp;
        Continue;
      end;
      vMatch := TRegEx.Match(vContent[vLineID], '\bShowCaption = ', [roIgnoreCase]);
      if vMatch.Success then
      begin
        vSTemp := vContent.ValueFromIndex[vLineID];
        vListElements[High(vListElements)].ShowCaption := vSTemp = 'True';
        Continue;
      end;
    end;
  end;
  addArrayToListObjects(vListElements);
  vContent.Free;
end;
procedure TDFMTagger.addArrayToListObjects(const aElements: array of TListObjects);
var
  i: Integer;
  vOne: TListObjects;
  vPattern: string;
begin
  vPattern := createPattern(CP_TYPES);
  for i := 0 to High(aElements) do
  begin
    vOne := aElements[i];
    if (vOne.Tag = 0)
      and vOne.ShowCaption
      and not vOne.InlineType
      and (Length(vOne.Caption)>1)
      and TRegEx.IsMatch(vOne.Typ, vPattern, [roIgnoreCase])
      and  not  (TRegEx.IsMatch(vOne.Caption, '^((grd|176''|181''|ink)?[ 1cmslg�NV/���C]{1,5}(bars|sek|grd|(''#)?176''C)?(''#17)?)$'))
      and  not  (TRegEx.IsMatch(vOne.Caption, '^([\d :,.+-]+ *(ms|%)?|v[123]|m?bar|mBar|OK|[Ss]ek|(I )?min|max|ink|[kK]eine Auswahl)$'))
      and  not  (TRegEx.IsMatch(vOne.Caption, '^(d?[xyzXYZ]{1,3} *([+-]?|Fig\.|Phase\.|Freq\.|Amp\.)|[xX]+|[.]{3,3})$'))
      and  not  (TRegEx.IsMatch(vOne.Caption, '^(GET|SET|dXdY|Bit|&\d)$'))
    then
      fListObjects.Add(vOne);
  end;
end;

procedure TDFMTagger.FormShow(Sender: TObject);
begin
  eDirectorySRCs.Text := fDirSRCs;
  getDFMFilelist;
end;

procedure TDFMTagger.sgResultDblClick(Sender: TObject);
var
  vFileSelected: string;
begin
//  sgResultMouseDown(nil, TMouseButton.mbLeft, [ssShift], 0,0) ;
  if (sgResult.Selection.Left = 5) then
  begin
    vFileSelected := fListObjects.Items[sgResult.Selection.Top-1].FileName;
    ShellExecute(Application.Handle, 'open', PChar(vFileSelected), nil, nil, SW_NORMAL);
  end;
end;

procedure TDFMTagger.sgResultKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #32 then
    sgResultMouseDown(nil, TMouseButton.mbLeft, [ssShift], 0,0);
end;

procedure TDFMTagger.sgResultMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  vSL: TStringList;
  vRow: Integer;
begin
  if (sgResult.Selection.Left = 5) then
    Exit;
  vRow := sgResult.Selection.Top;
  if vRow <= 0 then
    Exit();
  vSL := TStringList.Create;
  toggleReplaceTagValue(vRow - 1);
  vSL.AddStrings(getRowForStringGrid(vRow - 1));
  sgResult.Rows[vRow] := vSL;
  vSL.Free;
  updateCountReplacements;
end;

procedure TDFMTagger.updateCountReplacements;
var
  vCount: Integer;
begin
  vCount := 0;
  fListObjects.ForEach(procedure (const aItem: TListObjects)
    begin
      if aItem.ReplaceTag then
        Inc(vCount);
    end);
  lCountReplacements.Caption := 'zu ersetzen: ' + vCount.ToString;
end;

procedure TDFMTagger.bAllYesOrNoClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to fListObjects.Count-1 do
  begin
    toggleReplaceTagValue(i);
  end;
  showListInStrGrd;
end;

procedure TDFMTagger.bSetToMinusOneClick(Sender: TObject);
var
  vContent: TStringList;
  vLineID, vListNr: Integer;
  vMatch: TMatch;
  vInheritedName: string;
  vPatternName: string;
begin
  if fListObjects.Count = 0 then
    Exit;
  vContent := TStringList.Create;
  try
    for vListNr := 0 to fListObjects.Count - 1 do
    begin
      if not fListObjects.Items[vListNr].ReplaceTag then
        Continue;
      vContent.LoadFromFile(fListObjects.Items[vListNr].FileName);
      vInheritedName := '';
      for vLineID := 0 to vContent.Count - 1 do
      begin
        if TRegEx.IsMatch(vContent[vLineID], '^ *end *$') then
        begin
          vInheritedName := cutLastNameFromInherited(vInheritedName);
          Continue;
        end;

        setInheritedName(vContent[vLineID], vInheritedName);
        if fListObjects.Items[vListNr].Name.StartsWith(vInheritedName) then
          vPatternName := fListObjects.Items[vListNr].Name.Substring
            (Length(vInheritedName));
        if TRegEx.IsMatch(vContent[vLineID], '(object|inherited|inline) *' +
          vPatternName + ' *:') then
        begin
          vMatch := TRegEx.Match(vContent[vLineID + 1], '^ *', [roMultiLine]);
          vContent.Insert(vLineID + 1, vMatch.Value + 'Tag = -1');
          Break;
        end;
      end;
      vContent.SaveToFile(fListObjects.Items[vListNr].FileName);
    end;
  finally
    vContent.Free;
  end;
  lookForObjectsWith0Tags(getSelectedFiles());
  removeObjectsWithValueAssignments();
  showListInStrGrd;
end;

procedure TDFMTagger.bFilterOKFilesClick(Sender: TObject);
var
  vNewList: IList<string>;
  vStartIndex: Integer;
begin
  bLoadFilesSRCs.OnClick(Sender);
  Screen.Cursor := crHourGlass;
  lstFilesSRCs.Items.Clear;
  try
    vNewList := TCollections.CreateList<string>;
    fListObjects.ForEach(procedure (const aItem: TListObjects)
      begin
        if not vNewList.Contains(aItem.FileName) then
          vNewList.Add(aItem.FileName);
      end);
    vStartIndex := Length(fDirSRCs);
    vNewList.ForEach(procedure (const aFile: string)
      begin
        lstFilesSRCs.Items.Add(aFile.Substring(vStartIndex));
      end);
  finally
    Screen.Cursor := crDefault;
  end;
  lCountFilesSRCs.Caption := 'Anzahl: ' + IntToStr(lstFilesSRCs.Count);
end;

procedure TDFMTagger.bLoadFilesSRCsClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    lookForObjectsWith0Tags(getSelectedFiles());
    removeObjectsWithValueAssignments;
    showListInStrGrd;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDFMTagger.getSelectedFiles: TArray<string>;
var
  i: Integer;
  vList: IList<string>;
  procedure _add(const aIdx: Integer);
  begin
    vList.Add(fDirSRCs + lstFilesSRCs.Items[i]);
  end;
begin
  vList := TCollections.CreateList<string>;
  if lstFilesSRCs.SelCount = 0 then
  begin
    for i := 0 to lstFilesSRCs.Items.Count - 1 do
      _add(i);
  end
  else
  begin
    for i := 0 to lstFilesSRCs.Items.Count-1 do
      if lstFilesSRCs.Selected[i] then
        _add(i);
  end;
  Result := vList.ToArray;
end;

{--------- public Methods ----------}

constructor TDFMTagger.Create(const aOwner: TComponent; aDirSRCs: string);
begin
  inherited Create(aOwner);
  fDirSRCs := aDirSRCs;
  fListObjects := TCollections.CreateList<TListObjects>;
end;

destructor TDFMTagger.Destroy;
begin
  inherited;
end;

procedure TDFMTagger.bFilterObjectsByCaptionLengthClick(Sender: TObject);
var
  vTempList: IList<TListObjects>;
begin
  vTempList := TCollections.CreateList<TListObjects>( fListObjects.Where(function (const aEntry: TListObjects): Boolean
    begin
      Result := aEntry.Caption.Length >= seMinLengthObject.Value;
    end));
  fListObjects.Clear;
  fListObjects.AddRange(vTempList);
  showListInStrGrd;
end;

procedure TDFMTagger.bFilterSourcesClick(Sender: TObject);
var
  vPattern: string;
  vStartItems: IList<string>;
begin
  if eFilterSourceFiles.Text = EmptyStr then
    Exit();
  vPattern := eFilterSourceFiles.Text;
  vStartItems := TCollections.CreateList<string>;
  vStartItems.AddRange( lstFilesSRCs.Items.ToStringArray );
  lstFilesSRCs.Items.Clear;
  vStartItems.ForEach(procedure (const aItem: string)
    begin
      if TRegEx.IsMatch(aItem, vPattern, [roIgnoreCase]) then
        lstFilesSRCs.Items.Add(aItem);
    end);
  lCountFilesSRCs.Caption := 'Anzahl: ' + IntToStr(lstFilesSRCs.Count);
  eFilterSourceFiles.SelectAll;
end;

procedure TDFMTagger.bResetFilterSourcesClick(Sender: TObject);
begin
  eFilterSourceFiles.Text := EmptyStr;
  getDFMFilelist;
end;

procedure TDFMTagger.eFilterSourceFilesKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  if Key = VK_RETURN then
    bFilterSources.OnClick(Sender)
  else if Key = VK_ESCAPE then
    bResetFilterSources.OnClick(Sender)
  else if (Key = VK_UP) or (Key = VK_DOWN) then
    lstFilesSRCs.SetFocus
  else
    inherited;
end;

procedure TDFMTagger.SetDirTarget(const aDir: string);
begin
  fDirTarget := aDir;
end;

procedure TDFMTagger.toggleReplaceTagValue(const aNumberListElement: Integer);
var
  vElement: TListObjects;
begin
  vElement := fListObjects.Items[aNumberListElement];
  if vElement.ReplaceTag then
    vElement.ReplaceTag := False
  else
    vElement.ReplaceTag := True;
  fListObjects.Delete(aNumberListElement);
  fListObjects.Insert(aNumberListElement, vElement);
end;

function TDFMTagger.getRowForStringGrid(
  const aNumberListElement: Integer): TArray<string>;
var
  vSL: TStringList;
begin
  vSL := TStringList.Create;
  vSL.Add(IntToStr(aNumberListElement+1));
  with fListObjects do
    begin
      vSL.Add(Items[aNumberListElement].Name);
      vSL.Add(Items[aNumberListElement].Typ);
      vSL.Add(Items[aNumberListElement].Caption);
      vSL.Add(IntToStr(Items[aNumberListElement].Tag));
      vSL.Add(Items[aNumberListElement].FileName.Substring(fDirSRCs.Length));
      if Items[aNumberListElement].ReplaceTag then
        vSL.Add('Ja')
      else
        vSL.Add('Nein');
    end;
  Result := vSL.ToStringArray;
  vSL.Free;
end;

procedure TDFMTagger.showListInStrGrd;
var
  i: Integer;
  vRow: TStringList;
begin
  vRow := TStringList.Create;
  vRow.Add('Nr.'); vRow.Add('Name'); vRow.Add('Typ'); vRow.Add('Caption');
  vRow.Add('Tag'); vRow.Add('Datei'); vRow.Add('ersetze');
  sgResult.CleanupInstance;
  sgResult.Rows[0] := vRow;
  sgResult.RowCount := fListObjects.Count+1;
  for i := 0 to fListObjects.Count-1 do
  begin
    vRow.Clear;
    vRow.AddStrings(getRowForStringGrid(i));
    sgResult.Rows[i+1] := vRow;
  end;
  vRow.Free;
  lCountListObjects.Caption := 'Anzahl: ' + IntToStr(fListObjects.Count);
  updateCountReplacements;
end;


procedure TDFMTagger.LoadINIFile(const aFileName: string);
var
  vINI: TSafeIni;
  vSLTypes: TStringList;
begin
  if not FileExists(aFileName) then
    raise EDFMTagger.Create('ini-File not found');
  vINI := TSafeIni.Create(aFileName);
  vSLTypes := TStringList.Create;
  try
    vINI.ReadSectionValues('DFMTaggingTypes', vSLTypes);
    fINITypes := vSLTypes.ToStringArray;
  finally
    vSLTypes.Free;
    vINI.Free;
  end;
end;

procedure TDFMTagger.lstFilesSRCsKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  if Key = VK_RETURN then
    bLoadFilesSRCs.OnClick(Sender);
end;

{ TDFMTagger.TListObjects }

procedure TDFMTagger.TListObjects.Reset;
begin
  Self.Name := '';
  Self.InlineType := False;
  Self.Typ := '';
  Self.Caption := '';
  Self.Tag := 0;
  Self.ShowCaption := True;
  Self.FileName := '';
  Self.ReplaceTag := False;
end;

end.
