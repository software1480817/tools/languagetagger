unit common.SafeMemIni;

interface

uses
  System.IniFiles,
  System.Classes,
  System.SysUtils;

type

  TSafeIni = class(TCustomIniFile)
  private
    fFileName: string;
    FSections: TStringList;
    FEncoding: TEncoding;
    function AddSection(const Section: string): TStrings;
    procedure LoadValues;
  public
    constructor Create(const FileName: string); overload;
    constructor Create(const FileName: string; Encoding: TEncoding); overload;
    destructor Destroy; override;
    procedure Clear;
    procedure DeleteKey(const Section, Ident: String); override;
    procedure EraseSection(const Section: string); override;
    procedure GetStrings(List: TStrings);
    procedure ReadSection(const Section: string; Strings: TStrings); override;
    procedure ReadSections(Strings: TStrings); override;
    procedure ReadSectionValues(const Section: string; Strings: TStrings); override;
    function ReadString(const Section, Ident, Default: string): string; override;
    procedure Rename(const FileName: string; Reload: Boolean);
    procedure SetStrings(List: TStrings);
    procedure UpdateFile; override;
    procedure WriteString(const Section, Ident, Value: String); override;
    property Encoding: TEncoding read FEncoding write FEncoding;

    function ReadDate(const Section, Name: string; Default: TDateTime): TDateTime; override;
    function ReadDateTime(const Section, Name: string; Default: TDateTime): TDateTime; override;
  end;

implementation

{ TSafeIni }

constructor TSafeIni.Create(const FileName: string);
begin
  Create(FileName, nil);
  fFileName := FileName;
end;

procedure TSafeIni.UpdateFile;
var
  vStream: TFileStream;
  vBuf: TArray<Byte>;
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetStrings(List);
    List.SaveToFile(FFileName, FEncoding);
  finally
    List.Free;
  end;

  vStream := TFileStream.Create(fFileName, fmOpenRead);
  try
    vStream.Read(vBuf, 1);
  finally
    vStream.Free;
  end;
end;

constructor TSafeIni.Create(const FileName: string; Encoding: TEncoding);
begin
  inherited Create(FileName);
  FEncoding := Encoding;
  FSections := THashedStringList.Create;
  LoadValues;
end;

destructor TSafeIni.Destroy;
begin
  if FSections <> nil then
    Clear;
  FSections.Free;
  inherited Destroy;
end;

function TSafeIni.AddSection(const Section: string): TStrings;
begin
  Result := THashedStringList.Create;
  try
    (Result as THashedStringList).CaseSensitive := FALSE;
    FSections.AddObject(Section, Result);
  except
    Result.Free;
    raise;
  end;
end;

procedure TSafeIni.Clear;
var
  I: Integer;
begin
  for I := 0 to FSections.Count - 1 do
  begin
{$IFNDEF NEXTGEN}

    FSections.Objects[I].Free;
{$ENDIF !NEXTGEN}
  end;
  FSections.Clear;
end;

procedure TSafeIni.DeleteKey(const Section, Ident: String);
var
  I, J: Integer;
  Strings: TStrings;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
    Strings := TStrings(FSections.Objects[I]);
    J := Strings.IndexOfName(Ident);
    if J >= 0 then
      Strings.Delete(J);
  end;
end;

procedure TSafeIni.EraseSection(const Section: string);
var
  I: Integer;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
{$IFNDEF NEXTGEN}

    FSections.Objects[I].Free;
{$ENDIF !NEXTGEN}
    FSections.Delete(I);
  end;
end;

procedure TSafeIni.GetStrings(List: TStrings);
var
  I, J: Integer;
  Strings: TStrings;
begin
  List.BeginUpdate;
  try
    for I := 0 to FSections.Count - 1 do
    begin
      List.Add('[' + FSections[I] + ']');
      Strings := TStrings(FSections.Objects[I]);
      for J := 0 to Strings.Count - 1 do List.Add(Strings[J]);
      List.Add('');
    end;
  finally
    List.EndUpdate;
  end;
end;

procedure TSafeIni.LoadValues;
var
  Size: Integer;
  Buffer: TBytes;
  List: TStringList;
  Stream: TFileStream;
begin
  if (FileName <> '') and FileExists(FileName) then
  begin
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
     // Load file into buffer and detect encoding
      Size := Stream.Size - Stream.Position;
      SetLength(Buffer, Size);
      Stream.Read(Buffer[0], Size);
      Size := TEncoding.GetBufferEncoding(Buffer, FEncoding);

      // Load strings from buffer
      List := TStringList.Create;
      try
        List.Text := FEncoding.GetString(Buffer, Size, Length(Buffer) - Size);
        SetStrings(List);
      finally
        List.Free;
      end;
    finally
      Stream.Free;
    end;
  end
  else
    Clear;
end;

function TSafeIni.ReadDate(const Section, Name: string;
  Default: TDateTime): TDateTime;
var
  DateStr: string;
begin
  Result := Default;
  try
    DateStr := ReadString(Section, Name, '');
    if DateStr = '' then
      Exit(Default);
    Result := StrToDate(DateStr);
  except
    on E: EConvertError do
    try
      Result := StrToDate(DateStr, TFormatsettings.Create());
    except
    end
    else
      raise;
  end;
end;

function TSafeIni.ReadDateTime(const Section, Name: string;
  Default: TDateTime): TDateTime;
var
  DateStr: string;
begin
  Result := Default;
  try
    DateStr := ReadString(Section, Name, '');
    if DateStr = '' then
      Exit(Default);
    Result := StrToDateTime(DateStr);
  except
    on E: EConvertError do
      try
        Result := StrToDateTime(DateStr, TFormatsettings.Create());
      except
      end
    else
      raise;
  end;
end;

procedure TSafeIni.ReadSection(const Section: string;
  Strings: TStrings);
var
  I, J: Integer;
  SectionStrings: TStrings;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    I := FSections.IndexOf(Section);
    if I >= 0 then
    begin
      SectionStrings := TStrings(FSections.Objects[I]);
      for J := 0 to SectionStrings.Count - 1 do
        Strings.Add(SectionStrings.Names[J]);
    end;
  finally
    Strings.EndUpdate;
  end;
end;

procedure TSafeIni.ReadSections(Strings: TStrings);
begin
  Strings.Assign(FSections);
end;

procedure TSafeIni.ReadSectionValues(const Section: string;
  Strings: TStrings);
var
  I: Integer;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    I := FSections.IndexOf(Section);
    if I >= 0 then
      Strings.Assign(TStrings(FSections.Objects[I]));
  finally
    Strings.EndUpdate;
  end;
end;

function TSafeIni.ReadString(const Section, Ident,
  Default: string): string;
var
  I: Integer;
  Strings: TStrings;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
    Strings := TStrings(FSections.Objects[I]);
    I := Strings.IndexOfName(Ident);
    if I >= 0 then
    begin
      Result := Strings[I].SubString( Ident.Length + 1);
      Exit;
    end;
  end;
  Result := Default;
end;

procedure TSafeIni.Rename(const FileName: string; Reload: Boolean);
begin
  FFileName := FileName;
  if Reload then
    LoadValues;
end;

procedure TSafeIni.SetStrings(List: TStrings);
var
  I, J: Integer;
  S: string;
  Strings: TStrings;
begin
  Clear;
  Strings := nil;
  for I := 0 to List.Count - 1 do
  begin
    S := List[I];
    if (S <> '') and (S.Chars[0] <> ';') then
      if (S.Chars[0] = '[') and (S.Chars[S.Length-1] = ']') then
      begin
        // Remove Brackets
        Strings := AddSection(S.Substring(1, S.Length-2));
      end
      else
        if Strings <> nil then
        begin
          J := S.IndexOf('=');
          if J >= 0 then
            Strings.Add(S.SubString(0, J) + '=' + S.SubString( J+1))
          else
            Strings.Add(S);
        end;
  end;
end;

procedure TSafeIni.WriteString(const Section, Ident, Value: String);
var
  I: Integer;
  S: string;
  Strings: TStrings;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
    Strings := TStrings(FSections.Objects[I])
  else
    Strings := AddSection(Section);
  S := Ident + '=' + Value;
  I := Strings.IndexOfName(Ident);
  if I >= 0 then
    Strings[I] := S
  else
    Strings.Add(S);
end;

end.
