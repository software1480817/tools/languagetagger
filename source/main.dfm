object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'LanguageTagger'
  ClientHeight = 341
  ClientWidth = 962
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 978
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = mMain
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlRight: TPanel
    Left = 468
    Top = 0
    Width = 494
    Height = 341
    Align = alRight
    TabOrder = 0
    DesignSize = (
      494
      341)
    object lDispLanguageFile: TLabel
      Left = 8
      Top = 8
      Width = 144
      Height = 13
      Caption = 'Verzeichnis der Sprachdateien'
    end
    object lCountTagsLangF: TLabel
      Left = 317
      Top = 5
      Width = 129
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Anzahl Tags Sprachdatei: -'
    end
    object lDescriptionAlreadyUsedTags: TLabel
      Left = 8
      Top = 95
      Width = 118
      Height = 26
      Caption = 'Liste der bereits in der Datei verwendeten Tags'
      WordWrap = True
    end
    object lDescriptionGapList: TLabel
      Left = 145
      Top = 56
      Width = 113
      Height = 65
      Caption = 
        'L'#252'cken in der Tag-Liste der Sprachdatei entsprechend der gefunde' +
        'nen zu ersetzenden Ausdr'#252'cke'
      WordWrap = True
    end
    object lTagsFreeBehind: TLabel
      Left = 281
      Top = 222
      Width = 50
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Freie Tags'
      ExplicitTop = 223
    end
    object lDispDirTarget: TLabel
      Left = 8
      Top = 244
      Width = 89
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Ordner Zieldateien'
      ExplicitTop = 245
    end
    object imgOurplant: TImage
      Left = 287
      Top = 95
      Width = 202
      Height = 82
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000C80000
        005008060000005C6D3AB0000000017352474200AECE1CE90000000467414D41
        0000B18F0BFC6105000000097048597300000EC300000EC301C76FA86400000C
        114944415478DAED5D0BAC5D45151DDA579E5491A0A885AA1111F9B5201569D0
        2A0D1F6BF93D20500A486CC11A440129A2F44BC15250508905420394F26BADE0
        AFFC62C534E0AF0522FEB005514AA34211954A81283EEA5ACE9C3A6FDF99B9E7
        9C7BEFB9F7BDB757B233C9993DFBCC993BEBCCCC9E3DE76E63140A4514DBB4BB
        020A45274309A25024A00451281250822814092841148A0494200A45024A1085
        220125884291801244A1484009A25024A00451281250822814092841148A0494
        200A45024A1085220125884291801244A1484009A218F49873C9822148464336
        7E69EECC67FD3C258862D0018460BFDF1D7238E410C847204F42C68120BDBEAE
        1244312800520C43321ED2033902B2AB97FD1AE40090E351594E09A218B00029
        981C08990A3901B25344F57690E3E3A10C258862C001C4188A6412E47CC8FBEB
        A86F818C06411E0B652A411403066EC49808B912B277CE622B418E09B14C2588
        624000E4D801C93590530B163D1604F97E2C5309A2E8F70039B8E0BE17B267C1
        A27F82EC0A82FC27A6A00451F46B801CEF40F213C83B4B14BF18E49897525082
        28FA2D408E6D91FC14724089E2AF1A3B7AFC39A5A40451F45B80205F40F2E592
        C56F03394EABA7A40451B41DCE2DCB508F4D90E7202FA1F3D62B331CC906C89B
        4BDC92BBE5FBC55CBB3E94208A8E003A3C7DB4338CDD97781EB216B21A721FE4
        4174E6D784FE4948BE59F27637C0DEB43C8A4A9016033F64379213035977E047
        FA57BBEBD7C073752109ED3EDF8CE7DA52D2DE4390FD03D9BF87CC867C2B1B59
        A07F3D924F96A83A831147C1CEDFF2282B415A0CFC909C023C1FC8DA29EF8FD4
        89C0736D87E4E540D650F9B62F60731C92074DBC5F7E03722E49025D8E2E630B
        DE820BF30928BF2A6F0125488BA104296C771992C9099533607F31F4FE68FA06
        1CD603C9711ACA2E2F529F24415C231C0F39D2D89816FA9C5F07D90C79CAD821
        7105E4BED466CB60C6202448379EEBDF0DD87D2B925F40464654488CF7409E36
        B63FE6C13390538B8C1C198204715E85CF426641DE92C3CE7AC85C635D6785E7
        9F03194A9052B6B923CEF08FF746547683DC0DD9AB8EA997208B20F351A77F94
        A94B0D41503912E24E630F91140547130E63FF6CA481061294200DD9E7229C8E
        8031902E2FFBC354817C345094AE626E1EDE05595E961819FA10C4FD985C2485
        2221B9FAE7C2E805C82E900F42DE10D07B1872588824B0CFC32A078ACBF7C786
        3EE8D3FE0C7199F3DBB9FE4805BD83238D25C1C67A04F26379720C3678C26C4A
        CE766307E0B0FD1BDA83AD57638A7909E29EA127E7FDD9066CDF0DEE791E4B8D
        DCB0CD39FDE89CB6E959FB2BE409C81AD8DD1CB11923C84DAE7E19D8B157C3CE
        7539EF1FBA17FB0147134EBB1894B8D258D28C72F5659F64FFFC03647D236B20
        89AD0471C710E97396A1BF1B21E719EB62EBF5F459E9738C9D5A758B32DF851C
        2F377B50E65A249F96CF0FBDF99186799B7B701F5CEB74FB8D003D92684181E7
        5E07990C1BBFF26C7CCC3D7F51FCDDD84E71596844284090E948BE5AE2FED9F3
        B01DEF8CB423F70B4E2A6197045802B910B65F14366304096119CA9F52F2D9DA
        0A9F200C13BE4DE4732174301EEEE99801F7E663C7DA4E649D8072DF16BA0B8D
        5DDBF4B9DC0682101C01F6CC46BA0608E2DBEB81BD87C5335441900CECC835A1
        170D1024C3CF20E3FD91725011C48D1EBF357DA756EC803CC4FEF37A4650FE2C
        6363F17DFC12B2BF3F8A544C100EBD2FBB67DC1E3234708B69B07383B3112208
        3BC40BE21A3BC6EB4DD8C1C1D1648CFF42294010B6CB65F5DADA819EC4AEC075
        B6CDBEB0BB56B46388207CB65E718D6D342C72CFA9B0BBC4B3192348684AC6D9
        C719399FAD2E9C13A9AB8A8DD68C20EF43220FACDF8D0A1C5DA0C2FC2AC4BB44
        D6283FDE057A5F3376BAD6A7788B08722D743EE3F24990C5C69E4BF6B1103AE7
        389D10415621FF10512726238C757FCF33B55E3EEE904FF2F49BBE4877BBCE9C
        83B3E3CBBD8005B03B4BE88708F2BFFD04A1C7FE4017EA52531B21BB02FA3D9E
        6E4BF641723E3F9F999FE8C93B8295464610BEBD168ABCD351819B0A549AD383
        E9E2F299B0B1C8D3690B419CCE51C67A367C2C82CE992E3F174144FDD899B817
        B4A377996FE59128B7D1E9B4CC8B05DBE722B94A5CBE07768F127AB908E2E91F
        63AC9BD5C713D0DFC3D3692741F8E2BE573A5A5A818C20A18E7B102AB0BA40A5
        4F4772A3B87C396CCCF074AE40F27959B42282F01B482B633A6508E2CA5D64EC
        48E2E3C46CC15C9420EE8C034707764696CD1C20FCADE434915E9C49E2DA03B0
        3B5ED82C4A907D8C9D72FB7806FABB783AED24C84CDCA3E89AB314328284BC4B
        6342DF094A543AB4C8FF3A6C4CF774BE82E40259B42282D03D288F6472985EEF
        F2CB12E443C69E68F3B175B15C600DC2CECFE7E08BEA4D79DB3D80661084EDB4
        565C7E16FA3B7B3A6D2188FB0AE252DC6372C3C6722023081787178A3C7A6456
        14A83857E3978ACBB361E3524FE772245F9445AB20488EFA972508FDF38F8BCB
        5B8F72162048E8255506039D20FC3D4EC13DCA44F2164646109EACBA45E45D85
        4A9C97D7106CFCC8D8CF38FAE84332E8902CF2244C7F2708234AE554F40294BB
        D2E5D725885B74D2C931246F7D13683741BA5AB536700E92EF191BFBB7A84173
        B99011843B94DC95F57F20FE78EFCE133682F2FB1AEB05F3CBBF0219E197871E
        89304B14BF083A9744ECF6078230E441D6FF1894BBCBE5E72108C3296E15F9B7
        38BBD489BD91F909CD65E25ABB09B27D6CF7BD51E09E0C98A553646FDCE3F146
        EDE581BF51788FB10DEE837B04D352C71F518E3EF9074C6D08C98D72188C74E4
        3E6E51A1DFD10441192E92B9FED8C1BBCC17C3CE28B7C9E9E421C8D9C69E75F0
        3116F90FD5A973C8DBD40C82D041B04E5CCE4B90C3A1777FDE76CF0BB7F66018
        14DDEA7BD43B92DB2CF804213BD7985A4F0917D63343C3A65BF8F20D365164F1
        0DB20FCA6C10FAC71A1B86E28376D9C1E97BE77964FF6D4982C8AF4E54491012
        7F82BB6706760CC66D1D07F99CB19B903EAEC67DCFF6ECE621C864533B122C31
        76B44DB982E9CE95E125CD2008A3659F14972541D877F83B0F177A8C87E25A8A
        7D29DBC8EB6DF43804EEC7E93EBDADD119472B20831579E33901BD475DE51854
        C880BFB71B7B4684DF3E0DC5ED7F020F21D734B4CF1D68EE32C70EDA87E6AE92
        B05512648B27198698F8391A0EFB63B3D1C3D9CD431076BCF5906D037AF516BC
        72DD5209419CDE0F911C16A997DF6EB7A2EC145312B80F0363B380567EAAE72F
        656D158524081B9BD3AAA989325B4CBC83308F5EAA2B624320EEC1E914478BA1
        A61CAA2448113006EB38F99DA5025EACD07E4A195449101E89A0732614F6E283
        6ED9A29F04CDEEC1F55036B5BA0E769AE1E9CB8DD079105EA3CB779E09BFD162
        601C1277CEEFC8F1D0871A3B751B53A2CE9D461086BC5F0D591C9A4614200893
        4F19FB7182BC27E542A88C204E97BBDAD7D4A973A96045D86688FE0F20BC2F63
        E2F692FF00D56A448FDCA2723CADC5B71A638E86256C703AC186E6AEF973051E
        9E094335B8F6E167233997AFF72622488C39E23CC878537B1E644DEAA3C481FA
        8408C2E9E05271ADD73D33F37816E3A93A4E0CCED16707B2E6876289DC86E17E
        C69EDF60C7189EB35D32B03ED70B9B279BDAF320DF81DE23913A93D4E78BCB9B
        63BBD7EECF69C6B97AB3AC9C1DFC1A650B7DA207363985BFDDFCDF015228F4A9
        59A8FBD10677C2909D2F3B93CE452A5DB73C1BCC48DF5555048DB51A65DDBC8A
        E6C279C7B81D400748B6BEBA1932A52ACF950FFDAA898312A4BD70537B7A3919
        AFB79B97C529564FBBBE21A604715082B4076E7A46973963F464883DA7C827E3
        3778A55DF55382382841AA833BCF7290B1EB5BEE018D102A5CE7312CE9E25647
        06D78312C44109D23C38070CD70FF4823232997B659C36318CFE03C692E38D91
        E2FC26D65968F735ED7E0EA2E904716F076EEC1C616AFF6EB79341CF8B3C574F
        B7ED8B256C5505B675A7BEE458B7EE02F5E3116DBAFE97B77BD4F0D1D2C6750B
        2F6EF46464A12BB0C8DE8A6260837F81C67506DDB9AB3BF1A38395BE7D40180E
        AB0C4D98E86464631615FD04FC400437F8B835F03B63A751FCB8DBBA4E24858F
        B60DCF6E74E1BCB4BB515B8A8E043B3EF7C7B867B6A98AF3E3AD40A7CE5F158A
        8E801244A1484009A25024A00451281250822814092841148A0494200A45024A
        1085220125884291801244A1484009A25024A00451281250822814092841148A
        0494200A45024A1085220125884291C07F013A53A09C6BF582BA000000004945
        4E44AE426082}
    end
    object eDirLangFiles: TEdit
      AlignWithMargins = True
      Left = 8
      Top = 24
      Width = 451
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      Text = 'C:\OurPlant\VicoBase.V15\trunk\Sprache'
      OnKeyPress = eDirLangFilesKeyPress
    end
    object bSelLanguageFile: TButton
      Left = 461
      Top = 24
      Width = 25
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 1
      OnClick = bSelLanguageFileClick
    end
    object lstTagsAlreadyUsed: TListBox
      Left = 8
      Top = 127
      Width = 129
      Height = 72
      TabStop = False
      Anchors = [akLeft, akTop, akBottom]
      ItemHeight = 13
      TabOrder = 2
      OnMouseEnter = SetFocusOnMouseEnter
    end
    object lstGaps: TListBox
      Left = 145
      Top = 127
      Width = 128
      Height = 72
      TabStop = False
      Anchors = [akLeft, akTop, akBottom]
      ItemHeight = 13
      TabOrder = 3
      OnClick = lstGapsClick
      OnKeyPress = lstGapsKeyPress
      OnMouseEnter = SetFocusOnMouseEnter
    end
    object bLoadLangFile: TButton
      Left = 349
      Top = 51
      Width = 137
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Sprachdatei laden'
      TabOrder = 4
      OnClick = bLoadLangFileClick
    end
    object chkPreferLastTags: TCheckBox
      Left = 278
      Top = 182
      Width = 138
      Height = 17
      TabStop = False
      Anchors = [akLeft, akBottom]
      Caption = 'bevorzuge letzten Tag'
      TabOrder = 5
    end
    object chkShowHints: TCheckBox
      Left = 8
      Top = 221
      Width = 97
      Height = 17
      TabStop = False
      Anchors = [akLeft, akBottom]
      Caption = 'zeige Tooltips'
      TabOrder = 6
    end
    object eBeginTag: TLabeledEdit
      Left = 145
      Top = 219
      Width = 128
      Height = 21
      TabStop = False
      Anchors = [akLeft, akBottom]
      EditLabel.Width = 45
      EditLabel.Height = 13
      EditLabel.Caption = 'Start Tag'
      Enabled = False
      TabOrder = 7
      OnChange = eBeginTagChange
      OnKeyPress = eBeginTagKeyPress
    end
    object eDirTarget: TEdit
      Left = 8
      Top = 260
      Width = 451
      Height = 21
      Anchors = [akLeft, akRight, akBottom]
      TabOrder = 8
      Text = 'Ordner Zieldateien'
      OnChange = eDirTargetChange
      OnKeyPress = eDirTargetKeyPress
    end
    object bSelDirTarget: TButton
      Left = 461
      Top = 260
      Width = 25
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = '...'
      TabOrder = 9
      OnClick = bSelDirTargetClick
    end
    object bSetTags: TButton
      Left = 8
      Top = 287
      Width = 122
      Height = 40
      Anchors = [akLeft, akBottom]
      Caption = 'schreibe Tags'
      Enabled = False
      TabOrder = 10
      OnClick = bSetTagsClick
    end
    object chkOverWriteFiles: TCheckBox
      Left = 159
      Top = 288
      Width = 296
      Height = 17
      TabStop = False
      Anchors = [akLeft, akBottom]
      Caption = 'vorhandene Dateien in Zielverzeichnis '#252'berschreiben'
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
    object chkOverWriteOrigins: TCheckBox
      Left = 157
      Top = 309
      Width = 216
      Height = 17
      TabStop = False
      Anchors = [akLeft, akBottom]
      Caption = #252'berschreibe Originaldateien'
      TabOrder = 12
      OnClick = chkOverWriteOriginsClick
    end
  end
  object pnlLeft: TPanel
    Left = 0
    Top = 0
    Width = 468
    Height = 341
    Align = alClient
    TabOrder = 1
    object pnlLeftTop: TPanel
      Left = 1
      Top = 1
      Width = 466
      Height = 54
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lDirectorySRCs: TLabel
        Left = 8
        Top = 5
        Width = 125
        Height = 13
        Caption = 'Ordner Quellcode-Dateien'
      end
      object lShowFiles: TLabel
        Left = 287
        Top = 5
        Width = 69
        Height = 13
        Caption = 'zeige Dateien:'
      end
      object chkPAS: TCheckBox
        Left = 364
        Top = 4
        Width = 40
        Height = 17
        Caption = 'PAS'
        TabOrder = 0
        OnClick = chkDFMClick
        OnExit = chkDFMClick
      end
      object chkDFM: TCheckBox
        Left = 410
        Top = 4
        Width = 41
        Height = 17
        Caption = 'DFM'
        TabOrder = 1
        OnClick = chkDFMClick
        OnExit = chkDFMClick
      end
      object eDirectorySRCs: TEdit
        Left = 8
        Top = 24
        Width = 419
        Height = 21
        TabOrder = 2
        Text = 'Ordner Quelldateien'
        OnKeyPress = eDirectorySRCsKeyPress
      end
      object bSelDirSRCs: TButton
        Left = 433
        Top = 24
        Width = 25
        Height = 21
        Caption = '...'
        TabOrder = 3
        OnClick = bSelDirSRCsClick
      end
    end
    object pnlLeftBottom: TPanel
      Left = 1
      Top = 299
      Width = 466
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        466
        41)
      object lCountFoundExpressionInSRC: TLabel
        Left = 299
        Top = 14
        Width = 124
        Height = 13
        Alignment = taRightJustify
        Anchors = [akLeft, akBottom]
        Caption = 'gefundenen Ausdr'#252'cke: 0'
      end
      object bSelectFiles: TButton
        Left = 8
        Top = 7
        Width = 161
        Height = 27
        Anchors = [akLeft, akBottom]
        Caption = 'suche in ausgew'#228'hlten Dateien'
        Enabled = False
        TabOrder = 0
        WordWrap = True
        OnClick = bSelectFilesClick
      end
      object bFilterFilesToBeProcessed: TButton
        Left = 175
        Top = 9
        Width = 120
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Filter zu bearbeitende'
        TabOrder = 1
        OnClick = bFilterFilesToBeProcessedClick
      end
    end
    object pnlCenter: TPanel
      Left = 1
      Top = 55
      Width = 466
      Height = 244
      Align = alClient
      BevelOuter = bvNone
      Padding.Left = 8
      Padding.Top = 8
      Padding.Right = 8
      Padding.Bottom = 8
      TabOrder = 2
      object lstFilesSRC: TListBox
        Left = 8
        Top = 8
        Width = 450
        Height = 228
        Align = alClient
        BevelOuter = bvNone
        ItemHeight = 13
        MultiSelect = True
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        OnDblClick = bSelectFilesClick
        OnKeyPress = lstFilesSRCKeyPress
        OnMouseDown = lstFilesSRCMouseDown
        OnMouseEnter = SetFocusOnMouseEnter
        OnMouseLeave = lstFilesSRCMouseLeave
        OnMouseMove = lstFilesSRCMouseMove
      end
    end
  end
  object jvSelDirSRCs: TJvSelectDirectory
    Left = 824
  end
  object selDirTarget: TJvSelectDirectory
    Left = 600
    Top = 51
  end
  object popupMenu: TPopupMenu
    Left = 904
    object popSelAll: TMenuItem
      Caption = 'alle ausw'#228'hlen'
      OnClick = popSelAllClick
    end
    object bereitsgenutzteTags1: TMenuItem
      Caption = 'zeige bereits genutzte Tags'
      OnClick = popBereitsGenutzteTags1Click
    end
    object ffneDatei1: TMenuItem
      Caption = #246'ffne Datei'
      OnClick = ffneDatei1Click
    end
  end
  object mMain: TMainMenu
    Left = 792
    Top = 65528
    object Settings: TMenuItem
      Caption = 'Einstellungen'
      OnClick = SettingsClick
    end
    object UngenutzteTags1: TMenuItem
      Caption = 'Ungenutzte Tags'
      OnClick = UngenutzteTags1Click
    end
    object DFMTagger1: TMenuItem
      Caption = 'DFM-Tagger'
      object ag01setzen1: TMenuItem
        Caption = 'Tag 0 --> -1 setzen'
        OnClick = Tag01setzen1Click
      end
    end
    object SprachdateienSynchronisieren1: TMenuItem
      Caption = 'Sprachdateien Synchronisieren'
      OnClick = SprachdateienSynchronisieren1Click
    end
    object Sprachdateieditor1: TMenuItem
      Caption = 'Sprachdateieditor'
      OnClick = Sprachdateieditor1Click
    end
    object Info1: TMenuItem
      Caption = 'Info'
      OnClick = Info1Click
    end
  end
  object selDirLangFiles: TJvSelectDirectory
    Left = 656
    Top = 48
  end
end
