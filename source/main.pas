﻿unit main;

interface

{$REGION 'uses'}
uses
  packages.tools.VersionInformation,
  dfmTagging,
  Info,
  replaceTags,
  FormshowAlreadyUsedTags,
  unusedTags,
  settings,
  JvBaseDlg,
  JvSelectDirectory,
  Winapi.Windows,
  Winapi.Messages,
  system.Types,
  System.IOUtils,
  System.SysUtils,
  System.UITypes,
  System.Variants,
  System.Classes,
  System.Generics.Collections,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  Vcl.Menus,
  Vcl.ExtDlgs,
  Vcl.ExtCtrls,
  Vcl.Imaging.jpeg,
  Vcl.Imaging.pngimage;
{$ENDREGION}

{$Define _OGINFO}  // rename to LOGINFO

type

  TForm1 = class(TForm)
    jvSelDirSRCs: TJvSelectDirectory;
    selDirTarget: TJvSelectDirectory;
    popupMenu: TPopupMenu;
    popSelAll: TMenuItem;
    mMain: TMainMenu;
    Settings: TMenuItem;
    UngenutzteTags1: TMenuItem;
    DFMTagger1: TMenuItem;
    ag01setzen1: TMenuItem;
    ffneDatei1: TMenuItem;
    selDirLangFiles: TJvSelectDirectory;
    SprachdateienSynchronisieren1: TMenuItem;
    Sprachdateieditor1: TMenuItem;
    Info1: TMenuItem;
    pnlRight: TPanel;
    lDispLanguageFile: TLabel;
    lCountTagsLangF: TLabel;
    eDirLangFiles: TEdit;
    bSelLanguageFile: TButton;
    lDescriptionAlreadyUsedTags: TLabel;
    lstTagsAlreadyUsed: TListBox;
    lstGaps: TListBox;
    lDescriptionGapList: TLabel;
    bLoadLangFile: TButton;
    chkPreferLastTags: TCheckBox;
    chkShowHints: TCheckBox;
    eBeginTag: TLabeledEdit;
    lTagsFreeBehind: TLabel;
    lDispDirTarget: TLabel;
    eDirTarget: TEdit;
    bSelDirTarget: TButton;
    bSetTags: TButton;
    chkOverWriteFiles: TCheckBox;
    chkOverWriteOrigins: TCheckBox;
    imgOurplant: TImage;
    pnlLeft: TPanel;
    pnlLeftTop: TPanel;
    lDirectorySRCs: TLabel;
    lShowFiles: TLabel;
    chkPAS: TCheckBox;
    chkDFM: TCheckBox;
    eDirectorySRCs: TEdit;
    bSelDirSRCs: TButton;
    pnlLeftBottom: TPanel;
    bSelectFiles: TButton;
    bFilterFilesToBeProcessed: TButton;
    lCountFoundExpressionInSRC: TLabel;
    pnlCenter: TPanel;
    lstFilesSRC: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bSelectFilesClick(Sender: TObject);
    procedure lstGapsClick(Sender: TObject);
    procedure eBeginTagChange(Sender: TObject);
    procedure bSetTagsClick(Sender: TObject);
    procedure bSelDirSRCsClick(Sender: TObject);
    procedure bSelLanguageFileClick(Sender: TObject);
    procedure eDirLangFilesKeyPress(Sender: TObject; var Key: Char);
    procedure eDirectorySRCsKeyPress(Sender: TObject; var Key: Char);
    procedure bSelDirTargetClick(Sender: TObject);
    procedure eDirTargetKeyPress(Sender: TObject; var Key: Char);
    procedure lstFilesSRCKeyPress(Sender: TObject; var Key: Char);
    procedure eDirTargetChange(Sender: TObject);
    procedure lstFilesSRCMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure popSelAllClick(Sender: TObject);
    procedure popBereitsGenutzteTags1Click(Sender: TObject);
    procedure lstFilesSRCMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure lstFilesSRCMouseLeave(Sender: TObject);
    procedure bLoadLangFileClick(Sender: TObject);
    procedure SettingsClick(Sender: TObject);
    procedure UngenutzteTags1Click(Sender: TObject);
    procedure chkOverWriteOriginsClick(Sender: TObject);
    procedure Tag01setzen1Click(Sender: TObject);
    procedure chkDFMClick(Sender: TObject);
    procedure bFilterFilesToBeProcessedClick(Sender: TObject);
    procedure lstGapsKeyPress(Sender: TObject; var Key: Char);
    procedure ffneDatei1Click(Sender: TObject);
    procedure eBeginTagKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Info1Click(Sender: TObject);
    procedure SprachdateienSynchronisieren1Click(Sender: TObject);
    procedure Sprachdateieditor1Click(Sender: TObject);
    procedure SetFocusOnMouseEnter(Sender: TObject);

  private
    fReplaceTags : TReplaceTags;
    fCountFoundExp: Integer;
    fIDMouseHint: Integer;
    fInfoRecord: TExtractFileVersionData;
    fDeveloperVersionData: TDeveloperVersionData;

    procedure applySettings;
    procedure getSettings;
    procedure showGapsInTaglist;
    procedure checkBoxSet(const aChkBx: TCheckBox; aState: Boolean);
    procedure listFilesSRCs;
    procedure loadLangFile;
    function readyForProcessing: Boolean;
    function selectBestTag(const aListTagsUsed, aListGaps: TArray<Integer>): Integer;

  public
    property ReplaceTags: TReplaceTags read fReplaceTags;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

  // returning Version information from gitversion.inc
  function GetVersionInformation: TExtractFileVersionData;
  function GetVersionNumber: String;

var
  Form1: TForm1;

implementation

{$REGION 'uses'}
uses
{$ifdef LOGINFO }
  nxLogging,
{$endif }
  Winapi.ShellAPI,
  LangFileEditor;
{$ENDREGION}

{$R *.dfm}

function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations;
end;

function GetVersionNumber: string;
begin
  Result := GetVersionInformation.FileVersionData.FileVersionNr;
end;

//{$I gitversion.inc}

procedure TForm1.listFilesSRCs;
var
  vSL: TStringList;
  i: Integer;
begin
  vSL := TStringList.Create;
  lstFilesSRC.Clear;
  fReplaceTags.LoadFilesSourcesFromPath(eDirectorySRCs.Text);
  vSL.AddStrings(fReplaceTags.GetFilesSources);
  for i := 0 to vSL.Count-1 do
    lstFilesSRC.Items.Add(vSL[i].Substring(Length(eDirectorySRCs.Text)));
  lCountFoundExpressionInSRC.Caption := 'gefundenen Ausdrücke: 0';
  if vSL.Count > 0 then
    bSelectFiles.Enabled := True
  else
    bSelectFiles.Enabled := False;
  vSL.Free;
end;

procedure TForm1.popBereitsGenutzteTags1Click(Sender: TObject);
var
  vNewForm : TFormalreadyUsed;
  i: Integer;
  vPASFile: string;
begin
  if lstFilesSRC.Count = 0 then
    Exit;
  if not fReplaceTags.loadExpressionsFile() then
  begin
    ShowMessage('Expressions-File nicht gefunden.');
    Exit;
  end;
  for i := 0 to lstFilesSRC.Count-1 do
    if lstFilesSRC.Selected[i] then
    begin
      vNewForm := TFormalreadyUsed.Create(Self, fReplaceTags);
      try
        vPASFile := eDirectorySRCs.Text + lstFilesSRC.Items[i];
        vNewForm.SetPASFileUsedTags(vPASFile);
        vNewForm.ShowModal;
      finally
        vNewForm.Free;
      end;
      Break;
    end;
end;

procedure TForm1.Tag01setzen1Click(Sender: TObject);
var
  vDFMTagger: TDFMTagger;
begin
  getSettings;
  vDFMTagger := TDFMTagger.Create(Self, fReplaceTags.settings.Sources);
  try
    vDFMTagger.SetDirTarget(fReplaceTags.settings.DirectoryTarget);
    vDFMTagger.LoadINIFile(fReplaceTags.settings.Expressions);

    vDFMTagger.ShowModal;
  finally
    vDFMTagger.Free;
  end;
end;

procedure TForm1.checkBoxSet(const aChkBx: TCheckBox; aState: Boolean);
var
  vEventClick: procedure (Sender: TObject) of object;
  vEventExit: procedure (Sender: TObject) of object;
begin
  if (aState = aChkBx.Checked) then
    Exit;
  vEventClick := aChkBx.OnClick;
  aChkBx.OnClick := nil;
  vEventExit := aChkBx.OnExit;
  aChkBx.OnExit := nil;

  aChkBx.Checked := aState;

  aChkBx.OnClick := vEventClick;
  aChkBx.OnExit := vEventExit;
end;

procedure TForm1.chkDFMClick(Sender: TObject);
begin
  getSettings;
  listFilesSRCs;
end;

procedure TForm1.getSettings;
begin
  with fReplaceTags.Settings do
  begin
    Sources := eDirectorySRCs.Text;
    LanguagePath := eDirLangFiles.Text;
    DirectoryTarget := eDirTarget.Text;
    OverWrite := chkOverWriteFiles.Checked;
    OverWriteOrigins := chkOverWriteOrigins.Checked;
    DFMSearch := chkDFM.Checked;
    PASSearch := chkPAS.Checked;
    PreferLastTag := chkPreferLastTags.Checked;
  end;
end;

procedure TForm1.applySettings;
begin
  fReplaceTags.loadExpressionsFile();
  with fReplaceTags.Settings do
  begin
    checkBoxSet(chkDFM, DFMSearch);
    checkBoxSet(chkPAS, PASSearch);
    checkBoxSet(chkPreferLastTags, PreferLastTag);
    checkBoxSet(chkOverWriteFiles, OverWrite);
    checkBoxSet(chkOverWriteOrigins, OverWriteOrigins);
    eDirectorySRCs.Text := Sources;
    eDirTarget.Text := DirectoryTarget;
    eDirLangFiles.Text := LanguagePath;
  end;
end;

procedure TForm1.bFilterFilesToBeProcessedClick(Sender: TObject);
var
  i: Integer;
  vSL: TStringList;
begin
  vSL := TStringList.Create;
  try
    lstFilesSRC.SelectAll;
    bSelectFilesClick(nil);
    lstFilesSRC.Clear;
    fReplaceTags.FilterFilesToProcess;
    vSL.AddStrings(fReplaceTags.GetFilesSources);
    for i := 0 to vSL.Count-1 do
      lstFilesSRC.Items.Add(vSL[i].Substring(Length(eDirectorySRCs.Text)));
  finally
    vSL.Free;
  end;
  lstFilesSRC.ClearSelection;
end;

procedure TForm1.bLoadLangFileClick(Sender: TObject);
begin
  fReplaceTags.loadExpressionsFile();
  loadLangFile;
end;

procedure TForm1.bSelDirSRCsClick(Sender: TObject);
begin
  jvSelDirSRCs.InitialDir := eDirectorySRCs.Text;
  if not jvSelDirSRCs.Execute then
    Exit;
  eDirectorySRCs.Text := jvSelDirSRCs.Directory;
  listFilesSRCs;
end;
procedure TForm1.eDirectorySRCsKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key = #13) then
    Exit;
  if not DirectoryExists(eDirectorySRCs.Text) then
  begin
    ShowMessage('Ordner existiert nicht');
    Exit;
  end;
  listFilesSRCs;
  readyForProcessing;
end;


procedure TForm1.bSelectFilesClick(Sender: TObject);
var
  i ,vSelectedTagID: Integer;
  vUsedTags: TList<Integer>;
  vListSelectedFiles: TList<Integer>;
begin
  if not fReplaceTags.loadExpressionsFile() then
  begin
    raise Exception.Create('Expressions.ini - Datei nicht gefunden (es wird im EXE-Verzeichnis gesucht)');
    Exit;
  end;
  if lstFilesSRC.Count = 0 then
    begin
      ShowMessage('Keinen Ordner gewählt oder keine *.pas-Dateien im Ordner enthalten.');
      Exit;
    end;
  vListSelectedFiles := TList<Integer>.Create;
  for i := 0 to lstFilesSRC.Count-1 do
    if lstFilesSRC.Selected[i] then
      vListSelectedFiles.Add(i);
  if vListSelectedFiles.Count = 0 then
  begin
    i := MessageDlg('keine Dateien ausgewählt..' + sLineBreak + 'Sollen alle Dateien verwendet werden?', mtConfirmation, mbYesNo, 0, mbYes);
    if i = mrYes then
      fReplaceTags.SelectFilesFromSourcesALL
    else
      Exit;
  end
  else
    fReplaceTags.SelectFilesFromSources(vListSelectedFiles.ToArray);
  fCountFoundExp := fReplaceTags.searchExpressionsInSRC();
  lCountFoundExpressionInSRC.Caption := 'gefundenen Ausdrücke: '  + IntToStr(fCountFoundExp);
  showGapsInTaglist;
  vUsedTags := TList<Integer>.Create;
  vUsedTags.AddRange(fReplaceTags.ListOfUsedTagsInFiles);
  lstTagsAlreadyUsed.Clear;
  for i := 0 to vUsedTags.Count-1 do
    lstTagsAlreadyUsed.Items.Add(IntToStr(vUsedTags.Items[i]));
  lstGaps.ClearSelection;
  if (fCountFoundExp > 0)
  and fReplaceTags.IsLanguageFileLoaded then
  begin
    if not chkPreferLastTags.Checked then
      vSelectedTagID := selectBestTag(vUsedTags.ToArray, fReplaceTags.showGapsOfXTags)
    else
      vSelectedTagID := lstGaps.Count-1;
    lstGaps.Selected[vSelectedTagID] := True;
    eBeginTag.Text := lstGaps.Items[vSelectedTagID];
  end;
  vUsedTags.Free;
  readyForProcessing;
  vListSelectedFiles.Free;
end;

function TForm1.selectBestTag(const aListTagsUsed, aListGaps: TArray<Integer>): Integer;
var
  vListGaps: array of array[0..2] of Integer;
  // 0: first Tag, 1: distance to already used tags, 2: rest of gapsize
  i, j, vDistance, vDistanceMinID: Integer;
begin
  Result := 0;
  if (fCountFoundExp = 0) or (lstTagsAlreadyUsed.Count = 0) then
    Exit;
  SetLength(vListGaps, Length(aListGaps));
  vDistanceMinID := 0;
  for i := 0 to High(vListGaps) do
  begin
    vListGaps[i, 0] := aListGaps[i];
    vListGaps[i, 1] := 5000000;
    vListGaps[i, 2] := fReplaceTags.GapSize(vListGaps[i, 0]) - fCountFoundExp;
    for j := 0 to High(aListTagsUsed) do
    begin
      vDistance := Abs(aListTagsUsed[j]-vListGaps[i, 0]);
      if vDistance < vListGaps[i, 1] then
        vListGaps[i, 1] := vDistance;
    end;
    if vListGaps[vDistanceMinID, 1] > vListGaps[i,1] then
      vDistanceMinID := i;
  end;
  if vListGaps[vDistanceMinID, 1] < 30 then
    Result := vDistanceMinID
  else
  begin
    for i := vDistanceMinID to High(vListGaps) do
    begin
      if vListGaps[i, 2] < vListGaps[Result, 2] then
        Result := i;
    end;
  end;
end;

procedure TForm1.bSetTagsClick(Sender: TObject);
begin
  if not readyForProcessing then
    Exit;
  applySettings;
  Screen.Cursor := crHourGlass;
  fReplaceTags.processFiles(StrToIntDef(eBeginTag.Text, 0));
  bSelectFilesClick(nil);
  loadLangFile;
  Screen.Cursor := crDefault;
end;

procedure TForm1.chkOverWriteOriginsClick(Sender: TObject);
begin
  readyForProcessing;
end;

procedure TForm1.eBeginTagChange(Sender: TObject);
var
  vGap:Integer;
begin
  getSettings;
  if (eBeginTag.Text = '') then
    Exit;
  vGap := fReplaceTags.GapSize(StrToInt(eBeginTag.Text));
  lTagsFreeBehind.Caption := 'Freie Tags:' + Format('%7d',[vGap]);
  readyForProcessing;
end;

procedure TForm1.eBeginTagKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    bSetTagsClick(nil);
    lstFilesSRC.SetFocus;
  end;
end;

function TForm1.readyForProcessing: Boolean;
begin
  if (fCountFoundExp>0) and
    (StrToIntDef(eBeginTag.Text, 0) > 0) and
    (fReplaceTags.GapSize(StrToInt(eBeginTag.Text)) >= fCountFoundExp) and
    (DirectoryExists(eDirTarget.Text) or chkOverWriteOrigins.Checked) then
  begin
    Result := True;
    bSetTags.Enabled := True;
  end
  else
  begin
    bSetTags.Enabled := False;
    Result := False;
  end;
end;

procedure TForm1.SettingsClick(Sender: TObject);
var
  vSettings: TFormSettings;
begin
  getSettings;
  vSettings := TFormSettings.Create(Self, fReplaceTags);
  vSettings.ShowModal;
  vSettings.Free;
  applySettings;
end;

procedure TForm1.bSelDirTargetClick(Sender: TObject);
begin
  if eDirTarget.Text = EmptyStr then
    selDirTarget.InitialDir := ExtractFilePath(Application.ExeName)
  else
    if not DirectoryExists(eDirTarget.Text) then
      selDirTarget.InitialDir := ExtractFilePath(Application.ExeName)
    else
      selDirTarget.InitialDir := eDirTarget.Text;
  if not selDirTarget.Execute then
    Exit;
  eDirTarget.Text := selDirTarget.Directory;

  readyForProcessing;
end;
procedure TForm1.eDirTargetChange(Sender: TObject);
begin
  readyForProcessing;
end;

procedure TForm1.eDirTargetKeyPress(Sender: TObject; var Key: Char);
var
  vRV: Integer;
begin
  if not (Key = #13) then
    Exit;
  if not DirectoryExists(eDirTarget.Text) then
  begin
    vRV := MessageDlg('Zielordner existiert nicht, soll er erstellt werden?', mtConfirmation, mbYesNo, 0, mbYes);
    if vRV = mrYes then
      CreateDir(eDirTarget.Text);
  end;
  readyForProcessing;
end;

procedure TForm1.loadLangFile;
begin
  if not fReplaceTags.loadLanguageFile() then
  begin
    ShowMessage('Sprachdatei konnte nicht geladen werden');
    Exit;
  end;
  lCountTagsLangF.Caption := 'Anzahl Tags Sprachdatei: ' + IntToStr(fReplaceTags.CountTagList);
  showGapsInTaglist;
  readyForProcessing;
end;
procedure TForm1.bSelLanguageFileClick(Sender: TObject);
begin
  if DirectoryExists(eDirLangFiles.Text) then
    selDirLangFiles.InitialDir := eDirLangFiles.Text
  else
    selDirLangFiles.InitialDir := ExtractFileDir(Application.ExeName);
  if not selDirLangFiles.Execute then
    Exit;
  eDirLangFiles.Text := selDirLangFiles.Directory;
  fReplaceTags.Settings.LanguagePath := eDirLangFiles.Text;
  loadLangFile;
end;

procedure TForm1.eDirLangFilesKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    loadLangFile;
end;

procedure TForm1.ffneDatei1Click(Sender: TObject);
var
  i: Integer;
  vFileName: string;
begin
  for i := 0 to lstFilesSRC.Count-1 do
  if lstFilesSRC.Selected[i] then
  begin
    vFileName := fReplaceTags.GetFileNameOfIDSources(i);
    ShellExecute(Application.Handle, 'open', PChar(vFileName), nil, nil, SW_NORMAL);
    Break;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  lstFilesSRC.CustomHint := TCustomHint.Create(lstFilesSRC);
  fIDMouseHint := -1;
  fReplaceTags := TReplaceTags.Create;
  eDirectorySRCs.Text := 'C:\OurPlant\VicoBase.V15\trunk';
  eDirTarget.Text := ExtractFileDir(Application.ExeName) + '\FilesReplacedTags';
end;

procedure TForm1.AfterConstruction;
begin
  inherited;

  // local version information
  {$I gitversion.inc}

  fDeveloperVersionData.LASTUPDATE := LASTUPDATE;
  fDeveloperVersionData.GIT_LAST_TAG := GIT_LAST_TAG;
  fDeveloperVersionData.GIT_LAST_HASH := GIT_LAST_HASH;
  fDeveloperVersionData.GIT_CURRENT_BRANCH := GIT_CURRENT_BRANCH;
  fDeveloperVersionData.GIT_AUTHOR_NAME := GIT_AUTHOR_NAME;
  fDeveloperVersionData.GIT_COMMIT_DATE := GIT_COMMIT_DATE;

  try
    fInfoRecord := VersionInformation.ExtractInformations;
  except
  end;

  if VersionInformation.IsRelease then
    Self.Caption := Self.Caption + ' - V' + fInfoRecord.FileVersionData.FileVersionNr
  else
    Self.Caption := Self.Caption + ' - ' + 'Entwicklerversion' + ' | ' + fDeveloperVersionData.GIT_CURRENT_BRANCH + ' | ' + fDeveloperVersionData.GIT_AUTHOR_NAME
end;

procedure TForm1.BeforeDestruction;
begin
  inherited;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  lstFilesSRC.CustomHint.Free;
  fReplaceTags.Free;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  applySettings;
  if DirectoryExists(freplacetags.settings.sources) then
    listFilesSRCs;
  if FileExists(fReplaceTags.Settings.LanguageFiles[0]) then
    loadLangFile;
  if fReplaceTags.CommandLinePreSettings.filterFilesOnShow then
    bFilterFilesToBeProcessedClick(bFilterFilesToBeProcessed);
end;

procedure TForm1.lstFilesSRCKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    bSelectFilesClick(nil);
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = Ord('S')) then
    if readyForProcessing then
    begin
      bSetTagsClick(nil);
      lstFilesSRC.SetFocus;
    end;
  if (ssCtrl in Shift) and (Key = Ord('F')) then
  begin
    bFilterFilesToBeProcessedClick(nil);
    lstFilesSRC.SetFocus;
  end;
end;

procedure TForm1.Info1Click(Sender: TObject);
var
  vFormInfo : TFormInfo;
{$ifdef LOGINFO }
  vExtractFileVersionData : TExtractFileVersionData;
  vIsRelease: Boolean;
  vBuildNumber: Integer;
{$endif }
begin
{$ifdef LOGINFO }
  if VersionInformation.IsRelease then
  begin
    vExtractFileVersionData := VersionInformation.ExtractInformations;
    vIsRelease := VersionInformation.IsRelease;
    vBuildNumber := VersionInformation.GetBuildNumber;

    Logger.info('IsRelease', BoolToStr(vIsRelease, true));
    Logger.info('BuildNumber', IntToStr(vBuildNumber));

    Logger.info('Dev:LastUpdate', fDeveloperVersionData.LASTUPDATE);
    Logger.info('Dev:GitLastTag', fDeveloperVersionData.GIT_LAST_TAG);
    Logger.info('Dev:GitLastHash', fDeveloperVersionData.GIT_LAST_HASH);
    Logger.info('Dev:GitCurrentBranch', fDeveloperVersionData.GIT_CURRENT_BRANCH);
    Logger.info('Dev:GitAuthorName', fDeveloperVersionData.GIT_AUTHOR_NAME);
    Logger.info('Dev:GitCommitDate', fDeveloperVersionData.GIT_COMMIT_DATE);

    Logger.info('Rel.:CreationTime', vExtractFileVersionData.FileVersionData.CreationTime);
    Logger.info('Rel.:ModifiedTime', vExtractFileVersionData.FileVersionData.ModifiedTime);
    Logger.info('Rel.:FullFileVersionNr', vExtractFileVersionData.FileVersionData.FullFileVersionNr);
    Logger.info('Rel.:FileVersionNr', vExtractFileVersionData.FileVersionData.FileVersionNr);
    Logger.info('Rel.:BuildNumber', vExtractFileVersionData.FileVersionData.BuildNumber);
    Logger.info('Rel.:CompanyName', vExtractFileVersionData.FileVersionData.CompanyName);
    Logger.info('Rel.:OriginalFileName', vExtractFileVersionData.FileVersionData.OriginalFileName);
    Logger.info('Rel.:ProductName', vExtractFileVersionData.FileVersionData.ProductName);
    Logger.info('Rel.:ProductVersion', vExtractFileVersionData.FileVersionData.ProductVersion);

    Logger.info('Rel.:AccessedTime', vExtractFileVersionData.FileVersionData.AccessedTime);
    Logger.info('Rel.:FileDescription', vExtractFileVersionData.FileVersionData.FileDescription);
    Logger.info('Rel.:FileVersion', vExtractFileVersionData.FileVersionData.FileVersion);
    Logger.info('Rel.:InternalName', vExtractFileVersionData.FileVersionData.InternalName);
    Logger.info('Rel.:LegalCopyright', vExtractFileVersionData.FileVersionData.LegalCopyright);
    Logger.info('Rel.:LegalTrademarks', vExtractFileVersionData.FileVersionData.LegalTrademarks);
    Logger.info('Rel.:Comments', vExtractFileVersionData.FileVersionData.Comments);
    Logger.info('Rel.:PrivateBuild', vExtractFileVersionData.FileVersionData.PrivateBuild);
    Logger.info('Rel.:SpecialBuild', vExtractFileVersionData.FileVersionData.SpecialBuild);
  end;
{$endif }

  vFormInfo := TFormInfo.Create(Self, fInfoRecord, fDeveloperVersionData);
  vFormInfo.ShowModal;
  vFormInfo.Free;
end;

procedure TForm1.lstFilesSRCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    popupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  end;
end;


procedure TForm1.lstGapsClick(Sender: TObject);
var
  i : Integer;
begin
  for i := 0 to lstGaps.Count-1 do
    if lstGaps.Selected[i] then
      eBeginTag.Text := lstGaps.Items[i];
  if fReplaceTags.CountTagList>0 then
    eBeginTag.Enabled := True
  else
    eBeginTag.Enabled := False;
end;


procedure TForm1.lstGapsKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = 's' then
  begin
    bSetTagsClick(nil);
    lstFilesSRC.SetFocus;
  end;
end;

procedure TForm1.popSelAllClick(Sender: TObject);
begin
  lstFilesSRC.SelectAll;
end;

procedure TForm1.showGapsInTaglist;
var
  i : Integer;
  vGapList: TArray<Integer>;
begin
  lstGaps.Clear;
  vGapList := fReplaceTags.showGapsOfXTags;
  if (Length(vGapList) = 1) and (vGapList[0] = 0) then
    Exit;
  for i := 0 to High(vGapList) do
    lstGaps.Items.Add(IntToStr(vGapList[i]));
end;

procedure TForm1.Sprachdateieditor1Click(Sender: TObject);
var
  vLangFileEditor: TLanguageFileEditor;
begin
  if fReplaceTags.Settings.LanguageFiles[0]='' then
    ShowMessage('Das Sprachverzeichnis ist ungültig');
  vLangFileEditor := TLanguageFileEditor.Create(Self, fReplaceTags);
  vLangFileEditor.ShowModal;
  vLangFileEditor.Free;
  loadLangFile;
end;

procedure TForm1.SetFocusOnMouseEnter(Sender: TObject);
begin
  (Sender as TWinControl).SetFocus;
end;

procedure TForm1.SprachdateienSynchronisieren1Click(Sender: TObject);
var
  vMsg: string;
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  getSettings;
  try
    fReplaceTags.ProcessLanguageFiles;
  finally
    Screen.Cursor := crDefault;
  end;
  vMsg := EmptyStr;
  for i := 0 to High(fReplaceTags.Settings.LanguageFiles) do
    vMsg := vMsg + sLineBreak + fReplaceTags.Settings.LanguageFiles[i];
  vMsg := vMsg + sLineBreak + sLineBreak + 'Zielverzeichnis ist:' + sLineBreak;
  if fReplaceTags.Settings.OverWriteOrigins then
    vMsg := vMsg + fReplaceTags.Settings.LanguagePath
  else
    vMsg := vMsg + fReplaceTags.Settings.DirectoryTarget;
  ShowMessage('bearbeitete Sprachdateien:' + vMsg);
end;

procedure TForm1.UngenutzteTags1Click(Sender: TObject);
var
  vWindow: TFormUnusedTags;
  vListFiles: TStringList;
  i: Integer;
begin
  getSettings;
  applySettings;
  if lstFilesSRC.Count = 0 then
    Exit;
  vListFiles := TStringList.Create;
  for i := 0 to lstFilesSRC.Count-1 do
    vListFiles.Add(eDirectorySRCs.Text + lstFilesSRC.Items[i]);
  fReplaceTags.PathSources := eDirectorySRCs.Text;
  vWindow := TFormUnusedTags.Create(Self, fReplaceTags, vListFiles.ToStringArray);
  vWindow.ShowModal;
  vWindow.Free;
  fReplaceTags.loadLanguageFile();
  listFilesSRCs;
end;

procedure TForm1.lstFilesSRCMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  vUsedTags: TArray<Integer>;
  vFN: string;
  vIDold: Integer;
begin
  if not chkShowHints.Checked then
    Exit;
  if not fReplaceTags.IsLanguageFileLoaded then
    fReplaceTags.loadExpressionsFile();
  vIDold := fIDMouseHint;
  fIDMouseHint:=lstFilesSRC.ItemAtPos(Point(x,y),True);
  if vIDold = fIDMouseHint then
    Exit;
  if fIDMouseHint = -1 then
  begin
    lstFilesSRC.CustomHint.Description := EmptyStr;
    Exit;
  end;
  vFN := eDirectorySRCs.Text + lstFilesSRC.Items[fIDMouseHint];
  vUsedTags := fReplaceTags.LookForUsedTagsInFile(vFN);
  with lstFilesSRC do
  begin
    CustomHint.Description := fReplaceTags.ShowTagsInGroups(vUsedTags);
    CustomHint.ShowHint(Mouse.CursorPos);
    CustomHint.HideAfter := 20000;
  end;
end;
procedure TForm1.lstFilesSRCMouseLeave(Sender: TObject);
begin
  lstFilesSRC.CustomHint.HideHint;
end;

end.

