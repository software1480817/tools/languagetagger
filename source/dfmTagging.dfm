object DFMTagger: TDFMTagger
  Left = 0
  Top = 0
  Caption = 'DFM-Tagger'
  ClientHeight = 298
  ClientWidth = 1064
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  OnShow = FormShow
  DesignSize = (
    1064
    298)
  PixelsPerInch = 96
  TextHeight = 13
  object lCountFilesSRCs: TLabel
    Left = 328
    Top = 248
    Width = 72
    Height = 13
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    Caption = 'Anzahl Dateien'
    ExplicitTop = 366
  end
  object lCountListObjects: TLabel
    Left = 803
    Top = 269
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Anchors = [akRight, akBottom]
    Caption = 'lCountListObjects'
    ExplicitLeft = 864
    ExplicitTop = 387
  end
  object lFilterSources: TLabel
    Left = 8
    Top = 247
    Width = 65
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Filter Sources'
    ExplicitTop = 365
  end
  object lMinLengthCaption: TLabel
    Left = 588
    Top = 253
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Anchors = [akRight, akBottom]
    Caption = 'mindestl'#228'nge Caption'
  end
  object lCountReplacements: TLabel
    Left = 923
    Top = 245
    Width = 98
    Height = 13
    Alignment = taCenter
    Anchors = [akRight, akBottom]
    Caption = 'lCountReplacements'
    ExplicitLeft = 984
    ExplicitTop = 363
  end
  object bLoadFilesSRCs: TButton
    Left = 312
    Top = 264
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'lade Dateien'
    TabOrder = 1
    OnClick = bLoadFilesSRCsClick
    ExplicitTop = 382
  end
  object bAllYesOrNo: TButton
    Left = 423
    Top = 248
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'alle Ja <--> Nein'
    TabOrder = 2
    OnClick = bAllYesOrNoClick
    ExplicitTop = 366
  end
  object bSetToMinusOne: TButton
    Left = 899
    Top = 264
    Width = 148
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'setze auf -1'
    TabOrder = 5
    OnClick = bSetToMinusOneClick
    ExplicitLeft = 960
    ExplicitTop = 382
  end
  object eFilterSourceFiles: TEdit
    Left = 8
    Top = 264
    Width = 169
    Height = 21
    Hint = 'regular expressions;'#13#10'use also RETURN and ESC'
    Anchors = [akLeft, akBottom]
    TabOrder = 0
    OnKeyDown = eFilterSourceFilesKeyDown
    ExplicitTop = 382
  end
  object bFilterSources: TBitBtn
    Left = 183
    Top = 262
    Width = 26
    Height = 25
    Hint = 'applicates filter to the current file list'
    Anchors = [akLeft, akBottom]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 6
    OnClick = bFilterSourcesClick
    ExplicitTop = 380
  end
  object bResetFilterSources: TBitBtn
    Left = 215
    Top = 262
    Width = 26
    Height = 25
    Hint = 'reset the filter'
    Anchors = [akLeft, akBottom]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 7
    OnClick = bResetFilterSourcesClick
    ExplicitTop = 380
  end
  object seMinLengthObject: TSpinEdit
    Left = 699
    Top = 250
    Width = 65
    Height = 22
    Anchors = [akRight, akBottom]
    MaxValue = 1000
    MinValue = 0
    TabOrder = 3
    Value = 0
  end
  object bFilterObjectsByCaptionLength: TBitBtn
    Left = 770
    Top = 248
    Width = 26
    Height = 25
    Hint = 'applicates filter to the current file list'
    Anchors = [akRight, akBottom]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 4
    OnClick = bFilterObjectsByCaptionLengthClick
  end
  object bFilterOKFiles: TButton
    Left = 247
    Top = 248
    Width = 59
    Height = 42
    Anchors = [akLeft, akBottom]
    Caption = 'Filter OK Files'
    TabOrder = 8
    WordWrap = True
    OnClick = bFilterOKFilesClick
    ExplicitTop = 366
  end
  object pListsBack: TPanel
    Left = 0
    Top = 0
    Width = 1064
    Height = 239
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 9
    ExplicitWidth = 1125
    ExplicitHeight = 586
    object spl1: TSplitter
      Left = 385
      Top = 1
      Width = 8
      Height = 237
      ExplicitHeight = 127
    end
    object pLeftList: TPanel
      Left = 1
      Top = 1
      Width = 384
      Height = 237
      Align = alLeft
      TabOrder = 0
      ExplicitHeight = 127
      object l1: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 1
        Width = 376
        Height = 13
        Margins.Top = 0
        Align = alTop
        Caption = 'Quellcode-Verzeichnis'
        ExplicitLeft = -1
        ExplicitTop = -11
        ExplicitWidth = 382
      end
      object lstFilesSRCs: TListBox
        Left = 1
        Top = 44
        Width = 382
        Height = 192
        Align = alClient
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 0
        OnDblClick = bLoadFilesSRCsClick
        OnKeyDown = lstFilesSRCsKeyDown
        ExplicitLeft = -25
        ExplicitTop = -182
        ExplicitWidth = 409
        ExplicitHeight = 309
      end
      object eDirectorySRCs: TEdit
        AlignWithMargins = True
        Left = 4
        Top = 20
        Width = 376
        Height = 21
        Align = alTop
        Enabled = False
        TabOrder = 1
        Text = 'eDirectorySRCs'
        ExplicitLeft = 136
        ExplicitTop = 56
        ExplicitWidth = 121
      end
    end
    object sgResult: TStringGrid
      Left = 393
      Top = 1
      Width = 670
      Height = 237
      Align = alClient
      ColCount = 7
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSizing, goColSizing]
      TabOrder = 1
      OnDblClick = sgResultDblClick
      OnKeyPress = sgResultKeyPress
      OnMouseDown = sgResultMouseDown
      ExplicitLeft = 308
      ExplicitTop = -207
      ExplicitWidth = 685
      ExplicitHeight = 336
      ColWidths = (
        28
        107
        94
        143
        40
        195
        51)
      RowHeights = (
        24
        24)
    end
  end
end
