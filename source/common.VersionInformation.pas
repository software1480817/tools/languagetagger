/// <summary>
///   Getting Information
/// </summary>
unit common.VersionInformation;

interface

{$REGION 'uses'}

uses
  WinApi.Windows,
  Spring.Collections,
  System.SysUtils,
  System.Classes,
  Vcl.Forms,
  Vcl.STDCtrls,
  Vcl.Graphics;
{$ENDREGION}

const
  RELEEASE_TAG = 'Release';

{$IFDEF _WIN32 or WIN32}
  INVALID_HANDLE_VALUE_Migration = INVALID_HANDLE_VALUE;
{$ELSE}
  INVALID_HANDLE_VALUE_Migration = 0;
{$ENDIF}

type

  /// <summary>
  ///   Contains Release informations. Extracts informations from exe file.
  /// </summary>
  TFileVersionData = record
    CreationTime, ModifiedTime, AccessedTime, CompanyName, FileDescription, FileVersion, InternalName, LegalCopyright,
      LegalTrademarks, OriginalFileName, ProductName, ProductVersion, Comments, PrivateBuild, SpecialBuild,
      FullFileVersionNr, FileVersionNr, BuildNumber: string;
  end;

  /// <summary>
  ///   Contains Developer informations if is not Release. Extracts
  ///   informations from gitversion.inc.
  /// </summary>
  TDeveloperVersionData = record
    /// <summary>
    ///   Last update date time gitversion.inc
    /// </summary>
    LastUpdate: string;
    /// <summary>
    ///   last git tag
    /// </summary>
    GitLastTag: string;
    /// <summary>
    ///   current git short hash
    /// </summary>
    GitLastHash: string;
    /// <summary>
    ///   current git branch / feature
    /// </summary>
    GitCurrentBranch: string;
    /// <summary>
    ///   current git author name
    /// </summary>
    GitAuthorName: string;
    /// <summary>
    ///   current git last commit date time
    /// </summary>
    GitCommitDate: string;
  end;

  /// <summary>
  ///   Contains extracted informations from Release and Developer branch
  /// </summary>
  TExtractFileVersionData = record
    IsRelease: Boolean;
    Is64BitOP: Boolean;
    FileVersionData: TFileVersionData;
    DeveloperVersionData: TDeveloperVersionData;
  end;

  /// <summary>
  ///   Retrieves information about a given exe file. Set filename in create
  /// </summary>
  IVersionInformation = interface
    ['{65BCB9BC-D1AB-4B7A-8B03-5E62E6FA19BE}']
    /// <summary>
    ///   Sets the name of the application from which the information has to be
    ///   extract. Leave empty to use your own application.
    /// </summary>
    /// <param name="APathFileName">
    ///   Don't use this function or leave empty to use own application
    ///   otherwise set full path with exe name
    /// </param>
    procedure ChangeApplicationName(const APathFileName: string);
    /// <summary>
    ///   Extract informations from filename details page
    /// </summary>
    /// <exception cref="EUnknownExeFile">
    ///   File not found
    /// </exception>
    /// <exception cref="Exception">
    ///   Errors while extraction
    /// </exception>
    function ExtractInformations: TExtractFileVersionData;
    /// <summary>
    ///   returning is build with server (Release) or developer version
    /// </summary>
    function IsRelease: Boolean;
    /// <summary>
    ///   returning Build Number from Version Number Major.Minor.Hotfix.BuildNumber
    /// </summary>
    function GetBuildNumber: Integer;
  end;

  EUnknownExeFile = class(Exception);

  TVersionInformation = class(TInterfacedObject, IVersionInformation)
  strict private
  var
    fApplicationName:         string;
    fInformationHasToExtract: Boolean;
    fIsReleaseBuild:          Boolean;
    fBuildNumber:             Integer;
    fFileVersionData:         TFileVersionData;

  private
    // IVersionInformation
      // release part
    procedure ChangeApplicationName(const APathFileName: string);
    function ExtractInformations: TExtractFileVersionData;
    function IsRelease: Boolean;
    function GetBuildNumber: Integer;
    function Is32BitApp: Boolean;

    // Helper
    function GetFileVersionInformation(const AFileName: string): TFileVersionData;
    function GetFileTime(const AFileTime: TFileTime): string;

  public
    constructor Create();
  end;

implementation

{$I gitversion.inc}
{
  missing file \projects\DX10Rio\$I gitversion.inc ??
    => do the following steps
    1) be sure git.exe is in your system path
    2) add PRE Build Action to Project - Options: Build Events: "All configurations Win 32 Platform" : Pre Build!!
        .\..\..\scripts\GitVersion.bat
       this actions adds \projects\DX10Rio\gitversion.inc with following consts
        const LASTUPDATE
        const GIT_LAST_TAG
        const GIT_LAST_HASH
        const GIT_CURRENT_BRANCH
        const GIT_AUTHOR_NAME
        const GIT_COMMIT_DATE
}

{ TVersionInformation }

constructor TVersionInformation.Create();
begin
  inherited;

  fApplicationName := Application.ExeName;
  fInformationHasToExtract := True;
  fIsReleaseBuild := False;
  fBuildNumber := 0;
end;

procedure TVersionInformation.ChangeApplicationName(const APathFileName: string);
begin
  if (fInformationHasToExtract = False) and (APathFileName <> fApplicationName) then
    fInformationHasToExtract := True;

  if (APathFileName = '') then
    fApplicationName := Application.ExeName
  else
    fApplicationName := APathFileName;
end;

function TVersionInformation.ExtractInformations: TExtractFileVersionData;
begin
  if (fInformationHasToExtract = True) then
  begin
    fIsReleaseBuild := False;
    fBuildNumber := 0;
    try
      if not FileExists(fApplicationName) then
        raise EUnknownExeFile.Create('Datei existiert nicht: ' + fApplicationName);

      try
        fFileVersionData := GetFileVersionInformation(fApplicationName);

        Result.FileVersionData := fFileVersionData;

        fBuildNumber := StrToInt(fFileVersionData.BuildNumber);
        if (fFileVersionData.InternalName <> RELEEASE_TAG) or (fBuildNumber = 0) then
          fIsReleaseBuild := False
        else
          fIsReleaseBuild := True;

        Result.IsRelease := fIsReleaseBuild;
        Result.Is64BitOP := not Is32BitApp;

        Result.DeveloperVersionData.LastUpdate := LASTUPDATE;
        Result.DeveloperVersionData.GitLastTag := GIT_LAST_TAG;
        Result.DeveloperVersionData.GitLastHash := GIT_LAST_HASH;
        Result.DeveloperVersionData.GitCurrentBranch := GIT_CURRENT_BRANCH;
        Result.DeveloperVersionData.GitAuthorName := GIT_AUTHOR_NAME;
        Result.DeveloperVersionData.GitCommitDate := GIT_COMMIT_DATE;

      finally
        fInformationHasToExtract := False;
      end;

    except
      on E: EUnknownExeFile do
        raise EUnknownExeFile.Create(E.Message);
      on E: Exception do
        raise Exception.Create(E.Message);
    end;
  end
end;

function TVersionInformation.IsRelease: Boolean;
begin
  Result := fIsReleaseBuild;
end;

function TVersionInformation.GetBuildNumber: Integer;
begin
  Result := fBuildNumber;
end;

function TVersionInformation.Is32BitApp: Boolean;
begin
{$IFDEF WIN64}
  Result := False;
{$ELSE}
  Result := True;
{$ENDIF}
end;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  Helper
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function TVersionInformation.GetFileVersionInformation(const AFileName: string): TFileVersionData;
type
  PLandCodepage = ^TLandCodepage;

  TLandCodepage = record
    wLanguage, wCodePage: Word;
  end;
var
  vDummy, vLen:   cardinal;
  vBuffer, vPntr: pointer;
  vLang:          string;
  vPVerValue:     PVSFixedFileInfo;
  vFad:           TWin32FileAttributeData;
begin
  vLen := GetFileVersionInfoSize(PChar(AFileName), vDummy);
  if vLen = 0 then
    RaiseLastOSError;
  GetMem(vBuffer, vLen);
  try
    if not GetFileVersionInfo(PChar(AFileName), 0, vLen, vBuffer) then
    begin
      raise Exception.Create('versions informations not set');
    end;
    if not VerQueryValue(vBuffer, '\VarFileInfo\Translation\', vPntr, vLen) then
      RaiseLastOSError;
    vLang := Format('%.4x%.4x', [PLandCodepage(vPntr)^.wLanguage, PLandCodepage(vPntr)^.wCodePage]);
    if not GetFileAttributesEx(PChar(AFileName), GetFileExInfoStandard, @vFad) then
      RaiseLastOSError;
    Result.CreationTime := GetFileTime(vFad.ftCreationTime);
    Result.ModifiedTime := GetFileTime(vFad.ftLastWriteTime);
    Result.AccessedTime := GetFileTime(vFad.ftLastAccessTime);
    if VerQueryValue(vBuffer, '\', pointer(vPVerValue), vLen) then
      with vPVerValue^ do
      begin
        Result.FullFileVersionNr := Format('%d.%d.%d %d', [HiWord(dwFileVersionMS), //Major
          LoWord(dwFileVersionMS), //Minor
          HiWord(dwFileVersionLS), //Release
          LoWord(dwFileVersionLS)]); //Build
        Result.FileVersionNr := Format('%d.%d.%d', [HiWord(dwFileVersionMS), //Major
          LoWord(dwFileVersionMS), //Minor
          HiWord(dwFileVersionLS)]); //Release
        Result.BuildNumber := Format('%d', [LoWord(dwFileVersionLS)]);
        //Build
      end;
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\CompanyName'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.CompanyName := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\FileDescription'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.FileDescription := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\FileVersion'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.FileVersion := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\InternalName'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.InternalName := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\LegalCopyright'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.LegalCopyright := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\LegalTrademarks'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.LegalTrademarks := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\OriginalFileName'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.OriginalFileName := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\ProductName'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.ProductName := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\ProductVersion'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.ProductVersion := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\Comments'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.Comments := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\PrivateBuild'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.PrivateBuild := PChar(vPntr);
    if VerQueryValue(vBuffer, PChar('\StringFileInfo\' + vLang + '\SpecialBuild'), vPntr, vLen)
    { and (@len <> nil) } then
      Result.SpecialBuild := PChar(vPntr);
  finally
    FreeMem(vBuffer);
  end;
end;

function TVersionInformation.GetFileTime(const AFileTime: TFileTime): string;
var
  SystemTime, LocalTime: TSystemTime;
begin
  if not FileTimeToSystemTime(AFileTime, SystemTime) then
    RaiseLastOSError;
  if not SystemTimeToTzSpecificLocalTime(nil, SystemTime, LocalTime) then
    RaiseLastOSError;
  Result := DateTimeToStr(SystemTimeToDateTime(LocalTime));
end;

end.
