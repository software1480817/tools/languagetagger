unit LangFileEditor;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  System.Generics.Collections,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.Grids,
  System.Actions,
  Vcl.ActnList,

  replaceTags;

type
  TLanguageFileEditor = class(TForm)
    eDirSources: TEdit;
    lstFilesSources: TListBox;
    sgEntries: TStringGrid;
    lDirLangFiles: TLabel;
    cbLangFile: TComboBox;
    bReDo: TButton;
    bSave: TButton;
    chkAutoSync: TCheckBox;
    bFilterFilesWithTags: TButton;
    chkOnlyUnreleasedTags: TCheckBox;
    eTagToLookFor: TEdit;
    bSearchSpecialTag: TButton;
    bResetFilter: TButton;
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lstFilesSourcesClick(Sender: TObject);
    procedure cbLangFileChange(Sender: TObject);
    procedure bReDoClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bFilterFilesWithTagsClick(Sender: TObject);
    procedure setFocusOnMouseEnter(const aSender: TObject);
    procedure bSearchSpecialTagClick(Sender: TObject);
    procedure bResetFilterClick(Sender: TObject);
    procedure eTagToLookForEnter(Sender: TObject);
    procedure eTagToLookForKeyPress(Sender: TObject; var Key: Char);
    procedure sgEntriesDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgEntriesMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

  private
    { Private-Deklarationen }
    fReplaceTags: TReplaceTags;
    fTagsInFiles: TList<Integer>;
    fTagsLangFiles: array [0..1] of TList<Integer>;
    fTagsLangNew: array[0..1] of TList<Integer>;
    fDicts: array [0..1] of TDictionary<Integer, string>;
    fLangFiles: array [0..1] of string;
    fLangID: Integer;


    procedure listFilesSources;
    procedure writeStringGrid;
    procedure getChangesFromGrid;
    procedure loadLangFiles;
  public
    { Public-Deklarationen }
    constructor Create(const aOwner: TComponent; aRPT: TReplaceTags); reintroduce;
    destructor Destroy; override;
  end;

var
  LanguageFileEditor: TLanguageFileEditor;

implementation

uses

  common.SafeMemIni,
  System.RegularExpressions;

{$R *.dfm}
{ TLanguageFileEditor }

procedure TLanguageFileEditor.bFilterFilesWithTagsClick(Sender: TObject);
  function listOfUnreleasedTags(): TArray<Integer>;
  begin
    Result := fTagsLangNew[fLangID].ToArray;
  end;
var
  vFileID, j: Integer;
  vTags: TArray<Integer>;
  vFilesWithTags: TList<Integer>;
  vMarkedTags: TList<Integer>;
begin
  vMarkedTags := TList<Integer>.Create;
  if chkOnlyUnreleasedTags.Checked then
    vMarkedTags.AddRange(listOfUnreleasedTags);
  vFilesWithTags := TList<Integer>.Create;
  Screen.Cursor := crHourGlass;
  try
    for vFileID := 0 to lstFilesSources.Count-1 do
    begin
      fReplaceTags.SelectFilesFromSources([vFileID]);
      vTags := fReplaceTags.ListOfUsedTagsInFiles();
      if Length(vTags) > 0 then
        if chkOnlyUnreleasedTags.Checked then
        begin
          for j := 0 to High(vTags) do
            if vMarkedTags.Contains(vTags[j]) then
            begin
              vFilesWithTags.Add(vFileID);
              Break;
            end;
        end
        else
          vFilesWithTags.Add(vFileID);
    end;
    fReplaceTags.SelectFilesFromSources(vFilesWithTags.ToArray);
    fReplaceTags.RemoveFilesFromSourcesNotSelected();
  finally
    vFilesWithTags.Free;
    vMarkedTags.Free;
    Screen.Cursor := crDefault;
  end;
  fReplaceTags.SelectFilesFromSourcesALL;
  listFilesSources;
end;

procedure TLanguageFileEditor.bReDoClick(Sender: TObject);
begin
  cbLangFileChange(nil);
end;

procedure TLanguageFileEditor.bResetFilterClick(Sender: TObject);
begin
  fReplaceTags.LoadFilesSourcesFromPath();
  fReplaceTags.SelectFilesFromSourcesALL;
  listFilesSources;
end;

procedure TLanguageFileEditor.bSaveClick(Sender: TObject);
var
  i, j: Integer;
  vINI: TSafeIni;
begin
  getChangesFromGrid;
  for i := 0 to High(fLangFiles) do
  begin
    if fLangFiles[i] = EmptyStr then Exit;
    vINI := TSafeIni.Create(fLangFiles[i]);
    vINI.EraseSection('Liste');
    vINI.UpdateFile;
    vINI.WriteString('Kopf', 'Dateiname', fLangFiles[i]);
    vINI.WriteString('Kopf', 'Stand', DateToStr(Now));
    vINI.WriteInteger('Kopf', 'Eintraege', fTagsLangFiles[i].Last);
    for j := 0 to fTagsLangFiles[i].Count-1 do
    begin
      vINI.WriteString('Liste', IntToStr(fTagsLangFiles[i].Items[j]),
          fDicts[i].Items[fTagsLangFiles[i].Items[j]]);
      if fTagsLangNew[i].Contains(fTagsLangFiles[i].Items[j]) then
        vINI.WriteInteger('zu pruefen', IntToStr(fTagsLangFiles[i].Items[j]), fTagsLangFiles[i].Items[j])
      else
        vINI.DeleteKey('zu pruefen', IntToStr(fTagsLangFiles[i].Items[j]));
    end;
    vINI.UpdateFile;
    vINI.Free;
  end;
end;

procedure TLanguageFileEditor.bSearchSpecialTagClick(Sender: TObject);
var
  vTagSearch: Integer;
  i: Integer;
  vFileIDs, vTags: TList<Integer>;
begin
  if not TryStrToInt(eTagToLookFor.Text, vTagSearch) then
  begin
    ShowMessage('keine Zahl eingegeben');
    Exit;
  end;
  vFileIDs := tlist<Integer>.Create;
  vTags := tlist<Integer>.Create;
  Screen.Cursor := crHourGlass;
  for i := 0 to lstFilesSources.Count-1 do
  begin
    fReplaceTags.SelectFilesFromSources([i]);
    vTags.Clear;
    vTags.AddRange(fReplaceTags.ListOfUsedTagsInFiles);
    if vTags.Contains(vTagSearch) then
      vFileIDs.Add(i);
  end;
  fReplaceTags.SelectFilesFromSources(vFileIDs.ToArray);
  fReplaceTags.RemoveFilesFromSourcesNotSelected;
  vFileIDs.Free;
  vTags.Free;
  Screen.Cursor := crDefault;
  listFilesSources;
end;

procedure TLanguageFileEditor.cbLangFileChange(Sender: TObject);
var
  i: Integer;
begin
  i := cbLangFile.ItemIndex;
  case i of
    0: begin
        fLangFiles[0] := fReplaceTags.Settings.LanguageFiles[0]; // deutsche
        fLangFiles[1] := EmptyStr;
        sgEntries.ColCount := 3;
        fLangID := 0;
       end;
    1: begin
        fLangFiles[0] := fReplaceTags.Settings.LanguageFiles[0];
        fLangFiles[1] := fReplaceTags.Settings.LanguageFiles[1];
        sgEntries.ColCount := 4;
        fLangID := 1;
       end;
    else begin
          fLangFiles[0] := fReplaceTags.Settings.LanguageFiles[1]; // englische
          fLangFiles[1] := fReplaceTags.Settings.LanguageFiles[i];
          sgEntries.ColCount := 4;
          fLangID := 1;
         end;
  end;
  FormResize(nil);
  loadLangFiles;
  writeStringGrid;
end;

constructor TLanguageFileEditor.Create(const aOwner: TComponent; aRPT: TReplaceTags);
begin
  inherited Create(aOwner);
  fReplaceTags := aRPT;
  fTagsInFiles := TList<Integer>.Create;
end;

destructor TLanguageFileEditor.Destroy;
var
  i: Integer;
begin
  fTagsInFiles.Free;
  for i := 0 to High(fTagsLangFiles) do
  begin
    if Assigned(fTagsLangFiles[i]) then
      fTagsLangFiles[i].Free;
    if Assigned(fDicts[i]) then
      fDicts[i].Free;
    if Assigned(fTagsLangNew[i]) then
      fTagsLangNew[i].Free;
  end;
  inherited;
end;

procedure TLanguageFileEditor.eTagToLookForEnter(Sender: TObject);
begin
  eTagToLookFor.SelectAll;
end;

procedure TLanguageFileEditor.eTagToLookForKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    bSearchSpecialTagClick(nil);
end;

procedure TLanguageFileEditor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  fReplaceTags.LoadFilesSourcesFromPath();
  if chkAutoSync.Checked then
  begin
    fReplaceTags.loadLanguageFile;
    fReplaceTags.ProcessLanguageFiles;
  end;
end;

procedure TLanguageFileEditor.FormResize(Sender: TObject);
const
  WIDTH_COLORED_CELL = 30;
  WIDTH_SCROLLBAR = 24;
begin
  with sgEntries do
  begin
    ColWidths[0] := 40;
    if ColCount = 3 then
      ColWidths[1] := Width - ColWidths[0] - WIDTH_COLORED_CELL - WIDTH_SCROLLBAR;
      ColWidths[2] := WIDTH_COLORED_CELL;
    if ColCount = 4 then
    begin
      ColWidths[1] := (Width - ColWidths[0] - WIDTH_SCROLLBAR - WIDTH_COLORED_CELL) div 2;
      ColWidths[2] := Width - ColWidths[0] - WIDTH_SCROLLBAR - WIDTH_COLORED_CELL - ColWidths[1];
      ColWidths[3] := WIDTH_COLORED_CELL;
    end;
  end;
end;

procedure TLanguageFileEditor.FormShow(Sender: TObject);
var
  i: Integer;
begin
  if Length(fReplaceTags.Settings.LanguageFiles)<2 then
  begin
    ShowMessage('es m�ssen mindestens die Deutsche und Englische Sprachdatei'
        + 'in dem ausgew�hlten Sprachdateiordner vorhanden sein');
    Self.Close;
  end;
  eDirSources.Text := fReplaceTags.Settings.Sources;
  eDirSources.Enabled := False;
  lDirLangFiles.Caption := fReplaceTags.Settings.LanguagePath;
  for i := 0 to High(fReplaceTags.Settings.LanguageFiles) do
    cbLangFile.Items.Add(ExtractFileName(fReplaceTags.Settings.LanguageFiles[i]));
  fReplaceTags.LoadFilesSourcesFromPath();
  fReplaceTags.SelectFilesFromSourcesALL;
  listFilesSources;
  if fReplaceTags.IsLanguageFileLoaded then
  begin
    cbLangFile.ItemIndex := 0;
    cbLangFileChange(nil);
  end;
  writeStringGrid;
end;

procedure TLanguageFileEditor.getChangesFromGrid;
var
  vSL: TStringList;
  vTags: TList<Integer>;
  i, j: Integer;
begin
  vTags := tlist<Integer>.Create;
  vSL := TStringList.Create;
  try
    vSL.AddStrings(sgEntries.Cols[0]);
    vSL.Delete(0);
    for i := 0 to vSL.Count-1 do
      vTags.Add(StrToInt(vSL[i]));
    for i := 0 to High(fLangFiles) do
    begin
      if fLangFiles[i] = EmptyStr then Exit;
      vSL.Clear;
      vSL.AddStrings(sgEntries.Cols[i+1]);
      vSL.Delete(0);
      for j := 0 to vTags.Count-1 do
      begin
        if not fTagsLangFiles[i].Contains(vTags.Items[j]) then
        begin
          fTagsLangFiles[i].Add(vTags.Items[j]);
          fDicts[i].Add(fTagsLangFiles[i].Last, vSL[j]);
          fTagsLangFiles[i].Sort;
        end
        else
          fDicts[i].Items[vTags.Items[j]] := vSL[j];
      end;
    end;
  finally
    vSL.Free;
    vTags.Free;
  end;
end;

procedure TLanguageFileEditor.setFocusOnMouseEnter(const aSender: TObject);
begin
  (aSender as TWinControl).SetFocus;
end;

procedure TLanguageFileEditor.sgEntriesDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  if ARow = 0 then Exit;
  if not (ACol = sgEntries.ColCount-1) then
    Exit;
  if cbLangFile.ItemIndex > 0 then
    fLangID := 1 else fLangID := 0;
  if fTagsLangNew[fLangID].Contains(StrToInt(sgEntries.Cells[0, ARow])) then
  begin
    TStringGrid(Sender).Canvas.Brush.Color := clRed;
    TStringGrid(Sender).Canvas.FillRect(Rect);
  end;
end;

procedure TLanguageFileEditor.sgEntriesMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  with sgEntries do
  begin
    if not (Selection.Left = (ColCount-1)) then
    begin
      Options := Options + [TGridOption.goEditing];
      Exit;
    end;
    Options := Options - [TGridOption.goEditing];
    if fTagsLangNew[fLangID].Contains(StrToInt(Cells[0, Selection.Top])) then
      fTagsLangNew[fLangID].Delete(fTagsLangNew[fLangID].IndexOf(StrToInt(Cells[0, Selection.Top])))
    else
      fTagsLangNew[fLangID].Add(StrToInt(Cells[0, Selection.Top]));
  end;

  writeStringGrid;
end;

procedure TLanguageFileEditor.listFilesSources;
var
  i:      Integer;
  vFiles: TArray<string>;
begin
  vFiles := fReplaceTags.GetFilesSources;
  lstFilesSources.Clear;
  for i := 0 to High(vFiles) do
    lstFilesSources.Items.Add(vFiles[i].Substring(Length(fReplaceTags.Settings.Sources)));
end;

procedure TLanguageFileEditor.loadLangFiles;
var
  vINI: TSafeIni;
  vSL: TStringList;
  vFileID, i: Integer;
begin
  vSL := TStringList.Create;
  for vFileID := 0 to High(fLangFiles) do
  begin
    if fLangFiles[vFileID] = EmptyStr then
      Break;
    if Assigned(fTagsLangFiles[vFileID]) then
    begin
      fTagsLangFiles[vFileID].Clear;
      fDicts[vFileID].Clear;
      fTagsLangNew[vFileID].Clear;
    end
    else
    begin
      fTagsLangFiles[vFileID] := TList<Integer>.Create;
      fTagsLangNew[vFileID] := TList<Integer>.Create;
      fDicts[vFileID] := TDictionary<Integer, string>.Create;
    end;
    vINI := TSafeIni.Create(flangfiles[vFileID]);
    vINI.ReadSection('Liste', vSL);
    for i := 0 to vSL.Count-1 do
    begin
      fTagsLangFiles[vFileID].Add(StrToInt(vSL[i]));
      fDicts[vFileID].Add(fTagsLangFiles[vFileID].Last,
          vINI.ReadString('Liste', vSL[i], ''));
    end;
    vINI.ReadSection('zu pruefen', vSL);
    for i := 0 to vSL.Count-1 do
      fTagsLangNew[vFileID].Add(StrToInt(vSL[i]));
    vINI.Free;
  end;
  vSL.Free;
end;

procedure TLanguageFileEditor.lstFilesSourcesClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lstFilesSources.Count-1 do
    if lstFilesSources.Selected[i] then
      Break;
  if i >= lstFilesSources.Count then
    Exit;
  fReplaceTags.SelectFilesFromSources([i]);
  fTagsInFiles.Clear;
  fTagsInFiles.AddRange(fReplaceTags.ListOfUsedTagsInFiles());
  getChangesFromGrid;
  writeStringGrid;
end;

procedure TLanguageFileEditor.writeStringGrid;
  procedure writeOneColumn(const aI: Integer);
  var
    vEntry: string;
    vSL: TStringList;
    i: Integer;
  begin
    vSL := TStringList.Create;
    if fLangFiles[aI] = EmptyStr then
      vSL.Add('Sprachdatei')
    else
      vSL.Add(ExtractFileName(fLangFiles[aI]));
    if Assigned(fDicts[aI]) then
    begin
      for i := 0 to fTagsInFiles.Count-1 do
      begin
        if fDicts[aI].TryGetValue(fTagsInFiles.Items[i], vEntry) then
          vSL.Add(vEntry)
        else vSL.Add(EmptyStr);
      end;
    end;
    sgEntries.Cols[aI + 1] := vSL;
    vSL.Free;
  end;
var
  vSL: TStringList;
  i: Integer;
begin
  vSL := TStringList.Create;
  try
    sgEntries.Cells[sgEntries.ColCount-1,0] := 'Neu';
    sgEntries.RowCount := fTagsInFiles.Count+1;
    if sgEntries.RowCount > 1 then
      sgEntries.FixedRows := 1;
    vSL.Clear;
    vSL.Add('Tag');
    for i := 0 to fTagsInFiles.Count-1 do
    begin
      sgEntries.Cells[0,i+1] := IntToStr(fTagsInFiles.Items[i]);
    end;
    sgEntries.Cols[0] := vSL;
    case sgEntries.ColCount of
      3:
      begin
        writeOneColumn(0);
      end;
      4:
      begin
        writeOneColumn(0);
        writeOneColumn(1);
      end;
      else Exit;
    end;
    for i := 1 to sgEntries.RowCount-1 do
      sgEntries.Cells[sgEntries.ColCount-1, i] := '';
  finally
    vSL.Free;
  end;
end;

end.
