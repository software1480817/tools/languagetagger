unit Info;

interface

{$REGION 'uses'}
uses
  packages.tools.VersionInformation,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  Vcl.ExtCtrls, JvExComCtrls, JvComCtrls;
{$ENDREGION}

type
 TDeveloperVersionData = record
    LASTUPDATE: string;
    GIT_LAST_TAG: string;
    GIT_LAST_HASH: string;
    GIT_CURRENT_BRANCH: string;
    GIT_AUTHOR_NAME: string;
    GIT_COMMIT_DATE: string;
  end;

  TFormInfo = class(TForm)
    pnlBottom: TPanel;
    btnClose: TButton;
    pnlDevelop: TPanel;
    lblGitLastUpdate: TLabel;
    lblGitLastUpdateValue: TLabel;
    lblGitLastTag: TLabel;
    lblGitLastTagValue: TLabel;
    lblVersionTypeDev: TLabel;
    lblBitDevValue: TLabel;
    lblVersionTypeDevValue: TLabel;
    lblGitLastHash: TLabel;
    lblGitLastHashValue: TLabel;
    btnCopy: TButton;
    lblGitCurrentBranch: TLabel;
    lblGitCurrentBranchValue: TLabel;
    lblGitAuthorName: TLabel;
    lblGitAuthorNameValue: TLabel;
    lblGitCommitDate: TLabel;
    lblGitCommitDateValue: TLabel;
    pnlRelease: TPanel;
    lblFileVersionNr: TLabel;
    lblFileVersionNrValue: TLabel;
    lblBuildNumber: TLabel;
    lblBuildNumberValue: TLabel;
    lblVersionType: TLabel;
    lblVersionTypeValue: TLabel;
    lblBitValue: TLabel;
    lblCreationTime: TLabel;
    lblCreationTimeValue: TLabel;
    lblCompanyName: TLabel;
    lblCompanyNameValue: TLabel;
    lblFileDescription: TLabel;
    lblFileDescriptionValue: TLabel;
    procedure btnCloseClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);

  private
    { Private-Deklarationen }
  public
    constructor Create(aOwner: TComponent; aInfoRecord: TExtractFileVersionData; aDeveloperVersionData: TDeveloperVersionData); reintroduce;
  end;

var
  FormInfo: TFormInfo;

implementation

{$REGION 'uses'}
uses
  Vcl.Clipbrd;
{$ENDREGION}

{$R *.dfm}

const cFormMaxWidth = 280;

constructor TFormInfo.Create(aOwner: TComponent; aInfoRecord: TExtractFileVersionData; aDeveloperVersionData: TDeveloperVersionData);
begin
  inherited Create(aOwner);

  Self.Width := cFormMaxWidth;
  Self.Constraints.MinWidth := cFormMaxWidth;
  Self.Constraints.MaxWidth := cFormMaxWidth;
  pnlDevelop.Left := 0;
  pnlDevelop.Top := 0;
  pnlRelease.Left := 0;
  pnlRelease.Top := 0;

  if aInfoRecord.IsRelease then
  begin
    lblVersionTypeValue.Caption := 'Release';
    pnlDevelop.Visible := false;
    pnlRelease.Visible := true;

    lblCreationTimeValue.Caption := aInfoRecord.FileVersionData.CreationTime;
    lblCompanyNameValue.Caption := aInfoRecord.FileVersionData.CompanyName;
    lblFileDescriptionValue.Caption := aInfoRecord.FileVersionData.FileDescription;
    lblFileVersionNrValue.Caption := aInfoRecord.FileVersionData.FileVersionNr;
    lblBuildNumberValue.Caption := aInfoRecord.FileVersionData.BuildNumber;
    if aInfoRecord.Is64BitOP then
      lblBitValue.Caption := '64 Bit'
    else
      lblBitValue.Caption := '32 Bit'
  end
  else
  begin
    lblVersionTypeDevValue.Caption := 'Entwicklerversion';
    pnlDevelop.Visible := true;
    pnlRelease.Visible := false;

    lblGitLastUpdateValue.Caption := aDeveloperVersionData.LASTUPDATE;
    lblGitLastTagValue.Caption := aDeveloperVersionData.GIT_LAST_TAG;
    lblGitLastHashValue.Caption := aDeveloperVersionData.GIT_LAST_HASH;
    lblGitCurrentBranchValue.Caption := aDeveloperVersionData.GIT_CURRENT_BRANCH;
    lblGitAuthorNameValue.Caption := aDeveloperVersionData.GIT_AUTHOR_NAME;
    lblGitCommitDateValue.Caption := aDeveloperVersionData.GIT_COMMIT_DATE + ' Uhr';

    if aInfoRecord.Is64BitOP then
      lblBitDevValue.Caption := '64 Bit'
    else
      lblBitDevValue.Caption := '32 Bit'
  end;
end;

procedure TFormInfo.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFormInfo.btnCopyClick(Sender: TObject);
begin
  Clipboard().AsText := lblGitLastHashValue.Caption;
end;

end.
