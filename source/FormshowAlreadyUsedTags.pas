unit FormshowAlreadyUsedTags;

interface

uses
  replaceTags,
  usedTags,

  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls;

type
  TFormalreadyUsed = class(TForm)
    lFileName: TLabel;
    sgUsedTags: TStringGrid;
    lPath: TLabel;
    procedure FormDestroy(Sender: TObject);
  private
    fReplaceTags: TReplaceTags;
    fUsedTags: TUsedTags;
    procedure setFile(const aFile: string);

    { Private-Deklarationen }
  public
    procedure SetPASFileUsedTags(aFileName: string);
    constructor Create (aOwner: TComponent; aReplaceTags: TReplaceTags); reintroduce;

    { Public-Deklarationen }
  end;

var
  fFileName: string;
  FormalreadyUsed: TFormalreadyUsed;

implementation
uses
  System.RegularExpressions;
{$R *.dfm}

{ TFormalreadyUsed }

constructor TFormalreadyUsed.Create(aOwner: TComponent; aReplaceTags: TReplaceTags);
begin
  inherited Create(aOwner);
  fReplaceTags := aReplaceTags;

  sgUsedTags.Cells[0,0] := 'Tag';
  sgUsedTags.Cells[1,0] := 'Eintrag';
end;

procedure TFormalreadyUsed.setFile(const aFile: string);
begin
  fFileName := aFile;
  lFileName.Caption := ExtractFileName(aFile);
  lPath.Caption := ExtractFilePath(aFile);
end;

procedure TFormalreadyUsed.SetPASFileUsedTags(aFileName: string);
var
  vTagList: TArray<Integer>;
  vLangEntries: TArray<string>;
  i: Integer;
begin
  setFile(aFileName);
  if not fReplaceTags.IsLanguageFileLoaded then
    sgUsedTags.Cells[1,1] := 'Sprachdatei nicht geladen'
  else
  begin
    vTagList := fReplaceTags.LookForUsedTagsInFile(fFileName);
    sgUsedTags.RowCount := High(vTagList)+2;
    for i := 0 to High(vTagList) do
      sgUsedTags.Cells[0,i+1] := IntToStr(vTagList[i]);
    if fReplaceTags.IsLanguageFileLoaded then
    begin
      vLangEntries := fReplaceTags.LangEntrysForTags(vTagList);
      for i := 0 to High(vLangEntries) do
        sgUsedTags.Cells[1,i+1] := vLangEntries[i];
    end;
  end;
end;

procedure TFormalreadyUsed.FormDestroy(Sender: TObject);
begin
  fUsedTags.Free;
end;

end.
