object FormSettings: TFormSettings
  Left = 0
  Top = 0
  Caption = 'Einstellungen'
  ClientHeight = 305
  ClientWidth = 609
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lTopic1: TLabel
    Left = 8
    Top = 56
    Width = 75
    Height = 13
    Caption = 'Standard Pfade'
  end
  object chkOverWriteFiles: TCheckBox
    Left = 8
    Top = 201
    Width = 234
    Height = 17
    Caption = 'Dateien standardm'#228#223'ig '#252'berschreiben'
    TabOrder = 0
  end
  object chkOverwriteOrigins: TCheckBox
    Left = 8
    Top = 224
    Width = 234
    Height = 17
    Caption = 'Quelldateien standardm'#228#223'ig '#252'berschreiben'
    TabOrder = 1
  end
  object eExprFile: TLabeledEdit
    Left = 8
    Top = 24
    Width = 593
    Height = 21
    EditLabel.Width = 76
    EditLabel.Height = 13
    EditLabel.Caption = 'Expressions File'
    TabOrder = 2
    OnKeyPress = eExprFileKeyPress
  end
  object bCancel: TButton
    Left = 511
    Top = 271
    Width = 87
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 3
    OnClick = bCancelClick
  end
  object bLeave: TButton
    Left = 167
    Top = 270
    Width = 75
    Height = 25
    Caption = 'Speichern'
    TabOrder = 4
    OnClick = bLeaveClick
  end
  object bLoadActualFile: TButton
    Left = 8
    Top = 271
    Width = 153
    Height = 25
    Caption = 'lade aktuelle Einstellungen'
    TabOrder = 5
    OnClick = bLoadActualFileClick
  end
  object eDirDefLanguageFile: TLabeledEdit
    Left = 8
    Top = 136
    Width = 593
    Height = 21
    EditLabel.Width = 57
    EditLabel.Height = 13
    EditLabel.Caption = 'Sprachdatei'
    TabOrder = 6
  end
  object eDirDefSRCs: TLabeledEdit
    Left = 8
    Top = 96
    Width = 593
    Height = 21
    EditLabel.Width = 139
    EditLabel.Height = 13
    EditLabel.Caption = 'Verzeichnis Quellcodedateien'
    TabOrder = 7
  end
  object eDirTargetFiles: TLabeledEdit
    Left = 8
    Top = 176
    Width = 593
    Height = 21
    EditLabel.Width = 183
    EditLabel.Height = 13
    EditLabel.Caption = 'Zielverzeichnis f'#252'r bearbetete Dateien'
    TabOrder = 8
  end
  object chkSearchPAS: TCheckBox
    Left = 413
    Top = 200
    Width = 185
    Height = 17
    Caption = 'Suche nach PAS Dateien'
    TabOrder = 9
  end
  object chkSearchDFM: TCheckBox
    Left = 413
    Top = 223
    Width = 185
    Height = 17
    Caption = 'Suche nach DFM Dateien'
    TabOrder = 10
  end
  object bOK: TButton
    Left = 410
    Top = 270
    Width = 95
    Height = 25
    Caption = #220'bernehmen'
    TabOrder = 11
    OnClick = bOKClick
  end
  object chkPreferLastTag: TCheckBox
    Left = 413
    Top = 247
    Width = 185
    Height = 17
    Caption = 'schlage letzten freien Tag vor'
    TabOrder = 12
  end
end
