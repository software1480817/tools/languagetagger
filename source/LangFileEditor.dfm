object LanguageFileEditor: TLanguageFileEditor
  Left = 137
  Top = 77
  Caption = 'LanguageFileEditor'
  ClientHeight = 252
  ClientWidth = 712
  Color = clBtnFace
  Constraints.MinHeight = 290
  Constraints.MinWidth = 728
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  DesignSize = (
    712
    252)
  PixelsPerInch = 96
  TextHeight = 13
  object lDirLangFiles: TLabel
    Left = 271
    Top = 35
    Width = 59
    Height = 13
    Caption = 'lDirLangFiles'
  end
  object eDirSources: TEdit
    Left = 8
    Top = 8
    Width = 697
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'eDirSources'
  end
  object lstFilesSources: TListBox
    Left = 8
    Top = 32
    Width = 257
    Height = 129
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
    OnClick = lstFilesSourcesClick
    OnMouseEnter = setFocusOnMouseEnter
  end
  object sgEntries: TStringGrid
    Left = 271
    Top = 82
    Width = 434
    Height = 132
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clBtnFace
    ColCount = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 2
    OnDrawCell = sgEntriesDrawCell
    OnMouseDown = sgEntriesMouseDown
    ColWidths = (
      72
      150)
  end
  object cbLangFile: TComboBox
    Left = 271
    Top = 55
    Width = 266
    Height = 21
    TabOrder = 3
    Text = 'Sprachdatei ausw'#228'hlen'
    OnChange = cbLangFileChange
  end
  object bReDo: TButton
    Left = 271
    Top = 218
    Width = 130
    Height = 27
    Anchors = [akLeft, akBottom]
    Caption = 'verwerfe '#196'nderungen'
    TabOrder = 4
    OnClick = bReDoClick
  end
  object bSave: TButton
    Left = 575
    Top = 218
    Width = 130
    Height = 27
    Anchors = [akRight, akBottom]
    Caption = 'speichern'
    TabOrder = 5
    OnClick = bSaveClick
  end
  object chkAutoSync: TCheckBox
    Left = 407
    Top = 217
    Width = 162
    Height = 27
    Anchors = [akLeft, akBottom]
    Caption = 'Synchronisiere Sprachdateien beim Beenden'
    TabOrder = 6
    WordWrap = True
  end
  object bFilterFilesWithTags: TButton
    Left = 8
    Top = 219
    Width = 153
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Filter Dateien mit Tags'
    TabOrder = 7
    OnClick = bFilterFilesWithTagsClick
  end
  object chkOnlyUnreleasedTags: TCheckBox
    Left = 8
    Top = 196
    Width = 257
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'nur nach nicht freigegebene Tags suchen'
    TabOrder = 8
  end
  object eTagToLookFor: TEdit
    Left = 8
    Top = 169
    Width = 65
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 9
    Text = 'Tag'
    OnClick = eTagToLookForEnter
    OnEnter = eTagToLookForEnter
    OnKeyPress = eTagToLookForKeyPress
  end
  object bSearchSpecialTag: TButton
    Left = 79
    Top = 167
    Width = 186
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'suche speziellen Tag'
    TabOrder = 10
    OnClick = bSearchSpecialTagClick
  end
  object bResetFilter: TButton
    Left = 167
    Top = 219
    Width = 98
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'zur'#252'cksetzen'
    TabOrder = 11
    OnClick = bResetFilterClick
  end
end
