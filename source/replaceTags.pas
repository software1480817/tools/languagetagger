unit replaceTags;

interface

uses
  System.Types,
  System.IOUtils,
  System.SysUtils,
  System.SyncObjs,
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  Spring.Collections;

  const     MARK_NEW_LANG_ENTRY = '#!';

type
  TCommandLinePreSettings = record
  private
  public
    langdir: string;
    sources: string;
    parentdir: Integer;
    filterFilesOnShow: Boolean;
    function getlangdir: string;
    function getsources: string;
    procedure Reset;
  end;

  EReplaceTags = class(Exception) end;
  EReplaceTagsLanguage = class(EReplaceTags) end;
  TSettings = class
  private
    fLanguagePath: string;
    fLanguageFiles: TArray<string>;
    procedure setLanguageFiles(const aPath: string);
  public
    IsLoaded: Boolean;
    Expressions: string;
    Sources: string;
    OverWrite: Boolean;
    OverWriteOrigins: Boolean;
    DirectoryTarget: string;
    DFMSearch: Boolean;
    PASSearch: Boolean;
    PreferLastTag: Boolean;
    LangSearch: array[0..1] of string;
    property LanguagePath: string read fLanguagePath write setLanguageFiles;
    property LanguageFiles: TArray<string> read fLanguageFiles;
  end;
  TFunctionExpression = array of record
    Name:   string; // Name of the function
    Tag:    Integer; // number of parameter
    DefMsg: Integer; // number of parameter
  end;
  TFunctionValues = TArray<string>;
  TFunctionDirectAssignment = TArray<string>;
  TWorkingWork =  function(const aS1, aS2: string): Integer of object;
  TWorkingCallBack =  procedure(Sender: TObject) of object;

{ ##### Threads ##### }
EWorkingThread = class(Exception);
TWorkingThread = class(TThread)
  private
    fException: TObject;
    fWorkToDo: TWorkingWork;
    fs1, fs2: string;
  public
    procedure Execute; override;
    constructor Create(const aWorkToDo: TWorkingWork; s1, s2: string);
    procedure CheckAndRaiseException;
  end;

  { #### Classes #### }
TReplaceTags = class
  strict private

  const
    CP_ASSIGNMENT = 3;
    CP_RESULT = 2;
    CP_DIGITS = 1;
    CP_ZERO   = 0;
    FILES_PAS = 0;
    FILES_DFM = 1;

  type
    TNewTagsForLanguageFile = record
      Tag: Integer;
      Msg: string;
      constructor Create(const aTag: Integer; aMsg: string);
    end;
    TFilesToProcess = record
      Name: string;
      Tags: TArray<TNewTagsForLanguageFile>;
    end;
    TFilesSources = class
      Name: string;
      IsSelected: Boolean;
    end;
  var
    fFilesSources:             TObjectList<TFilesSources>;
    fFuncExp:                  TFunctionExpression;
    fFuncValues:               TFunctionValues;
    fFuncDirectAssignment:     TFunctionDirectAssignment;
    fTagsLangFile:             TList<Integer>;
    fCountFoundExpInSRC:       Integer;
    fGapSize:                  Integer;
    fMaxTagsInLangFile:        Integer;
    fIsLanguageFileLoaded:     Boolean;
    fLangEntrys:               TDictionary<Integer, string>;
    fFilesToProcess:           TList<TFilesToProcess>;
    fCSProcessingFiles:        TCriticalSection;
    fPathSRCs:                 string;
    fFilesContent:             TStringList;
    fSettings:                 TSettings;
    fIsLangSynchronized:       Boolean;

    fLangEntriesAdded:     IList<TNewTagsForLanguageFile>;
    fTagsUsed:      TList<Integer>;
    fThreadsCS:     TCriticalSection;

    function createPattern(const aTagType: Integer): string;
    function countOccurrencesOfPattern(const aFile, aPattern: string): Integer;
    function GetCountTaglist: Integer;
    function getSources(const aType: Integer): TStringDynArray;
    procedure isPossibleStartProcessingFiles(const aStartTag: Integer);
    procedure processOneSRCFilePAS(const aIndexIDOfFilesToProcess: Integer);
    procedure processOneSRCFileDFM(const aIndexIDOfFilesToProcess: Integer);
    function processOneSRCFile2Thread(const Idx, null: string): Integer;
    procedure processGermanLanguageFile;
    procedure processOtherLanguageFiles;
    procedure processSRCFilesThread;
    function ThreadsAreStillWorking(aThreads: array of TWorkingThread): Boolean;
    function lookForTagsAsResults(const aContent: TStringList): TArray<Integer>;
    function lookForTagsDirectAssignments(const aContent: TStringList): TArray<Integer>;
    procedure addTagIfNotExisting(const aToAdd: TArray<Integer>; aTagList: TList<Integer>);
    function lookForUsedTagsInFileThread(const aFile, s2: string): Integer;
    function getTagsLangFile: TArray<Integer>;
    function lookForUsedTagsInDFM(const aFile: string): TArray<Integer>;  // eine Datei
    function replaceUmlautsAndApostrophs(vLine: string): string;

  public
    CommandLinePreSettings: TCommandLinePreSettings;

    function loadExpressionsFile(): Boolean;
    function loadLanguageFile(): Boolean;
    procedure ProcessLanguageFiles;
    procedure LoadFilesSourcesFromPath(const aPathSources: string = '');
    procedure SelectFilesFromSources(const aIDs: array of Integer);
    procedure SelectFilesFromSourcesALL;
    procedure RemoveFilesFromSourcesNotSelected;
    procedure FilterFilesToProcess;
    function GetFilesSources: TArray<string>;
    function GetFileNameOfIDSources(const aID: Integer): string;
    function GetFilesSourcesSelected: TArray<string>;
    function GetFileIDSources(const aName: string): Integer;
    function searchExpressionsInSRC(): Integer;
    function showGapsOfXTags: TArray<Integer>;
    function GapSize(const aTag: Integer): Integer;
    procedure processFiles(const aStartTag: Integer);
    function SearchPattern0Tags: string;
    function LookForUsedTagsInFile(const aFN: string): TArray<Integer>;     // eine Datei - egal ob PAS oder DFM
    function ShowTagsInGroups(const aTags: TArray<Integer>): string;
    function LangEntrysForTags(const aTags: TArray<Integer>): TArray<string>;
    function ListOfUsedTagsInFiles(): TArray<Integer>;  //  durchsucht Dateiliste -endung egal
    function NumberInSRCsNotExistent(const aTags: TArray<Integer>): TArray<Integer>;
    function OccurrencesOfTagsInSRCs(const aTag: Integer; out aFiles: TArray<string>): TArray<string>;
    function GetLangEntry(aTag: Integer): string;
    procedure DeleteTagsFromLangFile(const aTags: TArray<Integer>);
    function GetListOfFilesToProcess: TArray<string>;
    procedure LogFilesToProcess(aLogFile: string); // f�r Kommandozeilenbenutzung nur

    constructor Create;
    destructor Destroy; override;

    property Settings: TSettings read fSettings write fSettings;
    property CountTagList:         Integer read GetCountTaglist;
    property IsLanguageFileLoaded: Boolean read fIsLanguageFileLoaded;
    property TagsLanguageFile:     TArray<Integer> read getTagsLangFile;
    property PathSources:          string read fPathSRCs write fPathSRCs;
    property IsLangFilesSynchronized: Boolean read fIsLangSynchronized;
  end;

implementation

uses
  Winapi.Windows,
  System.StrUtils,
  System.RegularExpressions,
  common.SafeMemIni,
  Vcl.Forms;

const
  THREADS_MAX = 50;

  { ########### TReplaceTags ######### }
  { --- private methods --- }
function TReplaceTags.GetCountTaglist: Integer;
begin
  Result := fTagsLangFile.Count;
end;


function TReplaceTags.getSources(const aType: Integer): TStringDynArray;
var
  vFileType: string;
begin
  vFileType := EmptyStr;
  if aType = FILES_PAS then
    vFileType := '*.pas';
  if aType = FILES_DFM then
    vFileType := '*.dfm';
  Result := TDirectory.GetFiles(fPathSRCs, vFileType, TSearchOption.soAllDirectories);
end;

function TReplaceTags.getTagsLangFile: TArray<Integer>;
begin
  Result := fTagsLangFile.ToArray;
end;

procedure TReplaceTags.ProcessLanguageFiles;
begin
  if not fIsLanguageFileLoaded then
    raise EReplaceTags.Create('Deutsche Sprachdatei ist noch nicht geladen');
  processGermanLanguageFile;
  if not (fSettings.LanguageFiles[1] = '') then
    processOtherLanguageFiles
  else
    raise EReplaceTagsLanguage.Create('keine anderen Sprachdateien zur Synchronisierung im Sprachverzeichnis gefunden');
end;

procedure TReplaceTags.processGermanLanguageFile;
var
  vNewFile:        string;
  vINI:            TSafeIni;
  vFileID, vTagID: Integer;
  vIDs:            TList<Integer>;
  vContent:        TStringList;
  vDict:           TDictionary<string, string>;
  i:         Integer;
  vMaxEntry:       Integer;
  vEntry:          string;
  vActualLangFile: string;
  vNewCheckEntry: TNewTagsForLanguageFile;
begin
  vActualLangFile := fSettings.LanguageFiles[0];
  vNewFile := fSettings.DirectoryTarget + '\' + ExtractFileName(fSettings.LanguageFiles[0]);
  if fSettings.OverWriteOrigins then
    vINI := TSafeIni.Create(vActualLangFile)
  else
  begin
    CopyFile(PWideChar(vActualLangFile), PWideChar(vNewFile), not fSettings.OverWrite);
    vINI := TSafeIni.Create(vNewFile);
  end;
  vIDs := TList<Integer>.Create;
  vDict := TDictionary<string, string>.Create;
  vContent := TStringList.Create;
  vContent.NameValueSeparator := '=';
  try
    vMaxEntry := 0;
    // read old content of section "Liste"
    vINI.ReadSectionValues('Liste', vContent);
    for i := 0 to vContent.Count - 1 do
    begin
      try
        vDict.Add(vContent.Names[i], vContent.ValueFromIndex[i]);
      except
        on E: EListError do
        begin
          raise EReplaceTagsLanguage.Create('Doppelter Eintrag in Sprachdatei:' + sLineBreak
              + vActualLangFile + sLineBreak
              + 'bei Eintrag: ' + vContent.Names[i]);
        end;
      end;
      if vMaxEntry < StrToInt(vContent.Names[i]) then
        vMaxEntry := StrToInt(vContent.Names[i]);
    end;
    for vFileID := 0 to fFilesToProcess.Count - 1 do
      if not (fFilesToProcess.Items[0].Tags[0].Msg = '') then
        for vTagID := 0 to High(fFilesToProcess[vFileID].Tags) do
        begin
          try
            vDict.Add(IntToStr(fFilesToProcess[vFileID].Tags[vTagID].Tag),
                fFilesToProcess[vFileID].Tags[vTagID].Msg);
          except
            on E: EListError do
              raise EReplaceTagsLanguage.Create('Doppelter Eintrag in Sprachdatei:'
                + vActualLangFile + sLineBreak
                + 'bei Eintrag: ' + IntToStr(fFilesToProcess[vFileID].Tags[vTagID].Tag));
          end;
          fTagsLangFile.Add(fFilesToProcess[vFileID].Tags[vTagID].Tag);
          // TODO -oRobert: processGermanLanguageFile - comparer ben�tigt
          vNewCheckEntry := TNewTagsForLanguageFile.Create(
              fFilesToProcess[vFileID].Tags[vTagID].Tag,
              fFilesToProcess[vFileID].Name.Substring(fSettings.Sources.Length));
          if not fLangEntriesAdded.Contains(vNewCheckEntry) then
            fLangEntriesAdded.Add(vNewCheckEntry);
          if vMaxEntry < vNewCheckEntry.Tag then
            vMaxEntry := vNewCheckEntry.Tag;
        end;
    vINI.EraseSection('Liste');
    for i := 0 to vMaxEntry do
      if vDict.TryGetValue(IntToStr(i), vEntry) then
        vINI.WriteString('Liste', IntToStr(i), vDict[IntToStr(i)]);
    for i  := 0 to fLangEntriesAdded.Count -1 do
      vINI.WriteString('zu pruefen', fLangEntriesAdded[i].Tag.ToString, fLangEntriesAdded[i].Msg);
    vINI.WriteString('Kopf', 'Dateiname', vNewFile);
    vINI.WriteString('Kopf', 'Stand', FormatDateTime('dd.mm.yyyy', Now));
    fTagsLangFile.Sort;
    vINI.WriteInteger('Kopf', 'Eintraege', fTagsLangFile.Last);
    vINI.UpdateFile;
  finally
    vIDs.Free;
    vContent.Free;
    vDict.Free;
    vINI.Free;
  end;
  fIsLangSynchronized := False;
end;

procedure TReplaceTags.processOtherLanguageFiles;
  function procLangFile(const aTagsFrom: TList<Integer>;
    aDictFrom, aDictOut: TDictionary<Integer, string>; aLangFile: string)
    : TArray<Integer>;
  var
    vSL: TStringList;
    vLangFile, vNewFile, vEntry: string;
    i, vTagFrom: Integer;
    vTagsOut, vTagsNew: TList<Integer>;
    vINIFile: TSafeIni;
  begin
    aDictOut.Clear;
    vLangFile := aLangFile;
    if not fSettings.OverWriteOrigins then
    begin
      vNewFile := fSettings.DirectoryTarget + '\' + ExtractFileName(vLangFile);
      if not(fSettings.OverWrite) and FileExists(vNewFile) then
        raise EReplaceTags.Create
          ('Zieldateien existieren bereits - Option �berscheiben aktivieren oder anderes Verzeichnis w�hlen');
      CopyFile(PWideChar(vLangFile), PWideChar(vNewFile),
        not fSettings.OverWrite);
      vLangFile := vNewFile;
    end;
    vINIFile := TSafeIni.Create(vLangFile);
    vTagsOut := TList<Integer>.Create;
    vTagsNew := TList<Integer>.Create;
    vSL := TStringList.Create;
    vINIFile.ReadSection('Liste', vSL);
    for i := 0 to vSL.Count - 1 do
      try
        aDictOut.Add(StrToInt(vSL[i]), vINIFile.ReadString('Liste',
          vSL[i], ''));
      except
        on E: EListError do
          raise EReplaceTagsLanguage.Create('Doppelter Eintrag in Sprachdatei:'
            + vLangFile + sLineBreak + 'bei Eintrag: ' + vSL[i]);
      end;

    vINIFile.ReadSection('zu pruefen', vSL);
    // TODO -oRobert: procLangFile - alte unit noch mit auslesen
    for i := 0 to vSL.Count - 1 do
      vTagsNew.Add(StrToInt(vSL[i]));
    vINIFile.EraseSection('Liste');
    vINIFile.EraseSection('zu pruefen');
    vINIFile.UpdateFile;
    // Einlesen ende

    for vTagFrom in aTagsFrom do
    begin
      if aDictFrom.TryGetValue(vTagFrom, vEntry) then
      begin
        if aDictOut.TryGetValue(vTagFrom, vEntry) then
        begin
          vTagsOut.Add(vTagFrom);
          vINIFile.WriteString('Liste', IntToStr(vTagFrom),
            aDictOut.Items[vTagFrom]);
        end
        else
        begin
          vINIFile.WriteString('Liste', IntToStr(vTagFrom),
            aDictFrom.Items[vTagFrom]);
          vTagsOut.Add(vTagFrom);
          vTagsNew.Add(vTagFrom);
        end;
      end;
    end;
    i := 0;
    while i < vTagsNew.Count do
    begin
      if not vTagsOut.Contains(vTagsNew.Items[i]) then
        vTagsNew.Delete(i)
      else
        inc(i);
    end;
    vINIFile.WriteString('Kopf', 'Dateiname', vLangFile);
    vINIFile.WriteString('Kopf', 'Stand', FormatDateTime('dd.mm.yyyy', Now));
    if vTagsOut.Last > vTagsNew.Last then
      vINIFile.WriteInteger('Kopf', 'Eintraege', vTagsOut.Last)
    else
      vINIFile.WriteInteger('Kopf', 'Eintraege', vTagsNew.Last);
    for i := 0 to vTagsNew.Count - 1 do
      if vTagsOut.Contains(vTagsNew.Items[i]) then
        vTagsOut.Delete(vTagsOut.IndexOf(vTagsNew.Items[i]));

    vTagsNew.Sort;
    vTagsOut.Sort;
    for i := 0 to vTagsNew.Count - 1 do
      vINIFile.WriteInteger('zu pruefen', IntToStr(vTagsNew.Items[i]),
        vTagsNew.Items[i]);
    Result := vTagsOut.ToArray;
    vINIFile.UpdateFile;
    vINIFile.Free;
    vSL.Free;
    vTagsOut.Free;
    vTagsNew.Free;
  end;

var
  vEnFile: string;
  vDictEN, vDictDummy: TDictionary<Integer, string>;
  vTagsDe, vTagsEn: TList<Integer>;
  i: Integer;
begin
  vEnFile := fSettings.LanguageFiles[1];
  vTagsDe := TList<Integer>.Create;
  vTagsEn := TList<Integer>.Create;
  vDictEN := TDictionary<Integer, string>.Create;
  vDictDummy := TDictionary<Integer, string>.Create;
  vTagsDe.AddRange(fTagsLangFile.ToArray);
  { TODO  als neu markierte englische Eintr�ge werden ohne zu zwinkern in die zb. spanische �bernommen }
  { TODO  "zu pr�fen" marker raus, wenn eintrag gel�scht wird }

  for i := 0 to fLangEntriesAdded.Count-1 do
    if vTagsDe.Contains(fLangEntriesAdded.Items[i].Tag) then
      vTagsDe.Delete(vTagsDe.IndexOf(fLangEntriesAdded.Items[i].Tag));
  try
    vTagsEn.AddRange( procLangFile(vTagsDe, fLangEntrys, vDictEN, vEnFile) );
    for i := 2 to High(fSettings.LanguageFiles) do
      procLangFile(vTagsEn, vDictEN, vDictDummy, fSettings.LanguageFiles[i]);
  finally
    vTagsEn.Free;
    vTagsDe.Free;
    vDictEN.Free;
    vDictDummy.Free;
  end;
  fIsLangSynchronized := True;
end;

function TReplaceTags.replaceUmlautsAndApostrophs(vLine: string): string;
begin
  Result := TRegEx.Replace(vLine,  '''?#228''?', '�');
  Result := TRegEx.Replace(Result, '''?#246''?', '�');
  Result := TRegEx.Replace(Result, '''?#252''?', '�');
  Result := TRegEx.Replace(Result, '''?#196''?', '�');
  Result := TRegEx.Replace(Result, '''?#214''?', '�');
  Result := TRegEx.Replace(Result, '''?#220''?', '�');
  Result := TRegEx.Replace(Result, '''?#223''?', '�');
  Result := TRegEx.Replace(Result, '''?#176''?', '�');
  Result := TRegEx.Replace(Result, '''? *\+ *', '');
  Result := TRegEx.Replace(Result, '^''', '');
  Result := TRegEx.Replace(Result, '''$', '');
  Result := TRegEx.Replace(Result, '''#181''', '�');
  Result := TRegEx.Replace(Result, '''?#13#10''?', ' #nl ');
end;

procedure TReplaceTags.processOneSRCFileDFM(
  const aIndexIDOfFilesToProcess: Integer);
var
  vFileName, vNewFileName: string;
  vCont: TStringList;
  vLineIdx, i: Integer;
  vCombi : record
    Tag: Integer;
    Entry: string;
    Found: Integer;
    Nr: Integer;
  end;
  vNewline, vSecondNewLine: string;
  vMatch: TMatch;
begin
  fCSProcessingFiles.Enter;
    vFileName := fFilesToProcess[aIndexIDOfFilesToProcess].Name;
  fCSProcessingFiles.Leave;
  vCont := TStringList.Create;
  vCont.LoadFromFile(vFileName);
  vCombi.Tag := 0;
  vCombi.Entry := EmptyStr;
  vCombi.Found := 0;
  vCombi.Nr := 0;
  for vLineIdx := 0 to vCont.Count-1 do
  begin
    if TRegEx.IsMatch(vCont[vLineIdx], '^ *object |^ *end *$', [roIgnoreCase, roMultiLine]) then
      vCombi.Found := 0;
    if TRegEx.IsMatch(vCont[vLineIdx], '^ *Tag *= *-1', [roMultiLine, roIgnoreCase]) then
      begin
        vCont[vLineIdx] := TRegEx.Replace(vCont[vLineIdx], '-1', IntToStr(fFilesToProcess.Items[aIndexIDOfFilesToProcess].Tags[vCombi.Nr].Tag));
        Inc(vCombi.Found);
        if vCombi.Found = 2 then
        begin
          vCombi.Found := 0;
          Inc(vCombi.Nr);
        end;
      end;
    vMatch := TRegEx.Match(vCont[vLineIdx], '\bCaption *= *', [roMultiLine, roIgnoreCase]);
    if vMatch.Success then
      begin
        i := 1;
        vNewline := vCont[vLineIdx].Substring(vMatch.Index + vMatch.Length - 1);
        vMatch := TRegEx.Match(vCont[vLineIdx+i], '^[\t ]*''', [roMultiLine]);
        while vMatch.Success do
        begin
          vSecondNewLine := vCont[vLineIdx+i].Substring(vMatch.Length - 1);
          vSecondNewLine := replaceUmlautsAndApostrophs(vSecondNewLine);
          vNewline := vNewline + vSecondNewLine;
          Inc(i);
          vMatch := TRegEx.Match(vCont[vLineIdx+i], '^[\t ]*''', [roMultiLine]);
        end;

        vNewline := replaceUmlautsAndApostrophs(vNewline);
        fCSProcessingFiles.Enter;
          fFilesToProcess.Items[aIndexIDOfFilesToProcess].Tags[vCombi.Nr].Msg := vNewline;
        fCSProcessingFiles.Leave;
        Inc(vCombi.Found);
        if vCombi.Found = 2 then
        begin
          vCombi.Found := 0;
          Inc(vCombi.Nr);
        end;
      end;
      if vCombi.Nr > High(fFilesToProcess.Items[aIndexIDOfFilesToProcess].Tags) then
        Break;
  end;
  if fSettings.OverWriteOrigins then
    vCont.SaveToFile(vFileName)
  else
  begin
    vNewFileName := fSettings.DirectoryTarget +'\'+ ExtractFileName(vFileName);
    vCont.SaveToFile(vNewFileName);
  end;
  fIsLangSynchronized := False;
  vCont.Free;
end;

procedure TReplaceTags.processOneSRCFilePAS(const aIndexIDOfFilesToProcess: Integer);
var
  vFileName, vNewFileName:   string;
  vContent:                  TStringList;
  i, j, vTagIndex:           Integer;
  vPattern:                  string;
  vLine:           string;
  vMatch, vMatch2, vMatch3:  TMatch;
  vMatches:   TMatchCollection;
  vSplit:                    TArray<string>;
  vMsg, vFunction, vNewCont: string;
begin
  fCSProcessingFiles.Enter;
  vFileName := fFilesToProcess[aIndexIDOfFilesToProcess].Name;
  fCSProcessingFiles.Leave;
  vContent := TStringList.Create;
  vPattern := createPattern(CP_ZERO);
  vTagIndex := 0;
  try
    vContent.LoadFromFile(vFileName);
    vMatch := TRegEx.Match(vContent.Text, vPattern, [roIgnoreCase, roMultiLine]);
    while vMatch.Success do
    begin
      vLine := vMatch.Value;
      vMsg := EmptyStr;
      vFunction := EmptyStr;
      for j := 0 to High(fFuncExp) do
      begin
        if TRegEx.IsMatch(vLine, fFuncExp[j].Name + '\s*\(', [roIgnoreCase]) then
        begin
          vMatch2 := TRegEx.Match (vLine, '\([\s\S]*', [roIgnoreCase, roMultiLine]);
          // Funtionsinhalt separieren
          if not vMatch2.Success then
            raise Exception.Create('Fehler beim parsen der Funktion:' + sLineBreak + vLine);
          vFunction := vMatch2.Value.Substring(1); // Klammer entfernen
          //            vFunction := TRegEx.Replace(vFunction, '\r\n[\t ]*', ' '); // Zeilenumbr�che entfernen
          vFunction := TRegEx.Replace(vFunction, '[\t ]*\/\/.*|[ \t]*{[\s\S]*}', '').Trim;
          vMatches := TRegEx.Matches(vFunction, '\s*(''[^'']+'')+\s*|[^,]+', [roMultiLine, roIgnoreCase]);
          SetLength(vSplit, 0);
          for vMatch3 in vMatches do
          begin
            SetLength(vSplit, Length(vSplit)+1);
            vSplit[High(vSplit)] := vMatch3.Value;
          end;
          vMsg := vSplit[fFuncExp[j].DefMsg].Trim;
//          vMsg := TRegEx.Replace(vMsg, '[\t ]*\/\/.*|[ \t]*{[\s\S]*}', '').Trim;   // Kommentare entfernen
          vMsg := vMsg.Substring(1, Length(vMsg) - 2); // Apostrophe entfernen
          fCSProcessingFiles.Enter;
            vSplit[fFuncExp[j].Tag] :=
              IntToStr(fFilesToProcess[aIndexIDOfFilesToProcess].Tags[vTagIndex].Tag);
            fFilesToProcess[aIndexIDOfFilesToProcess].Tags[vTagIndex].Msg := vMsg;
          fCSProcessingFiles.Leave;
          vNewCont := vContent.Text.Substring(0, vMatch.Index + vMatch2.Index - 1);
          // alles bis zur ( schreiben
          for i := 0 to High(vSplit) do
          begin
            vNewCont := vNewCont + vSplit[i];
            if not(i = High(vSplit)) then
              vNewCont := vNewCont + ',';
          end;
          vNewCont := vNewCont + vContent.Text.Substring(vMatch.Index + vMatch.Length - 1);//,
 //           vContent.Text.Length - vMatch.Index + vMatch.Length + 1);
          vContent.Text := vNewCont;
          vMatch := TRegEx.Match(vContent.Text, vPattern, [roIgnoreCase, roMultiLine]);
          Inc(vTagIndex);
          Break;
        end;
      end;
    end;
    vNewFileName := fSettings.DirectoryTarget + '\' + ExtractFileName(vFileName);
    if fSettings.OverWriteOrigins then
      vContent.SaveToFile(vFileName)
    else
      vContent.SaveToFile(vNewFileName);
  finally
    vContent.Free;
  end;
  fIsLangSynchronized := False;
end;

procedure TReplaceTags.isPossibleStartProcessingFiles(const aStartTag: Integer);
var
  i: Integer;
begin
  if not fIsLanguageFileLoaded then
    raise Exception.Create('Sprachdatei noch nicht geladen');
  if fFilesToProcess.Count = 0 then
    raise Exception.Create('noch keine Quellcode-Dateien gew�hlt oder keine' +
      ' Ausdr�cke gefunden, welche bearbetet werden k�nnten');
  if not fSettings.IsLoaded then
    raise Exception.Create('Expressions-File ist noch nicht geladen');
  if aStartTag = 0 then
    raise Exception.Create('Ung�ltiger Start-Tag');
  if fGapSize < fCountFoundExpInSRC then
    raise Exception.Create
      ('Nicht gen�gend Platz in der Sprachdatei f�r die ben�tigte Menge an Tags');
  if not DirectoryExists(fSettings.DirectoryTarget) then
    raise Exception.Create('Zielverzeichnis nicht gew�hlt oder existiert nicht');
  if (not fSettings.OverWrite) and (not fSettings.OverWriteOrigins) then
  begin
    for i := 0 to fFilesToProcess.Count - 1 do
    begin
      if FileExists(fSettings.DirectoryTarget + '\' + ExtractFileName(fFilesToProcess[i].Name)) then
        raise Exception.Create('Datei "' + ExtractFileName(fFilesToProcess[i].Name) + '"' +
          sLineBreak + 'ist im Zielverzeichnis schon vorhanden');
    end;
    if FileExists(fSettings.DirectoryTarget + '\' + ExtractFileName(fSettings.LanguageFiles[0])) then
      raise Exception.Create('Sprachdatei existiert schon im Zielverzeichnis');
  end;
end;

function TReplaceTags.createPattern(const aTagType: Integer): string;
var
  i:          Integer;
  vExpID:     Integer;
  vTempS:     string;
  vLastField: Integer;
begin
  Result := EmptyStr;
  if aTagType = CP_RESULT then
  begin
    Result := '^ *function +.*\.(';
    for i := 0 to High(fFuncValues) do
    begin
      Result := Result + fFuncValues[i];
      if not(i = High(fFuncValues)) then
        Result := Result + '|';
    end;
    Result := Result + ')';
    Exit;
  end;
  if aTagType = CP_ASSIGNMENT then
  begin
    Result := '(';
    for i := 0 to High(fFuncDirectAssignment) do
    begin
      Result := Result + fFuncDirectAssignment[i];
      if not(i = High(fFuncDirectAssignment)) then
        Result := Result + '|';
    end;
    Result := Result + ')\s*:=\s*\d{2,5}';
    Exit;
  end;
  for vExpID := 0 to High(fFuncExp) do
  begin
    vLastField := fFuncExp[vExpID].DefMsg;
    if vLastField < fFuncExp[vExpID].Tag then
      vLastField := fFuncExp[vExpID].Tag;
    vTempS := fFuncExp[vExpID].Name + '\s*\(';
    for i := 0 to vLastField do
    begin
      if i = fFuncExp[vExpID].Tag then
      begin
        if aTagType = CP_ZERO then
          vTempS := vTempS + '\s*(0|-1)\s*';
        if aTagType = CP_DIGITS then
          vTempS := vTempS + '[\d\s]+';
      end
      else if i = fFuncExp[vExpID].DefMsg then
//        vTempS := vTempS + '[^,'']*''[^'']+'''
        vTempS := vTempS + '\s*(\/\/.*$|\{[\s\S]*\})*\s*(''[^'']+'')+'
      else
        vTempS := vTempS + '[^,]+';
      if not(i = vLastField) then
        vTempS := vTempS + ',';
    end;
    Result := Result + vTempS;
    if not(vExpID = High(fFuncExp)) then
      Result := Result + '|';
  end;
end;

function TReplaceTags.countOccurrencesOfPattern(const aFile, aPattern: string): Integer;
var
  vContent: TStringList;
  vMatches: TMatchCollection;
  vMemFile: TFilesToProcess;
begin
  vContent := TStringList.Create;
  try
    vContent.LoadFromFile(aFile);
    if (LowerCase(ExtractFileExt(aFile)) = '.dfm') then
      vMatches := TRegEx.Matches(vContent.Text, '\WTag *= *-1\D', [roIgnoreCase, roMultiLine])
    else
      vMatches := TRegEx.Matches(vContent.Text, aPattern, [roIgnoreCase, roMultiLine]);
    Result := vMatches.Count;
    if Result > 0 then
    begin
      vMemFile.Name := aFile;
      SetLength(vMemFile.Tags, Result);
      fFilesToProcess.Add(vMemFile);
    end;
  finally
    vContent.Free;
  end;
end;
{ ------------ public methods TReplaceTags ----------------- }

constructor TReplaceTags.Create;
begin
  inherited Create;
  CommandLinePreSettings.Reset;
  fSettings := TSettings.Create;
  fSettings.Expressions := ExtractFilePath(Application.ExeName) + 'Expressions.ini';
  fIsLangSynchronized := True;
  fIsLanguageFileLoaded := False;
  fGapSize := 0;
  fCountFoundExpInSRC := 0;
  fFilesSources := TObjectList<TFilesSources>.Create();
  fThreadsCS := TCriticalSection.Create;
  fTagsLangFile := TList<Integer>.Create;
  fFilesToProcess := TList<TFilesToProcess>.Create;
  fCSProcessingFiles := TCriticalSection.Create;
  fLangEntrys := TDictionary<Integer, string>.Create;
  fTagsUsed := TList<Integer>.Create;
  fLangEntriesAdded := TCollections.CreateList<TNewTagsForLanguageFile>(
    function (const Left, Right: TNewTagsForLanguageFile): Integer
      begin
        Result := Left.Tag - Right.Tag;
      end);
  fFilesContent := TStringList.Create;
end;


destructor TReplaceTags.Destroy;
begin
  fFilesSources.Free;
  fFilesContent.Free;
  fThreadsCS.Free;
  fTagsUsed.Free;
  fLangEntriesAdded := nil;
  fCSProcessingFiles.Free;
  fFilesToProcess.Free;
  fTagsLangFile.Free;
  fLangEntrys.Free;
  fSettings.Free;
  inherited;
end;

function TReplaceTags.getLangEntry(aTag: Integer): string;
begin
  try
    Result := fLangEntrys.Items[aTag];
  except
    Result := 'keinen Eintrag gefunden.';
  end;
end;

function TReplaceTags.loadExpressionsFile(): Boolean;
var
  vINI:      TSafeIni;
  vTempSL:   TStringList;
  i:         Integer;
  vSplitted: TArray<string>;
  vFileName: string;
  vLangDefDir: string;

  procedure setPathToEXEDirIfNotExistent(var aPath: string);
  begin
    if not DirectoryExists(aPath) then
      aPath := ExtractFileDir(Application.ExeName);
  end;
begin
  if fSettings.IsLoaded then
  begin
    Result := True;
    Exit;
  end;
  vFileName := fSettings.Expressions;
  if not FileExists(vFileName) or not(ExtractFileExt(vFileName) = '.ini') then
    raise Exception.Create('ExpressionsFile not found.');
  SetLength(fFuncExp, 0);
  vINI := TSafeIni.Create(vFileName);
  vTempSL := TStringList.Create;
  try
    with fSettings do
    begin
      if CommandLinePreSettings.sources.IsEmpty then
        Sources := vINI.ReadString('settings', 'SourcesDefDir', EmptyStr)
      else
        Sources := CommandLinePreSettings.getsources;

      setPathToEXEDirIfNotExistent(Sources);
      OverWrite := vINI.ReadBool('settings', 'OverWriteFiles', False);
      OverWriteOrigins := vINI.ReadBool('settings', 'OverWriteOrigins', False);
      if CommandLinePreSettings.langdir.IsEmpty then
        DirectoryTarget := vINI.ReadString('settings', 'TargetDirectory',
              ExtractFilePath(Application.ExeName)+'FilesReplaceTags')
      else
        DirectoryTarget := CommandLinePreSettings.getlangdir;
      setPathToEXEDirIfNotExistent(DirectoryTarget);
      PASSearch := vINI.ReadBool('settings', 'SearchPASFiles', True);
      DFMSearch := vINI.ReadBool('settings', 'SearchDFMFiles', True);
      PreferLastTag := vINI.ReadBool('settings', 'PreferLastTagAsSuggestion', True);
    end;

    vINI.ReadSection('functions', vTempSL);
    SetLength(fFuncExp, vTempSL.Count);
    for i := 0 to vTempSL.Count - 1 do
      fFuncExp[i].Name := vTempSL[i];
    vINI.ReadSectionValues('functions', vTempSL);
    for i := 0 to vTempSL.Count - 1 do
    begin
      vSplitted := vTempSL[i].Split(['=', ',']);
      fFuncExp[i].Tag := StrToInt(vSplitted[1]);
      fFuncExp[i].DefMsg := StrToInt(vSplitted[2]);
    end;
    vINI.ReadSectionValues('returnValues', vTempSL);
    SetLength(fFuncValues, vTempSL.Count);
    for i := 0 to vTempSL.Count - 1 do
      fFuncValues[i] := vTempSL[i];
    vINI.ReadSectionValues('directAssignment', vTempSL);
    SetLength(fFuncDirectAssignment, vTempSL.Count);
    for i := 0 to vTempSL.Count - 1 do
      fFuncDirectAssignment[i] := vTempSL[i];
    fSettings.LangSearch[0] := vINI.ReadString('languageFileNames', 'German', '');
    fSettings.LangSearch[1] := vINI.ReadString('languageFileNames', 'English', '');
    if CommandLinePreSettings.langdir.IsEmpty then
      vLangDefDir := vINI.ReadString('settings', 'LanguageDefDir', EmptyStr)
    else
      vLangDefDir := CommandLinePreSettings.getlangdir;
    setPathToEXEDirIfNotExistent(vLangDefDir);
    fSettings.LanguagePath := vLangDefDir;
    fSettings.IsLoaded := True;
  finally
    vTempSL.Free;
    vINI.Free;
  end;
  fSettings.IsLoaded := True;
  Result := True;
end;

function TReplaceTags.loadLanguageFile(): Boolean;
var
  vLangFile: TSafeIni;
  vContent:  TStringList;
  vEntry:    string;
  i:         Integer;
  vLangFileName: string;
begin
  vContent := TStringList.Create;
  with fSettings do
    vLangFileName := LanguageFiles[0];
  vLangFile := TSafeIni.Create(vLangFileName);
  fTagsLangFile.Clear;
  fLangEntrys.Clear;
  try
    vLangFile.ReadSectionValues('Liste', vContent);
    fTagsLangFile.Clear;
    for i := 0 to vContent.Count - 1 do
    begin
      fTagsLangFile.Add(StrToInt(vContent.Names[i]));
      vEntry := vContent.ValueFromIndex[i];
      try
        fLangEntrys.Add(fTagsLangFile.Last, vEntry);
      except
        on E: EListError do
        raise EReplaceTagsLanguage.Create('Sprachdatei "'+ vLangFileName
            + '" fehlerhaft:'+sLineBreak+'Eintrag: '+vContent[i]);
      end;
    end;
    fMaxTagsInLangFile := vLangFile.ReadInteger('Kopf', 'Eintraege', 0);
    vLangFile.ReadSectionValues('zu pruefen', vContent);
    fLangEntriesAdded.Clear;
    for i := 0 to vContent.Count-1 do
      fLangEntriesAdded.Add( TNewTagsForLanguageFile.Create( StrToInt(vContent.Names[i]), vContent.ValueFromIndex[i]));
  finally
    vContent.Free;
    vLangFile.Free;
  end;
  fTagsLangFile.Sort;
  fIsLanguageFileLoaded := True;
//  fSettings.Language := aFileName;
  Result := True;
end;

procedure TReplaceTags.LoadFilesSourcesFromPath(const aPathSources: string = '');
  function dynArrayToArray(const aDyn: TStringDynArray): TArray<string>;
  var
    j: Integer;
  begin
    SetLength(Result, Length(aDyn));
    for j := 0 to High(aDyn) do
      Result[j] := aDyn[j];
  end;
var
  vFilesSRC: TStringDynArray;
  vSL: TStringList;
  i: Integer;
  vPathSources: string;
begin
  if aPathSources = '' then
    vPathSources := fSettings.Sources
  else
  begin
    fSettings.Sources := aPathSources;
    vPathSources := aPathSources;
  end;
  fTagsUsed.Clear;
  if not DirectoryExists(vPathSources) then
    raise EReplaceTags.Create('Quellcodeverzeichnis ist ungl�cklich');
  vSL := TStringList.Create;
  if fSettings.PASSearch then
  begin
    vFilesSRC := TDirectory.GetFiles(vPathSources, '*.pas', TSearchOption.soAllDirectories);
    vSL.AddStrings(dynArrayToArray(vFilesSRC));
    SetLength(vFilesSRC, 0);
  end;
  if fSettings.DFMSearch then
  begin
    vFilesSRC := TDirectory.GetFiles(vPathSources, '*.dfm', TSearchOption.soAllDirectories);
    vSL.AddStrings(dynArrayToArray(vFilesSRC));
  end;
  vSL.Sort;
  fFilesSources.Clear;
  for i := 0 to vSL.Count-1 do
  begin
    fFilesSources.Add(TFilesSources.Create);
    fFilesSources.Last.Name := vSL[i];
    fFilesSources.Last.IsSelected := True;
  end;
  vSL.Free;
end;

procedure TReplaceTags.SelectFilesFromSources(const aIDs: array of Integer);
var
  i: Integer;
begin
  fTagsUsed.Clear;
  for i := 0 to fFilesSources.Count-1 do
    fFilesSources.Items[i].IsSelected := False;
  for i := 0 to High(aIDs) do
    fFilesSources.Items[aIDs[i]].IsSelected := True;
end;
procedure TReplaceTags.SelectFilesFromSourcesALL;
var
  i: Integer;
begin
  fTagsUsed.Clear;
  for i := 0 to fFilesSources.Count-1 do
    fFilesSources.Items[i].IsSelected := True;
end;

procedure TReplaceTags.RemoveFilesFromSourcesNotSelected;
var
  i: Integer;
begin
  i := 0;
  while i<fFilesSources.Count do
  begin
    if not fFilesSources.Items[i].IsSelected then
      fFilesSources.Delete(i)
    else
      Inc(i);
  end;
end;

procedure TReplaceTags.FilterFilesToProcess;
var
  i, j: Integer;
begin
  for i := 0 to fFilesSources.Count-1 do
  begin
    fFilesSources.Items[i].IsSelected := False;
    for j := 0 to fFilesToProcess.Count -1  do
      if fFilesSources.Items[i].Name.Contains(fFilesToProcess.Items[j].Name) then
        fFilesSources.Items[i].IsSelected := True;
  end;
  RemoveFilesFromSourcesNotSelected;
end;

function TReplaceTags.GetFilesSources: TArray<string>;
var
  i: Integer;
begin
    SetLength(Result, fFilesSources.Count);
    for i := 0 to High(Result) do
      Result[i] := fFilesSources.Items[i].Name;
end;

function TReplaceTags.GetFileNameOfIDSources(const aID: Integer): string;
begin
  Result := fFilesSources.Items[aID].Name;
end;

function TReplaceTags.GetFilesSourcesSelected: TArray<string>;
var
  i: Integer;
begin
    for i := 0 to fFilesSources.Count-1 do
      if fFilesSources.Items[i].IsSelected then
      begin
        SetLength(Result, Length(Result)+1);
        Result[High(Result)] := fFilesSources.Items[i].Name;
      end;
end;

function TReplaceTags.GetFileIDSources(const aName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to fFilesSources.Count do
    if fFilesSources.Items[i].Name.Contains(aName) then
    begin
      Result := i;
      Break;
    end;
end;

function TReplaceTags.ThreadsAreStillWorking(aThreads: array of TWorkingThread): Boolean;
var
  Count: Integer;
begin
  begin
    Result := False;
    for Count := 0 to THREADS_MAX - 1 do
      if Assigned(aThreads[Count]) then
      begin
        Result := True;
        Exit;
      end;
  end;
end;

function TReplaceTags.searchExpressionsInSRC(): Integer;
var
  vPattern:      string;
  i, vRV, vFileIndex: Integer;
  vThreads:      array of TWorkingThread;
  vFiles: TArray<string>;
begin
  fCountFoundExpInSRC := 0;
  if not fSettings.IsLoaded then
    raise Exception.Create('Expressions-File noch nicht geladen');
  if Length(fFuncExp) = 0 then
    raise Exception.Create('Keine Suchausdr�cke in INI-file vorhanden.');
  vFiles := GetFilesSourcesSelected;
  vPattern := createPattern(CP_ZERO);
  SetLength(vThreads, THREADS_MAX);
  for i := 0 to THREADS_MAX - 1 do
    vThreads[i] := nil;
  vFileIndex := 0;
  fFilesToProcess.Clear;
  while (vFileIndex < Length(vFiles)) and (vFileIndex < THREADS_MAX) do
  begin
    vThreads[vFileIndex] := TWorkingThread.Create(countOccurrencesOfPattern, vFiles[vFileIndex],
      vPattern);
    Inc(vFileIndex);
  end;
  while ThreadsAreStillWorking(vThreads) do
  begin
    Sleep(50);
    for i := 0 to THREADS_MAX - 1 do
    begin
      if Assigned(vThreads[i]) and vThreads[i].Terminated then
      begin
        vRV := vThreads[i].WaitFor;
        fCountFoundExpInSRC := fCountFoundExpInSRC + vRV;
        vThreads[i].CheckAndRaiseException;
        vThreads[i].Free;
        vThreads[i] := nil;
        if vFileIndex < Length(vFiles) then
        begin
          vThreads[i] := TWorkingThread.Create(countOccurrencesOfPattern, vFiles[vFileIndex],
            vPattern);
          Inc(vFileIndex);
        end;
        Continue;
      end;
    end;
  end;
  Result := fCountFoundExpInSRC;
end;

function TReplaceTags.SearchPattern0Tags: string;
begin
  Result := createPattern(CP_ZERO);
end;

function TReplaceTags.showGapsOfXTags: TArray<Integer>;
var
  i: Integer;
begin
  SetLength(Result, 1);
  Result[0] := 0;
  if not fIsLanguageFileLoaded then
    Exit;
  if fCountFoundExpInSRC < 1 then
    Exit;
  SetLength(Result, 0);
  if fTagsLangFile.Count < 2 then
  begin
    SetLength(Result, 1);
    Result[0] := fTagsLangFile.Last;
    Exit;
  end;
  for i := 1 to fTagsLangFile.Count - 1 do
  begin
    if (fTagsLangFile.Items[i] - fTagsLangFile.Items[i - 1] > fCountFoundExpInSRC) then
    begin
      SetLength(Result, Length(Result) + 1);
      Result[high(Result)] := fTagsLangFile.Items[i - 1] + 1;
    end;
  end;
  SetLength(Result, Length(Result) + 1);
  Result[high(Result)] := fTagsLangFile.Last + 1;
end;

function TReplaceTags.GapSize(const aTag: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to fTagsLangFile.Count - 1 do
  begin
    if aTag = fTagsLangFile.Items[i] then
    begin
      fGapSize := 0;
      Exit;
    end;
    if aTag < fTagsLangFile.Items[i] then
    begin
      fGapSize := fTagsLangFile.Items[i] - aTag;
      Result := fGapSize;
      Exit;
    end;
  end;
  fGapSize := 999;
  Result := fGapSize;
end;

procedure TReplaceTags.processFiles(const aStartTag: Integer);
var
  i, k: Integer;
  vTag: Integer;
begin
  isPossibleStartProcessingFiles(aStartTag); // raises Exceptions
  vTag := aStartTag;
  for i := 0 to fFilesToProcess.Count - 1 do
    for k := 0 to High(fFilesToProcess.Items[i].Tags) do
    begin
      fFilesToProcess.Items[i].Tags[k].Tag := vTag;
      fFilesToProcess.Items[i].Tags[k].Msg := EmptyStr;
      Inc(vTag);
    end;
  processSRCFilesThread;
  processGermanLanguageFile;
//  processLanguageFiles;
end;

function TReplaceTags.processOneSRCFile2Thread(const Idx, null: string): Integer;
var
  vExt: string;
begin
  vExt := LowerCase(ExtractFileExt(fFilesToProcess.Items[StrToInt(Idx)].Name));
  if vExt = '.pas' then
    processOneSRCFilePAS(StrToInt(Idx));
  if vExt = '.dfm' then
    processOneSRCFileDFM(StrToInt(Idx));
  Result := 0;
end;


procedure TReplaceTags.processSRCFilesThread;
var
  i:         Integer;
  vThreadID: Integer;
  vThreads:  array of TWorkingThread;
begin
  SetLength(vThreads, THREADS_MAX);
  for i := 0 to High(vThreads) do
    vThreads[i] := nil;
  i := 0;
  while (i < fFilesToProcess.Count) and (i < THREADS_MAX) do
  begin
    vThreads[i] := TWorkingThread.Create(processOneSRCFile2Thread, IntToStr(i), '');
    Inc(i);
  end;
  while ThreadsAreStillWorking(vThreads) do
  begin
 //   Sleep(5);
    for vThreadID := 0 to THREADS_MAX - 1 do
    begin
      if Assigned(vThreads[vThreadID]) and vThreads[vThreadID].Terminated then
      begin
        vThreads[vThreadID].CheckAndRaiseException;
        vThreads[vThreadID].WaitFor;
        vThreads[vThreadID].Free;
        vThreads[vThreadID] := nil;
        if i < fFilesToProcess.Count then
        begin
          vThreads[vThreadID] := TWorkingThread.Create(processOneSRCFile2Thread, IntToStr(i), '');
          Inc(i);
        end;
        Continue;
      end;
    end;
  end;
end;

function TReplaceTags.ShowTagsInGroups(const aTags: TArray<Integer>): string;
var
  vBegin, vEnd, vTemp: Integer;
  i:                   Integer;
begin
  Result := EmptyStr;
  if Length(aTags) = 0 then
  begin
    Result := 'keine Tags vorhanden';
    Exit;
  end;
  if Length(aTags) = 1 then
  begin
    Result := IntToStr(aTags[0]);
    Exit;
  end;
  vBegin := aTags[0];
  vTemp := vBegin;
  vEnd := vBegin;
  for i := 1 to High(aTags) do
  begin
    if (aTags[i] - 1) = vTemp then
      vTemp := aTags[i]
    else
      begin
        vEnd := vTemp;
        if not(Result = EmptyStr) then
          Result := Result + sLineBreak;
        if vBegin = vEnd then
          Result := Result + IntToStr(vBegin)
        else
          Result := Result + IntToStr(vBegin) + '-' + IntToStr(vEnd);
        vBegin := aTags[i];
        vTemp := aTags[i];
        vEnd := vBegin;
      end;
  end;

  if not (vEnd = vTemp) then
  begin
    if not(Result = EmptyStr) then
      Result := Result + sLineBreak;
    if vBegin = vTemp then
      Result := Result + IntToStr(vTemp)
    else
      Result := Result + IntToStr(vBegin) + '-' + IntToStr(vTemp);
  end;
  if not TRegEx.IsMatch(Result, '\D'+IntToStr(atags[High(aTags)]), [roMultiLine]) then
    Result := Result + sLineBreak + IntToStr(atags[High(aTags)]);
end;

function TReplaceTags.LookForUsedTagsInFile(const aFN: string): TArray<Integer>;
var
  vPattern:             string;
  vContent:             TStringList;
  vMatch, vMatchBraces: TMatch;
  vLine:                string;
  vTagList:             TList<Integer>;
  vSplit:               TArray<string>;
  i, vExpID:            Integer;
begin
  vPattern := createPattern(CP_DIGITS);
  vContent := TStringList.Create;
  vTagList := TList<Integer>.Create;
  try
    vContent.LoadFromFile(aFN);
    if (LowerCase(ExtractFileExt(aFN)) = '.dfm') then
    begin
      Result := lookForUsedTagsInDFM(aFN);
      for i := 0 to High(Result) do
        if not vTagList.Contains(Result[i]) then
          vTagList.Add(Result[i])
    end
    else
    begin
      vMatch := TRegEx.Match(vContent.Text, vPattern, [roIgnoreCase, roMultiLine]);
      while vMatch.Success do
      begin
        vLine := vMatch.Value;
        for vExpID := 0 to High(fFuncExp) do
          if TRegEx.IsMatch(vLine, fFuncExp[vExpID].Name, [roIgnoreCase, roMultiLine]) then
            Break;
        vMatchBraces := TRegEx.Match(vLine, '\([\s\S]*', [roIgnoreCase, roMultiLine]);
        vLine := vMatchBraces.Value.Substring(1); // Klammer entfernen
        vSplit := vLine.Split([',']);
        vLine := vSplit[fFuncExp[vExpID].Tag].Trim;
        vLine := TRegEx.Replace(vLine, '[\t ]*\/\/.*|[ \t]*{[\s\S]*}', EmptyStr);
        if not vTagList.Contains(StrToIntDef(vLine, -1)) then
          vTagList.Add(StrToIntDef(vLine, -1));
        vMatch := vMatch.NextMatch;
      end;
      if TRegEx.IsMatch(vContent.Text, createPattern(CP_RESULT), [roMultiLine, roIgnoreCase]) then
        vTagList.AddRange(lookForTagsAsResults(vContent));
      vTagList.AddRange(lookForTagsDirectAssignments(vContent));
    end;
    vTagList.Sort;
    Result := vTagList.ToArray;
  finally
    vTagList.Free;
    vContent.Free;
  end;
end;

function TReplaceTags.lookForTagsAsResults(const aContent: TStringList): TArray<Integer>;
var
  i:                     Integer;
  vSearchLevel:             Integer;
  vLine, vSearchExpression: string;
  vMatch, vMatch2:          TMatch;
  vCollectionMatches:       TMatchCollection;
  vTag:                     Integer;
  vPosition:                Integer;
  vTempString:              string;
begin
  vSearchLevel := 0;
  vSearchExpression := '^implementation *$';
  for i := 0 to aContent.Count - 1 do
  begin
    vLine := aContent[i];
    if TRegEx.IsMatch(vLine, vSearchExpression, [roIgnoreCase]) then
    begin
      if TRegEx.IsMatch(vLine, '^ *end *(;|$)', [roIgnoreCase]) then
      begin
        Dec(vSearchLevel);
        if vSearchLevel = 2 then
          Dec(vSearchLevel);
      end
      else
        Inc(vSearchLevel);
      if vSearchLevel = 1 then
        vSearchExpression := createPattern(CP_RESULT);
      if vSearchLevel = 2 then
        vSearchExpression := '(^ *begin *$)|(^ *end *(;|$))';
    end;

    if vSearchLevel >= 3 then
    begin
      vCollectionMatches := TRegEx.Matches(vLine, 'result *:= *\d{2,5} *($|;)', [roIgnoreCase]);
      if vCollectionMatches.Count > 0 then
        for vMatch in vCollectionMatches do
        begin
          vPosition := PosEx(':=', vLine, vMatch.Index);
          begin
            vLine := vLine.Substring(vPosition + 1);
            vMatch2 := TRegEx.Match(vLine, '\d{2,5}');
            vTempString := vMatch2.Value;
            vTag := StrToInt(vTempString);
            SetLength(Result, Length(Result) + 1);
            Result[High(Result)] := vTag;
            vLine := vLine.Substring(Pos(';', vLine));
          end;
        end;
    end;
  end;
end;

function TReplaceTags.lookForTagsDirectAssignments(
  const aContent: TStringList): TArray<Integer>;
var
  vMatches: TMatchCollection;
  vMatch, vMatchInt: TMatch;
begin
  vMatches := TRegEx.Matches(aContent.Text, createPattern(CP_ASSIGNMENT), [roMultiLine, roIgnoreCase]);
  for vMatch in vMatches do
  begin
    vMatchInt := TRegEx.Match(vMatch.Value, '\d+', [roMultiLine, roIgnoreCase]);
    SetLength(Result, Length(Result)+1);
    Result[High(Result)] := StrToInt(vMatchInt.Value);
  end;
end;


function TReplaceTags.LangEntrysForTags(const aTags: TArray<Integer>): TArray<string>;
var
  i: Integer;
begin
  if not fIsLanguageFileLoaded then
    Exit;
  SetLength(Result, Length(aTags));
  for i := 0 to High(aTags) do
  begin
    if aTags[i] = 0 then
    begin
      Result[i] := 'Datei ist verbesserungsbed�rftig!';
      Continue;
    end;
    try
      Result[i] := fLangEntrys.Items[aTags[i]];
    except
      raise Exception.Create('Eintrag in Sprachdatei nicht gefunden (Tag: ' +
        IntToStr(aTags[i]) + ')');
    end;
  end;
end;

procedure TReplaceTags.addTagIfNotExisting(const aToAdd: TArray<Integer>;
  aTagList: TList<Integer>);
var
  i: Integer;
begin
  for i := 0 to High(aToAdd) do
  begin
    if not aTagList.Contains(aToAdd[i]) then
      aTagList.Add(aToAdd[i]);
  end;
end;

function TReplaceTags.lookForUsedTagsInFileThread(const aFile, s2: string): Integer;
var
  vTags: TArray<Integer>;
begin
  vTags := LookForUsedTagsInFile(aFile);
  fThreadsCS.Enter;
   addTagIfNotExisting(vTags, fTagsUsed);
  fThreadsCS.Leave;
  result := 0;
end;

function TReplaceTags.lookForUsedTagsInDFM(const aFile: string): TArray<Integer>;
var
  vContent: TStringList;
  vMatch, vMatchInt: TMatch;
begin
  vContent := TStringList.Create;
  vContent.LoadFromFile(aFile);
  vMatch := TRegEx.Match(vContent.Text, '\WTag\s*=\s*\d{1,5}\D', [roMultiLine, roIgnoreCase]);
  while vMatch.Success do
  begin
    vMatchInt := TRegEx.Match(vMatch.Value, '\d{1,5}', [roIgnoreCase]);
    SetLength(Result, Length(Result)+1);
    Result[High(Result)] := StrToInt(vMatchInt.Value);
    vMatch := vMatch.NextMatch;
  end;
  vContent.Free;
end;

function TReplaceTags.ListOfUsedTagsInFiles(): TArray<Integer>;
var
  vThreads: TArray<TWorkingThread>;
  vFileID: Integer;
  i: Integer;
  vFiles: TArray<string>;
begin
  vFiles := GetFilesSourcesSelected;
  SetLength(vThreads, THREADS_MAX);
  fTagsUsed.Clear;
  vFileID := 0;
  while (vFileID<THREADS_MAX) and (vFileID<Length(vFiles)) do
  begin
    vThreads[vFileID] := TWorkingThread.Create(lookForUsedTagsInFileThread,
        vFiles[vFileID], '');
    Inc(vFileID);
  end;
  while (vFileID<Length(vFiles)) or ThreadsAreStillWorking(vThreads) do
  begin
    for i := 0 to High(vThreads) do
    begin
      if Assigned(vThreads[i]) and vThreads[i].Terminated then
      begin
        vThreads[i].WaitFor;
        vThreads[i].CheckAndRaiseException;
        vThreads[i].Free;
        vThreads[i] := nil;
        if vFileID<Length(vFiles) then
        begin
          vThreads[i] := TWorkingThread.Create(lookForUsedTagsInFileThread,
              vFiles[vFileID], '');
          Inc(vFileID);
          Continue;
        end;
      end;
    end;
    Sleep(50);
  end;
  fTagsUsed.Sort;
  Result := fTagsUsed.ToArray;
end;


function TReplaceTags.NumberInSRCsNotExistent(const aTags: TArray<Integer>): TArray<Integer>;
var
  vFiles: TStringDynArray;
  vContent: TStringList;
  i,j : Integer;
  vMatch, vMatchInt: TMatch;
  vMatches: TMatchCollection;
  vNumbers: TList<Integer>;

begin
  vFiles := TDirectory.GetFiles(fPathSRCs, '*.pas', TSearchOption.soAllDirectories);
  if Length(vFiles) = 0  then
    raise Exception.Create('TReplaceTags.IsNumberInSRCsExistentPrepare: '+ sLineBreak
              + 'no .pas-files found');
  vContent := TStringList.Create;
  vNumbers := TList<Integer>.Create;
  for i := 0 to High(vFiles) do
  begin
    vContent.LoadFromFile(vFiles[i]);
    vMatches := TRegEx.Matches(vContent.Text, '[=,(]\s*\d{1,5} *[,;)\s]', [roMultiLine]);

    for vMatch in vMatches do
    begin
      vMatchInt := TRegEx.Match(vMatch.Value, '\d+', [roMultiLine]);
      j := StrToInt(vMatchInt.Value);
      if not vNumbers.Contains(j) then
        vNumbers.Add(j);
//      vMatch.NextMatch;
    end;
  end;
  vContent.Free;

  for i := 0 to High(aTags) do
    if not vNumbers.Contains(aTags[i]) then
    begin
      SetLength(Result, length(Result)+1);
      Result[high(Result)] := aTags[i];
    end;

  vNumbers.Free;
end;

function TReplaceTags.OccurrencesOfTagsInSRCs(
  const aTag: Integer; out aFiles: TArray<string>): TArray<string>;
var
  vFiles: TStringDynArray;
  vContent, vSLLines: TStringList;
  i: Integer;
  vMatch: TMatch;
  vMatches: TMatchCollection;
begin
  vFiles := getSources(FILES_PAS);
  vSLLines := TStringList.Create;
  vContent := TStringList.Create;
  SetLength(aFiles, 0);
  for i := 0 to High(vFiles) do
  begin
    vContent.LoadFromFile(vFiles[i]);
    vMatches := TRegEx.Matches(vContent.Text, '^.*[,(=] *' + IntToStr(aTag) + ' *[,();].*$',
              [roMultiLine, roIgnoreCase]);
    for vMatch in vMatches do
    begin
      vSLLines.Add(vMatch.Value);
      SetLength(aFiles, Length(aFiles)+1);
      aFiles[High(aFiles)] := vFiles[i];
    end;
  end;
  Result := vSLLines.ToStringArray;
  vSLLines.Free;
  vContent.Free;
end;

function TReplaceTags.GetListOfFilesToProcess: TArray<string>;
var
  i: Integer;
begin
  SetLength(Result, fFilesToProcess.Count);
  for i := 0 to fFilesToProcess.Count-1 do
    Result[i] := fFilesToProcess.Items[i].Name;
end;

procedure TReplaceTags.LogFilesToProcess(aLogFile: string);
var
  vLogfile: string;
  vFile: TextFile;
  i: Integer;
begin
  vLogfile := ExpandFileName(aLogFile);
  Assign(vFile, vLogfile);
  Rewrite(vFile);
  Writeln(vFile, DateToStr(Now));
  for i := 0 to fFilesToProcess.Count-1 do
  begin
    Writeln(vFile, fFilesToProcess.Items[i].Name + ';'
        + IntToStr(Length(fFilesToProcess.Items[i].Tags)));
  end;
  CloseFile(vFile);
end;

procedure TReplaceTags.DeleteTagsFromLangFile(const aTags: TArray<Integer>);
var
  vNewfile: string;
  vINI: TSafeIni;
  vTagsDelete: TList<Integer>;
  i: Integer;
  vActualLangFile: string;
begin
  vActualLangFile := fSettings.LanguageFiles[0];
  vNewfile := fSettings.DirectoryTarget + '\' + ExtractFileName(fSettings.LanguageFiles[0]);
  CopyFile(PWideChar(vActualLangFile), PWideChar(vNewFile), not fSettings.OverWrite);
  if FileExists(vNewfile) and not fSettings.OverWrite then
    raise Exception.Create('Sprachdatei existiert schon im Zielverzeichnis und/oder die Option �berschreiben ist nicht aktiviert.');
  vINI := TSafeIni.Create(vNewFile);
  vTagsDelete := TList<Integer>.Create;
  try
    vTagsDelete.AddRange(aTags);
    vINI.EraseSection('Liste');
    vINI.UpdateFile;
    i := 0;
    while i < fTagsLangFile.Count do
    begin
      if not vTagsDelete.Contains(fTagsLangFile.Items[i]) then
      begin
        vINI.WriteString('Liste', IntToStr(fTagsLangFile.Items[i]),
                fLangEntrys[fTagsLangFile.Items[i]]);
        Inc(i);
      end
      else
      begin
        vINI.DeleteKey('zu pruefen', IntToStr( fTagsLangFile.Items[i]) );
        fTagsLangFile.Delete(i);
      end;
    end;
    vINI.WriteString('Kopf', 'Dateiname', vNewFile);
    vINI.WriteString('Kopf', 'Stand', FormatDateTime('dd.mm.yyyy', Now));
    vINI.WriteInteger('Kopf', 'Eintraege', fTagsLangFile.Last);
    vINI.UpdateFile;
  finally
    vINI.Free;
    vTagsDelete.Free;
  end;
  fIsLangSynchronized := False;
end;
{ ##################TWorkingThread################ }
procedure TWorkingThread.CheckAndRaiseException;
begin
  if Assigned(fException) then
  begin
    if fException is Exception then
      (fException as Exception).RaiseOuterException(EWorkingThread.Create('EWorkingThread - Arguments: s1: ' + fs1 + '; s2: ' + fs2))
    else
      raise fException;
  end;
end;

constructor TWorkingThread.Create(const aWorkToDo: TWorkingWork; s1, s2: string);
begin
  inherited Create(True);
  fWorkToDo := aWorkToDo;

//  fMyProc := aWorkToDo;
  fs1 := s1;
  fs2 := s2;
  //  Self.OnTerminate := aOnTerminate;
  Self.FreeOnTerminate := False;
  Self.Resume;
end;

procedure TWorkingThread.Execute;
begin
  ReturnValue := -1;
  try
    try
      ReturnValue := fWorkToDo(fs1, fs2);
    except
      fException := AcquireExceptionObject;
    end;
  finally
    Terminate;
  end;
end;

{ TSettings }

procedure TSettings.setLanguageFiles(const aPath: string);
var
  vLangFiles: TStringDynArray;
  i: Integer;
begin
  fLanguagePath := aPath;
  SetLength(fLanguageFiles, 2);
  vLangFiles := TDirectory.GetFiles(fLanguagePath, '*.vbl', TSearchOption.soTopDirectoryOnly);
  for i := 0 to High(vLangFiles) do
  begin
    if TRegEx.IsMatch(vlangfiles[i], LangSearch[0] + '$', [roMultiLine]) then
    begin
      fLanguageFiles[0] := vLangFiles[i];
      Continue;
    end;
    if TRegEx.IsMatch(vlangfiles[i], LangSearch[1] + '$', [roMultiLine]) then
    begin
      fLanguageFiles[1] := vLangFiles[i];
      Continue;
    end;
    SetLength(fLanguageFiles, Length(fLanguageFiles)+1);
    fLanguageFiles[High(fLanguageFiles)] := vLangFiles[i];
  end;
end;

{ TReplaceTags.TNewTagsForLanguageFile }

constructor TReplaceTags.TNewTagsForLanguageFile.Create(const aTag: Integer;
  aMsg: string);
begin
  Tag := aTag;
  Msg := aMsg;
end;

function TCommandLinePreSettings.getlangdir: string;
begin
  Result := TPath.Combine(getsources, langdir);
end;

function TCommandLinePreSettings.getsources: string;
var
  i: Integer;
begin
  Result := sources;
  for i := 0 to parentdir-1 do
    Result := TPath.GetDirectoryName(Result);
end;

{ TCommandLinePreSettings }

procedure TCommandLinePreSettings.Reset;
begin
  sources := '';
  parentdir := 0;
  langdir := '';
end;

end.
