unit unusedTags;

interface

uses
  replaceTags,

  System.Classes,
  System.Generics.Collections,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TFormUnusedTags = class(TForm)
    lstTagsLangFile: TListBox;
    lLanguageFile: TLabel;
    lSources: TLabel;
    lstTagsSources: TListBox;
    lDiffLangSRC: TLabel;
    lstTagsDiffLangSRC: TListBox;
    lCountTagsLangFile: TLabel;
    lCountTagsSources: TLabel;
    lCountTagsDiffLangSRC: TLabel;
    bLookForUsedTags: TButton;
    bSearchTagsInSRCs: TButton;
    lstTagsNotFoundInSRCs: TListBox;
    lTagsNotFoundInSRCs: TLabel;
    lCountTagsNotFoundInSRCs: TLabel;
    lstFoundTagsText: TListBox;
    bSearchSelectedInSRC: TButton;
    lTagSearchedInSRCs: TLabel;
    eFileSearchedTagInSRCs: TEdit;
    bMoveTagRight: TButton;
    bMoveTagLeft: TButton;
    bDeleteTagsFromLangFile: TButton;
    chkOverWriteFiles: TCheckBox;
    eTargetDirectory: TEdit;
    bMoveAllBack: TButton;
    statBottom: TStatusBar;
    chkShowToolTips: TCheckBox;

    procedure FormShow(Sender: TObject);
    procedure bLookForUsedTagsClick(Sender: TObject);
    procedure bSearchTagsInSRCsClick(Sender: TObject);
    procedure bSearchSelectedInSRCClick(Sender: TObject);
    procedure lstFoundTagsTextClick(Sender: TObject);
    procedure bMoveTagRightClick(Sender: TObject);
    procedure lstTagsDiffLangSRCKeyPress(Sender: TObject; var Key: Char);
    procedure lstTagsNotFoundInSRCsKeyPress(Sender: TObject; var Key: Char);
    procedure bMoveTagLeftClick(Sender: TObject);
    procedure bDeleteTagsFromLangFileClick(Sender: TObject);
    procedure bMoveAllBackClick(Sender: TObject);
    procedure chkShowToolTipsClick(Sender: TObject);
  private
    { Private-Deklarationen }
    fReplaceTags: TReplaceTags;
    fFilesSRCs: TArray<string>;
    fTagListLangFile: TList<Integer>;
    fTagListSRCs: TList<Integer>;
    fTagListDiff: TList<Integer>;
    fTagListNotFound: TList<Integer>;
    fFilesOfTagSearchedInSRCs: TArray<string>;

    procedure listTagsInListBox(const aBox: TListBox; aTags: TArray<Integer>);
    procedure diffTagsLangSRCs;
    procedure enableButtons;

  public
    { Public-Deklarationen }
    constructor Create(const aOwner: TComponent; aReplaceTags: TReplaceTags; aFiles: TArray<string>); reintroduce;
    destructor Destroy; override;
  end;

var
  FormUnusedTags: TFormUnusedTags;

implementation

{$R *.dfm}

{ TForm2 }


constructor TFormUnusedTags.Create(const aOwner: TComponent; aReplaceTags: TReplaceTags; aFiles: TArray<string>);
begin
  inherited Create(aOwner);
  fReplaceTags := aReplaceTags;
  fFilesSRCs := aFiles;
  fTagListLangFile := TList<Integer>.Create;
  fTagListSRCs := TList<Integer>.Create;
  fTagListDiff := tlist<Integer>.Create;
  fTagListNotFound := TList<Integer>.Create;
  chkShowToolTips.Checked := Self.ShowHint;
end;
destructor TFormUnusedTags.Destroy;
begin
  fTagListNotFound.Free;
  fTagListDiff.Free;
  fTagListLangFile.Free;
  fTagListSRCs.Free;
  inherited;
end;


procedure TFormUnusedTags.listTagsInListBox(const aBox: TListBox;
  aTags: TArray<Integer>);
var
  i: Integer;
  vLabelCount: TLabel;
  vNameBox: string;
begin
  aBox.Clear;
  for i := 0 to High(aTags) do
    aBox.Items.Add(IntToStr(aTags[i]));
  vNameBox := aBox.Name;
  vNameBox := vNameBox.Substring(3);
  vLabelCount := TLabel(FindComponent('lCount' + vNameBox));
  if not Assigned(vLabelCount) then
    raise Exception.Create('Label nicht gefunden');
  vLabelCount.Caption := 'Anzahl: ' + IntToStr(Length(aTags));
end;

procedure TFormUnusedTags.lstFoundTagsTextClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lstFoundTagsText.Count-1 do
  begin
    if lstFoundTagsText.Selected[i] then
      Break;
  end;
  eFileSearchedTagInSRCs.Text := fFilesOfTagSearchedInSRCs[i];
end;


procedure TFormUnusedTags.lstTagsDiffLangSRCKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #109 then // "m" dr�cken
    bMoveTagRightClick(nil);
  if Key = #13 then  // enter
    bSearchSelectedInSRCClick(nil);
end;

procedure TFormUnusedTags.lstTagsNotFoundInSRCsKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #109 then // "m" dr�cken
    bMoveTagLeftClick(nil);
end;

procedure TFormUnusedTags.diffTagsLangSRCs;
var
  i: Integer;
begin
  fTagListDiff.Clear;
  for i := 0 to fTagListLangFile.Count-1 do
  begin
    if not fTagListSRCs.Contains(fTagListLangFile.Items[i]) then
      fTagListDiff.Add(fTagListLangFile.Items[i]);
  end;
end;


procedure TFormUnusedTags.enableButtons;
begin
  if fTagListNotFound.Count = 0 then
  begin
    bDeleteTagsFromLangFile.Enabled := False;
  end
  else
  begin
    bDeleteTagsFromLangFile.Enabled := True;
  end;
  if fTagListSRCs.Count = 0 then
  begin
    bSearchTagsInSRCs.Enabled := False;
    bSearchSelectedInSRC.Enabled := False;
    bMoveTagRight.Enabled := False;
    bMoveTagLeft.Enabled := False;
    bMoveAllBack.Enabled := False;
  end
  else
  begin
    bSearchTagsInSRCs.Enabled := True;
    bSearchSelectedInSRC.Enabled := True;
    bMoveTagLeft.Enabled := True;
    bMoveTagRight.Enabled := True;
    bMoveAllBack.Enabled := True;
  end;
end;

procedure TFormUnusedTags.FormShow(Sender: TObject);
begin
  fReplaceTags.SelectFilesFromSourcesALL;
  chkOverWriteFiles.Checked := fReplaceTags.Settings.OverWrite;
  fTagListLangFile.AddRange(fReplaceTags.TagsLanguageFile);
  listTagsInListBox(lstTagsLangFile, fTagListLangFile.ToArray);
  lstTagsSources.Clear;
  lstTagsSources.Items.Add('bitte unten clicken...');
  lstTagsDiffLangSRC.Items.Add('warte...');
  eTargetDirectory.Text := fReplaceTags.Settings.DirectoryTarget;
end;


procedure TFormUnusedTags.bDeleteTagsFromLangFileClick(Sender: TObject);
begin
  if fTagListNotFound.Count = 0 then
    Exit;
  fReplaceTags.Settings.DirectoryTarget := eTargetDirectory.Text;
  fReplaceTags.Settings.OverWrite := chkOverWriteFiles.Checked;
  fReplaceTags.DeleteTagsFromLangFile(fTagListNotFound.ToArray);
  fReplaceTags.ProcessLanguageFiles;
  fReplaceTags.loadLanguageFile;
  Self.Close;
end;

procedure TFormUnusedTags.bLookForUsedTagsClick(Sender: TObject);
begin
  fTagListSRCs.Clear;
  fTagListNotFound.Clear;
  listTagsInListBox(lstTagsNotFoundInSRCs, fTagListNotFound.ToArray);
  fReplaceTags.SelectFilesFromSourcesALL;
  fTagListSRCs.AddRange(fReplaceTags.ListOfUsedTagsInFiles());
  fTagListSRCs.Sort;
  listTagsInListBox(lstTagsSources, fTagListSRCs.ToArray);

  diffTagsLangSRCs;
  listTagsInListBox(lstTagsDiffLangSRC, fTagListDiff.ToArray);
  enableButtons;
  if bSearchTagsInSRCs.Enabled then
    bSearchTagsInSRCs.SetFocus;
end;

procedure TFormUnusedTags.bMoveAllBackClick(Sender: TObject);
begin
  fTagListDiff.AddRange(fTagListNotFound.ToArray);
  fTagListDiff.Sort;
  fTagListNotFound.Clear;
  listTagsInListBox(lstTagsDiffLangSRC, fTagListDiff.ToArray);
  listTagsInListBox(lstTagsNotFoundInSRCs, fTagListNotFound.ToArray);
  enableButtons;
end;

procedure TFormUnusedTags.bMoveTagLeftClick(Sender: TObject);
var
  i: Integer;
  vTag: Integer;
begin
  for i := 0 to lstTagsNotFoundInSRCs.Count-1 do
    if lstTagsNotFoundInSRCs.Selected[i] then
    begin
      vTag := StrToInt(lstTagsNotFoundInSRCs.Items[i]);
      fTagListNotFound.Delete(fTagListNotFound.IndexOf(vTag));
      lstTagsNotFoundInSRCs.DeleteSelected;
      fTagListDiff.Add(vTag);
      fTagListDiff.Sort;
      listTagsInListBox(lstTagsDiffLangSRC, fTagListDiff.ToArray);
      Break;
    end;
end;

procedure TFormUnusedTags.bMoveTagRightClick(Sender: TObject);
var
  i: Integer;
  vTag: Integer;
begin
  for i := 0 to lstTagsDiffLangSRC.Count-1 do
    if lstTagsDiffLangSRC.Selected[i] then
    begin
      vTag := StrToInt(lstTagsDiffLangSRC.Items[i]);
      fTagListDiff.Delete(fTagListDiff.IndexOf(vTag));
      fTagListDiff.Sort;
      lstTagsDiffLangSRC.DeleteSelected;
//      listTagsInListBox(lstTagsDiffLangSRC, fTagListDiff.ToArray);
      fTagListNotFound.Add(vTag);
      fTagListNotFound.Sort;
      listTagsInListBox(lstTagsNotFoundInSRCs, fTagListNotFound.ToArray);
      Break;
    end;
end;

procedure TFormUnusedTags.bSearchSelectedInSRCClick(Sender: TObject);
var
  i, vTag: Integer;
  vOccurrences: TArray<string>;
begin
  for i := 0 to lstTagsDiffLangSRC.Count-1 do
    if lstTagsDiffLangSRC.Selected[i] then
      Break;
  if i > lstTagsDiffLangSRC.Count-1 then
    Exit;
  SetLength(fFilesOfTagSearchedInSRCs,0);
  vTag := StrToInt(lstTagsDiffLangSRC.Items[i]);
  lTagSearchedInSRCs.Caption := 'gesuchter Tag: ' + IntToStr(vTag)
        + '  --> ' + fReplaceTags.GetLangEntry(vTag);
  vOccurrences := fReplaceTags.OccurrencesOfTagsInSRCs(vTag, fFilesOfTagSearchedInSRCs);
  lstFoundTagsText.Clear;
  for i := 0 to High(vOccurrences) do
    lstFoundTagsText.Items.Add(vOccurrences[i]);
end;

procedure TFormUnusedTags.bSearchTagsInSRCsClick(Sender: TObject);
var
  i: Integer;
begin
  fTagListNotFound.AddRange(fReplaceTags.NumberInSRCsNotExistent(
        fTagListDiff.ToArray));
  listTagsInListBox(lstTagsNotFoundInSRCs, fTagListNotFound.ToArray);
  for i := 0 to fTagListNotFound.Count-1 do
  begin
    if fTagListDiff.Contains(fTagListNotFound.Items[i]) then
      fTagListDiff.Delete(fTagListDiff.IndexOf(fTagListNotFound.Items[i]));
  end;
  listTagsInListBox(lstTagsDiffLangSRC, fTagListDiff.ToArray);
  enableButtons;
end;

procedure TFormUnusedTags.chkShowToolTipsClick(Sender: TObject);
begin
  Self.ShowHint := chkShowToolTips.Checked;
end;

end.
