object FormInfo: TFormInfo
  Left = 0
  Top = 0
  Caption = 'Information'
  ClientHeight = 222
  ClientWidth = 558
  Color = clBtnFace
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 181
    Width = 558
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btnClose: TButton
      Left = 84
      Top = 6
      Width = 97
      Height = 25
      Caption = 'OK'
      TabOrder = 0
      OnClick = btnCloseClick
    end
  end
  object pnlDevelop: TPanel
    Left = 0
    Top = 0
    Width = 280
    Height = 180
    BevelOuter = bvNone
    TabOrder = 1
    object lblGitLastUpdate: TLabel
      Left = 26
      Top = 18
      Width = 73
      Height = 13
      Caption = 'Letztes Update'
    end
    object lblGitLastUpdateValue: TLabel
      Left = 128
      Top = 18
      Width = 94
      Height = 13
      Caption = 'GitLastUpdateValue'
    end
    object lblGitLastTag: TLabel
      Left = 26
      Top = 40
      Width = 62
      Height = 13
      Caption = 'Basis Version'
    end
    object lblGitLastTagValue: TLabel
      Left = 128
      Top = 40
      Width = 77
      Height = 13
      Caption = 'GitLastTagValue'
    end
    object lblVersionTypeDev: TLabel
      Left = 26
      Top = 62
      Width = 56
      Height = 13
      Caption = 'Versionstyp'
    end
    object lblBitDevValue: TLabel
      Left = 128
      Top = 62
      Width = 27
      Height = 13
      Caption = '32 Bit'
    end
    object lblVersionTypeDevValue: TLabel
      Left = 159
      Top = 62
      Width = 59
      Height = 13
      Caption = 'VersionType'
    end
    object lblGitLastHash: TLabel
      Left = 26
      Top = 84
      Width = 24
      Height = 13
      Caption = 'Hash'
    end
    object lblGitLastHashValue: TLabel
      Left = 128
      Top = 84
      Width = 24
      Height = 13
      Caption = 'Hash'
    end
    object lblGitCurrentBranch: TLabel
      Left = 26
      Top = 106
      Width = 33
      Height = 13
      Caption = 'Branch'
    end
    object lblGitCurrentBranchValue: TLabel
      Left = 128
      Top = 106
      Width = 109
      Height = 13
      Caption = 'GitCurrentBranchValue'
    end
    object lblGitAuthorName: TLabel
      Left = 26
      Top = 128
      Width = 27
      Height = 13
      Caption = 'Autor'
    end
    object lblGitAuthorNameValue: TLabel
      Left = 128
      Top = 128
      Width = 99
      Height = 13
      Caption = 'GitAuthorNameValue'
    end
    object lblGitCommitDate: TLabel
      Left = 26
      Top = 150
      Width = 69
      Height = 13
      Caption = 'Commit Datum'
    end
    object lblGitCommitDateValue: TLabel
      Left = 128
      Top = 150
      Width = 97
      Height = 13
      Caption = 'GitCommitDateValue'
    end
    object btnCopy: TButton
      Left = 187
      Top = 82
      Width = 50
      Height = 20
      Caption = 'Copy'
      TabOrder = 0
      OnClick = btnCopyClick
    end
  end
  object pnlRelease: TPanel
    Left = 279
    Top = 0
    Width = 280
    Height = 180
    BevelOuter = bvNone
    TabOrder = 2
    object lblFileVersionNr: TLabel
      Left = 18
      Top = 18
      Width = 35
      Height = 13
      Caption = 'Version'
    end
    object lblFileVersionNrValue: TLabel
      Left = 120
      Top = 18
      Width = 62
      Height = 13
      Caption = 'FileVersionNr'
    end
    object lblBuildNumber: TLabel
      Left = 18
      Top = 40
      Width = 54
      Height = 13
      Caption = 'Bildnummer'
    end
    object lblBuildNumberValue: TLabel
      Left = 120
      Top = 40
      Width = 59
      Height = 13
      Caption = 'BuildNumber'
    end
    object lblVersionType: TLabel
      Left = 18
      Top = 62
      Width = 56
      Height = 13
      Caption = 'Versionstyp'
    end
    object lblVersionTypeValue: TLabel
      Left = 151
      Top = 62
      Width = 59
      Height = 13
      Caption = 'VersionType'
    end
    object lblBitValue: TLabel
      Left = 120
      Top = 62
      Width = 27
      Height = 13
      Caption = '32 Bit'
    end
    object lblCreationTime: TLabel
      Left = 18
      Top = 84
      Width = 59
      Height = 13
      Caption = 'Erstelldatum'
    end
    object lblCreationTimeValue: TLabel
      Left = 120
      Top = 84
      Width = 63
      Height = 13
      Caption = 'CreationTime'
    end
    object lblCompanyName: TLabel
      Left = 18
      Top = 106
      Width = 58
      Height = 13
      Caption = 'Firmenname'
    end
    object lblCompanyNameValue: TLabel
      Left = 120
      Top = 106
      Width = 98
      Height = 13
      Caption = 'CompanyNameValue'
    end
    object lblFileDescription: TLabel
      Left = 18
      Top = 128
      Width = 89
      Height = 13
      Caption = 'Dateibeschreibung'
    end
    object lblFileDescriptionValue: TLabel
      Left = 120
      Top = 128
      Width = 95
      Height = 13
      Caption = 'FileDescriptionValue'
    end
  end
end
