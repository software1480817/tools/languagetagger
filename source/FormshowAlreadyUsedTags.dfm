object FormalreadyUsed: TFormalreadyUsed
  Left = 0
  Top = 0
  Caption = 'already used Tags'
  ClientHeight = 453
  ClientWidth = 554
  Color = clBtnFace
  Constraints.MinHeight = 190
  Constraints.MinWidth = 570
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  DesignSize = (
    554
    453)
  PixelsPerInch = 96
  TextHeight = 13
  object lFileName: TLabel
    Left = 8
    Top = 8
    Width = 69
    Height = 19
    Caption = 'lFileName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lPath: TLabel
    Left = 8
    Top = 33
    Width = 24
    Height = 13
    Caption = 'lPath'
  end
  object sgUsedTags: TStringGrid
    Left = 8
    Top = 52
    Width = 538
    Height = 393
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 2
    FixedCols = 0
    RowCount = 2
    ScrollBars = ssVertical
    TabOrder = 0
    ColWidths = (
      64
      451)
  end
end
