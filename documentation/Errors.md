# Fehlerfälle

- Nachfolgende Fälle sind bekannt, die eine Übersetzung mit dem _LanguageTagger_ verhindern

## Delphi Komponenten

### TPanel

- Parameter:
  -  _ParentShowHint_ muss _true_ sein

- Fehler:
  
![Tags ersetzen](pictures/panel1.png)

- Korrekt:

![Tags ersetzen](pictures/panel2.png)