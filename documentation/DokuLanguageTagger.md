# Dokumentation Sprachtool „LanguageTagger"

##  Begriffe / Definitionen / Abkürzungen

-   Sprachfunktionen: in der Software werden verschiedene Funktionen
    verwendet, um im laufenden Betrieb auf die Sprachdateien zuzugreifen.
    Die Basisfunktion ist\
    **Sprache.ItemDef()** und ist in der Unit **OurPlant.Migration.Common.GlobalCellAccess** implementiert

##  Ziele

Aufgrund der gewachsenen Struktur der Software wurden die
Sprachfunktionen, welche die Mehrsprachigkeit gewährleisten sollen nicht
immer sorgfältig verwendet -- Dieses Tool soll helfen mehr Ordnung in
das Thema Sprache zu bringen.

Weiterhin soll es eine Programmierhilfe darstellen, welche den
Programmierern die Suche nach freien Tags in der Sprachdatei abnehmen
soll.

Nachfolgend die einzelnen Unterziele:

###  Ungenutzte Tags aus der Sprachdatei entfernen

Die Vermutung bestand, dass die Sprachdateien Tags enthalten, welche
nicht mehr in der Software verwendet werden.

###  Sprachdateien untereinander konsistent halten / synchronisieren

Die Sprachdateien sollen untereinander konsistent gehalten werden, d.h.:
neue Tags sollen auch in den jeweils anderen Sprachen angelegt werden,
überflüssige auch entfernt werden. Somit können später Lücken in den
Sprachdateien gefüllt werden und die Zugehörigkeit von Tag und
Sinngehalt entspricht in jeder Sprache bleibt erhalten.

### Sprachfunktionen mit 0 bzw. -1 Tags im Quellcode ersetzen

Feature als Programmierhilfe.\
beim Programmieren sollen die Tags (als extra Commit) in dem Branch _develop_ von
dem Tool ersetzt werden -- als Schritt im Entwicklungsprozess. D.h. für
den Entwickler entfällt der Arbeitsschritt der Anpassung in der Sprachdatei.

### Nicht übersetzte Objekte der Benutzeroberfläche ausfindig machen und taggen

Einige Objekte der Benutzeroberflächen wurden nicht übersetzt, diese
sollen gefunden werden und automatisch getaggt und in die Sprachdateien
aufgenommen werden.

### Editierhilfe für Sprachdateien

Die Sprachdateien werden bis dato per Hand übersetzt. Oftmals sind die
Übersetzungen allerdings von der Situation abhängig sodass es hilfreich
wäre den jeweiligen Kontext des Spracheintrages zu kennen.

### Einbindung in den Releaselauf -- Überprüfung von neu eingechecktem Quellcode

Vor dem Bauen einer neuen Version (Testversion oder Release) soll der
Quellcode auf nicht getaggte Sprachfunktionen überprüft werden.

## Umsetzung

###  Ungenutzte Tags

Über den Hauptmenüeintrag „Ungenutzte Tags" wird ein neues Fenster
geöffnet.

In diesem Unterprogramm werden die Quellcodedateien (pas und dfm)
auf Tags untersucht und mit den Tags der deutschen Sprachdatei
abgeglichen, um am Ende nicht verwendete ausfindig zu machen. Eine
Erklärung der einzelnen Listen:

-   Sprachdatei: Liste der Tags die sich in der deutschen Sprachdatei
    befinden

-   Quellen: Liste der Tags die in den Quellen gefunden wurden. Die zu
    suchenden Ausdrücke müssen in der Expressions.ini definiert sein.

-   Diff Sprache-Quellen: Tags die nicht in den Quellen gefunden wurden
    sich aber noch in der Sprachdatei befinden.

In der Spalte diff Sprache-Quellen kann durch Auswahl eines Tags und
Klick auf „suche Ausgewählten Tag in Quellen" das Vorkommen als einfache
Zahl in den Quelldateien angezeigt werden. Die einzige Bedingung ist: es
darf vor und nach der gesuchten Zahl (dem vermeintlichen Tag) keine
weitere Zahl stehen. Oftmals wird der entsprechende Tag gar nicht
gefunden. Um diese Tags auszusortieren hilft ein klick auf „suche
Vorkommen aller Tags in Quellen"

-   Nicht gefundene Tags: ungenutzte Tags die nirgends in den Quellen
    auftauchen.

Durch einen Klick auf „lösche Tags aus Sprachdatei" wird eine neue
Sprachdatei im Zielordner angelegt bei der die in der letzten Spalte
angezeigten Tags nicht mehr enthalten sind.

Die Tags die sich jetzt noch in der Spalte „diff Sprache-Quellen"
befinden konnten nicht automatisch Sprachfunktionen zugeordnet werden.
Dennoch kommen diese Zahlen irgendwo im Quellcode vor. Durch Auswahl
eines Tags und einen Doppelklick, drücken der EnterTaste oder klick auf
„suche Ausgewählten Tag in Quellen" wird in dem Feld ganz Rechts die
jeweilige Quellcodezeile angezeigt. Durch Klick auf eine Zeile wird
unter dem Feld die Quellcodedatei angezeigt.

Durch einen Klick auf „verschiebe Tag" können Tags in die Liste der
nicht gefundenen Tags aufgenommen werden und auch wieder zurück
verschoben werden. Die gleiche Funktionalität bietet nach Auswahl des
entsprechenden Tags die Taste „m".

Beim Speichern werden die restlichen Sprachdateien automatisch
synchronisiert. Um die Originaldateien zu überschreiben muss als
Zielverzeichnis dem Quellverzeichnis der Sprachdateien übereinstimmen
und die Option „Sprachdatei im Zielordner überschreiben" aktiviert sein.

###  Sprachdateien synchronisieren

Ausgangspunkt für alle Operationen ist die deutsche Sprachdatei -- im
Augenblick „Deutsch.VBL". Sämtliche Operationen werden primär auf
Grundlage dieser Datei ausgeführt. Damit keine Überschneidungen bzw.
falschen Zuweisungen auftreten müssen die Sprachdateien untereinander
synchronisiert werden.

Generell werden neue Einträge mit einem Marker versehen, zur Zeit ist
dieser #!

Alle neuen Einträge müssen per Hand kontrolliert, bearbeitet und durch
Entfernen des Markers freigegeben werden.

Freigegebene Deutsche Einträge werden bei der Synchronisierung in die
englische Sprachdatei mit vorangestelltem Marker übernommen. In die
restlichen Sprachdateien werden freigegebene Englische Einträge
übernommen -- natürlich auch wieder markiert.

- Beispiel

```
[Kopf]
Dateiname=C:\Arbeit\OurPlantOS\Sprache\Deutsch.VBL
Stand=20.12.2022
Eintraege=13591

[Liste]
1=OurPlant OS
2=Resetlauf
3=Programm-Reset
...
13584=berücksichtigen
13585=Messen
13586=Vakuum steht nicht bereit
13587=Filtertyp
13588=Farbauswahl
13589=Start
13590=Ende
13591=EntwicklerModus: beide Prozesswerte pro Durchlauf erfassen)

[zu pruefen]
13579=\VWUNIGPH.dfm
13580=\VWUNIGPH.dfm
13581=\VWUNIGPH.dfm
13582=\VWUNIGPH.dfm
13583=\VWUNIGPH.dfm
13584=\VWUNIGPH.dfm
13585=\VWUNIGPH.dfm
13586=\MPUNIGPH.pas
13587=\Monitore\monitors.BondingFrame.dfm
13588=\Monitore\monitors.BondingFrame.dfm
13589=\Monitore\monitors.BondingFrame.pas
13590=\Monitore\monitors.BondingFrame.pas
13591=\Monitore\monitors.BondingFrame.pas
```


### Tags im Quellcode ersetzen (Hauptprogramm)

Der Quellcode kann auf in der Expressions.ini definierte
Sprachfunktionen durchsucht werden, welche mit dem Tag 0 oder -1
versehen sind. Es werden die Tags in der Sprachdatei (deutsche)
eingelesen und mögliche Lücken in den Tags aufgezeigt und schlägt eine
passende Lücke vor.

Der Vorschlag wird wie folgt ermittelt:

-   Es wird nach einer Lücke gesucht die möglichst nah an einem bereits
    in der Datei verwendeten Tag liegt

-   Ist der Abstand größer als 30 wird eine Lücke der Größe
    vorgeschlagen die der Anzahl der zu ersetzenden Tags entspricht

-   Ist das Häkchen „bevorzuge letzten Tag" gesetzt werden die Tags
    immer hinten angehängt

Per Mausklick kann eine Lücke gewählt werden. Der Anfangstag wird in das
Feld „Start Tag" eingetragen welches auch manuell nachbearbeitet werden
kann. Somit können einige Tags frei gehalten werden um evtl. Ergänzungen
zu ermöglichen.

Mit Klick auf „schreibe Tags" werden die 0 bzw. -1 Tags in den
Quellcodedateien ersetzt und neue Einträge in der Sprachdatei angelegt.
Als Eintrag wird der Default-Eintrag im Quellcode mit dem Marker #!
eingetragen.

Aus DFM Dateien werden die Einträge unter Caption verwendet -- hier
müssen obligatorisch die Tags auf -1 gesetzt werden.

###  DFM-Tagger - Ungetaggte Elemente der Benutzeroberfläche taggen

DFM Dateien können auf fehlende Tag-Einträge untersucht werden (was dem
Tag 0 entspricht -- dieser wird nicht explizit in der dfm gespeichert).

Nach Laden der Dateien wird die rechts angefügte Tabelle ausgefüllt. Die
aufgelisteten Typen lassen sich in der Expressions.ini ändern. Mit Klick
auf eine Zeile ändert sich der Zustand der Spalte „ersetze". Ein
Doppelklick auf den Dateinamen öffnet die entsprechende Datei -- wenn
RAD Studio installiert und dfm-Dateien dem Studio zugeordnet sind,
werden sie direkt dort geöffnet.

Ein Klick auf „setze auf -1" fügt den Eintrag „Tag = -1" dem
entsprechenden Objekt in der dfm-Datei ein. Danach können mit dem
normalen LanguageTagger die -1 Tags wieder durch sinnvolle ersetzt
werden.

![Tags setzen](pictures/tagset1.png)

**=> LanguageTagger**

![Tags setzen](pictures/tagset2.png)


###  LanguageFileEditor - Sprachdateieditor

Der Editor sortiert die Tags nach den in der ausgewählten Datei
vorhandenen Tags -- damit für die Übersetzung der entsprechende
Zusammenhang klar ist.

Zwischen den Dateien (in der linken Box gelistet) kann während der
Bearbeitung hin- und hergesprungen werden. Die Änderungen werden im
Speicher gehalten. Vor dem Ändern der Sprachdatei muss allerdings
gespeichert werden -- es werden immer alle angezeigten Sprachdateien
gespeichert.

Das Filtern dauert äußerst lange -- ist dennoch praktisch

Nach dem Editieren sollten die Sprachdateien wieder abgeglichen werden.

###  Einbindung in den Releaselauf

Das Tool kann in der Kommandozeile ausgeführt werden. Sobald ein
Parameter übergeben wird öffnet sich die grafische Oberfläche nicht. Der
Parameter „--h" zeigt die Hilfe an.

Obligatorisch ist die Angabe der Logdatei -- sie kann absolut oder
relativ erfolgen.

Der Rückgabewert ist 0 wenn keine zu ersetzenden Tags gefunden wurden
und -1 wenn doch. Im zweiten Falle wird das angegebene LogFile angelegt
(vorhandene wird überschrieben), in dem die Dateien gelistet werden die
0 bzw. -1 Tags enthalten. Nach dem Dateinamen ist die Anzahl der
Ersetzungen gelistet.

##  Expressions.ini -- Einstellungen

Die Expressions.ini wird im Arbeitsverzeichnis erwartet. Sie enthält die
gespeicherten Einstellungen sowie die zu suchenden Funktionen.

Die einzelnen Abschnitte:

-   **[functions]:**
    Die Liste mit den zu suchenden Funktionen -- hier können die Tags
    auch ersetzt werden. Der erste Parameter gibt die Position des Tags
    an, der Zweite die des Default- Spracheintrags, welcher bei Ersetzen
    der Tags als Deutscher Eintrag in die Sprachdatei übernommen wird.

-   **[returnValues]:**
    sind Funktionen die als Rückgabewert Tags ausgeben. Diese Funktionen
    dienen nur dazu bereits verwendete Tags im Quellcode zu finden.

-   **[directAssignment]:**
    Im Quellcode gibt es Passagen in denen die hier aufgeführten
    Variablen direkte Nummern zugewiesen werden, wie z.B. „*StoerTag =
    15*"

-   **[languageFileNames]:**
    für das Laden der Sprachdateien wird nur der Ordner im Programm
    angegeben. Die deutsche und englische Sprachdatei müssen in diesem
    Abschnitt eingetragen werden. Dies ist nötig um die Reihenfolge für
    die Synchronisierung der Sprachdateien zu gewährleisten.

-   **[settings]:**

    Diese Einstellungen werden vom Programm zum Start geladen und in die
    Benutzeroberfläche übertragen

-   **[DFMTaggingTypes]:**
    Typen die im Unterprogramm „DFM-Tagger" ausgewertet werden. Die hier
    eingetragenen Typen werden in der Tabelle angezeigt um später evtl.
    die Tags auf -1 zu setzen.


  **Beispiel**

- [Expressions.ini](Expressions.md)

##  Graphische Darstellungen

###  Tags ersetzen

![Tags ersetzen](pictures/tags.png)

### Sprachdateien synchronisieren

![Sprachdateien synchronisieren](pictures/sync.png)


## Fehlerfälle

- [folgende Fehlerfälle sind bekannt](Errors.md)