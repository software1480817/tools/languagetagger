**[examples]**
Functions in Regular Expressions till the beginning
without braces e.g.
Sprache.ItemDef(12, 'Test'); -> Sprache\.ItemDef
after the equal sign follows:
1st field number of tag in function
2nd field number of default entry
both are zero based:
Sprache\.ItemDef=0,1
--returnValues--
Functions named with the listed names are porcessed and
the returnValues are interpreted as Language Tags
--directAssignment--
if the value behind the listed word is a number it is
interpreted as a Language tag:
ErrorTag :=2298;

**[functions]**
Sprache\.ItemDef=0,1
MessageWithOKOnlyAndSwitchToMonitor=0,1
MessageWithOKOnly=0,1
Mitteilung=0,1
MitteilungErweitert=0,1
AnfrageErweitert=1,2
Anfrage=1,2
TuerText=0,1
Sprache\.Translator(Ext)?=1,2
\.TriggerError=0,1
sys\.prozess\.ProzessMitteilung=0,1
sys\.Prozess\.Stoerung=1,2
Stoerfall=0,1
Sprache\.SListColTranslator[1N]=2,3
Heading=0,1
Sprache\.TranslateMenuItem=1,2
NotifyErrorDeprecated=1,2
NotifyProcessStatus=0,1
TErrorNotificationData\.Create=1,2
TErrorNotificationData\.CreateMin=0,1
\bE\w+\.create=0,1
TNameTranslator\.Create=0,1
TKontrollMessage\.GetQuestionable=0,1
TKontrollMessage\.GetNotification=0,1
TDataParameter\.Create=0,1
TPositionParameter\.Create=0,1
TWarning\.Create=0,1
ETSURRThread\.Create=0,1

**[returnValues]**
ZustandTag
SammelWarnungTag
ProzessFehlerTag

**[directAssignment]**
StoerTag
ErrorTag
ProzessMitteilungTag
result\.Error

**[languageFileNames]**
German=Deutsch.VBL
English=English.vbl

**[settings]**
SourcesDefDir=C:\Development\VicoBase.V15\trunk
LanguageDefDir=C:\Development\VicoBase.V15\trunk\Sprache
TargetDirectory=C:\Development\VicoBase.V15\trunk\Sprache
OverWriteFiles=1
OverWriteOrigins=1
SearchPASFiles=1
SearchDFMFiles=1
PreferLastTagAsSuggestion=1

**[DFMTaggingTypes]**
TBitBtn
TLabel
TLabeledEdit
TGroupBox
TPanel
TButton
TMenuItem

