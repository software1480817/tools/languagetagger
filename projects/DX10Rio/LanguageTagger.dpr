// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program LanguageTagger;

{$APPTYPE CONSOLE}

uses
  Vcl.Forms,
  Winapi.Windows,
  System.SysUtils,
  Info in '..\..\source\Info.pas' {FormInfo},
  commandline in '..\..\source\commandline.pas',
  common.SafeMemIni in '..\..\source\common.SafeMemIni.pas',
  dfmTagging in '..\..\source\dfmTagging.pas' {DFMTagger},
  FormshowAlreadyUsedTags in '..\..\source\FormshowAlreadyUsedTags.pas' {FormalreadyUsed},
  LangFileEditor in '..\..\source\LangFileEditor.pas' {LanguageFileEditor},
  main in '..\..\source\main.pas' {Form1},
  replaceTags in '..\..\source\replaceTags.pas',
  settings in '..\..\source\settings.pas' {FormSettings},
  unusedTags in '..\..\source\unusedTags.pas' {FormUnusedTags},
  usedTags in '..\..\source\usedTags.pas';

{$R Version.res}

var
  vCommandLinePreSetting: TCommandLinePreSettings;
  vString: string;
begin
  Application.Initialize;
  vCommandLinePreSetting.Reset;
  if ParamCount>0 then
  begin
    if FindCmdLineSwitch('onlypreset') then
    begin
      FindCmdLineSwitch('projfile', vCommandLinePreSetting.sources, True, [clstValueNextParam]);
      FindCmdLineSwitch('langdir', vCommandLinePreSetting.langdir, True, [clstValueNextParam]);
      FindCmdLineSwitch('parentdir', vString, True, [clstValueNextParam]);
      vCommandLinePreSetting.filterFilesOnShow := FindCmdLineSwitch('filterFiles');
      if not vString.IsEmpty then
        vCommandLinePreSetting.parentdir := vString.ToInteger;
    end
    else
    begin
      ExitCode := 0;
      try
        ExitCode := CommandLineExecution();
      except
        on E: Exception do
        begin
          ExitCode := -1;
          Writeln(E.ClassName, ': ', E.Message);
        end;
      end;
      Exit;
    end;
  end;

  FreeConsole();
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Form1.ReplaceTags.CommandLinePreSettings := vCommandLinePreSetting;
  Application.Run;  
end.
